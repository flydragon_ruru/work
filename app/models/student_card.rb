class StudentCard < ApplicationRecord
  belongs_to :student, optional: true
  belongs_to :course_info, optional: true
end
