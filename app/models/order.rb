class Order < ApplicationRecord
  belongs_to :good
  belongs_to :employee
  belongs_to :student
  belongs_to :studio
end
