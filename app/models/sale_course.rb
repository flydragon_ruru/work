class SaleCourse < ApplicationRecord
  acts_as_paranoid
  belongs_to :company, optional: true
  belongs_to :course_info, optional: true

  scope :defaults, -> { where(name: %w(动漫玩具 潮流服饰 品牌周边)) }
  scope :privates, -> { where.not(name: %w(动漫玩具 潮流服饰 品牌周边)) }

  def period
    self.course_info.period
  end
end
