class AppointmentRecord < ApplicationRecord
  belongs_to :course, optional: true
  belongs_to :student, optional: true
  belongs_to :operator, class_name: "Employee", foreign_key: "operated_by", optional: true

  validates :course_id, uniqueness: {scope: [:student_id]} 
end
