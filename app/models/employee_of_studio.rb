class EmployeeOfStudio < ApplicationRecord
  #validates_presence_of :employee_id, :studio_id, :employee_user_id, :studio_company_id, :position_id, :department_id
  validates_presence_of :employee_id, :studio_id, :employee_user_id, :studio_company_id

  belongs_to :studio
  belongs_to :company, foreign_key: "studio_company_id"
  belongs_to :employee
  has_many :targets
  has_one :teacher, :dependent => :destroy
  belongs_to :position_configuration, foreign_key: "position_id"
  belongs_to :department_configuration, foreign_key: "department_id"

  def employee_user_name
    self.employee.name
  end
end
