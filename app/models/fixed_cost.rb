class FixedCost < ApplicationRecord
  acts_as_paranoid
  has_and_belongs_to_many :studios, :join_table => "fixed_costs_studios"
  validates :name, presence: true
  validates :amount, presence: true
  validates :cycle, presence: true
end
