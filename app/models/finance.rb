class Finance < ApplicationRecord
  validates :finance_category, presence: true
  validates :remark, presence: true
  validates :amount, presence: true
  validates :setdate, presence: true
  belongs_to :studio


  INCOME_CATEGORY = {"主营业务收入" => "主营业务收入", "营业外收入" => "营业外收入", "其他业务收入" => "其他业务收入"}

  EXPENSE_CATEGORY = {"产品成本" => "产品成本", "管理成本" => "管理成本", "营销成本" => "营销成本", "学员退费" => "学员退费", "其他业务成本" => "其他业务成本"}


  def get_studio_name

    return "总公司" if self.finance_category=="员工工资"
    return self.studio.present? ? self.studio.name : ""
  end
  
end

