# 员工
class Employee < ApplicationRecord
  validates_presence_of :user_id
  validates :user_id, uniqueness: {scope: [:company_id]}

  has_many :studios, :through => :employee_of_studios
  has_many :employee_of_studios
  has_many :salaries
  has_many :salary_month_statistics
  belongs_to :company
  belongs_to :user

  def one_position
    position_names = self.employee_of_studios.select(:position_name).distinct.map {|x| x.position_name}
    if position_names.size == 1
      position_names.first
    else
      position_names.first + " 等"
    end
  end

  def one_department
    department_ids = self.employee_of_studios.select(:department_id).distinct.map {|x| x.department_id}
    if department_ids.size == 1
      DepartmentConfiguration.find(department_ids.first).department_name
    else
      DepartmentConfiguration.find(department_ids.first).department_name+ " 等"
    end
  end

  def one_studio
    studio_ids = self.employee_of_studios.select(:studio_id).distinct.map {|x| x.studio_id}
    if studio_ids.size == 1
      Studio.find(studio_ids.first).name
    else
      Studio.find(studio_ids.first).name+ " 等"
    end
  end

end
