# 公司
class Company < ApplicationRecord
  has_many :studios
  has_many :department_configurations
  has_many :position_configurations
  has_one  :private_education_configuration
  has_many :teacher_level_configurations
  has_many :course_room_configurations
  has_one :bonus_configuration
  #has_many :goods_category_configurations
  has_many :student_group_configurations
  has_many :employee_welfare_configurations
  has_many :teachers
  has_many :potential_student_group_configurations
  has_many :potential_students
  has_many :students
  has_many :employee_of_studios, foreign_key: "studio_company_id"
  has_many :employees
  has_many :sale_courses
  has_many :goods
  has_many :course_infos
  has_many :courses, through: :course_infos
  has_many :course_reckons, through: :studios
  has_many :orders, through: :studios
  has_many :student
  has_many :finances, through: :studios
  has_many :salary_month_statistics

  validates_presence_of :name, :owner_id
  validates_uniqueness_of :owner_id

  after_create :initialize_company_configurations


  DANCE_TYPE = {"Jazz" => "Jazz", "HipHop" => "HipHop", "Poppin" => "Poppin", "Locking" => "Locking", "Waacking" => "Waacking", "House" => "House", "Breaking" => "Breaking", "其他" => "其他"}

  AGE_GROUP = {"幼儿" => "幼儿", "少年" => "少年", "青少年" => "青少年", "成年" => "成年"}

  SOURCE_FROM = {"线下活动" => "线下活动", "官方网站" => "官方网站", "互联网搜索" => "互联网搜索", "微信宣传" => "微信宣传", "广告招贴" => "广告招贴", "自然到访" => "自然到访", "学员推荐" => "学员推荐", "其他" => "其他"}

  WEEKTIME = {"0" => "星期一","1" => "星期二", "2" => "星期三", "3" => "星期四", "4" => "星期五", "5" => "星期六" , "6" => "星期日"}

  GENDER = {"男" => "男", "女" => "女"}

  def initialize_company_configurations
    DepartmentConfiguration.create! company_id: id, department_name: '主理人'
    DepartmentConfiguration.create! company_id: id, department_name: '财务部'
    DepartmentConfiguration.create! company_id: id, department_name: '人资部'
    DepartmentConfiguration.create! company_id: id, department_name: '产品部'
    DepartmentConfiguration.create! company_id: id, department_name: '市场部'
    DepartmentConfiguration.create! company_id: id, department_name: '运营部'

    PositionConfiguration.create! company_id: id, position_name: '主理人'
    PositionConfiguration.create! company_id: id, position_name: '产品总监'
    PositionConfiguration.create! company_id: id, position_name: '导师'
    PositionConfiguration.create! company_id: id, position_name: '市场总监'
    PositionConfiguration.create! company_id: id, position_name: '市场专员'
    PositionConfiguration.create! company_id: id, position_name: '运营总监'
    PositionConfiguration.create! company_id: id, position_name: '前台'
    PositionConfiguration.create! company_id: id, position_name: '服务顾问'
    PositionConfiguration.create! company_id: id, position_name: '财务总监'
    PositionConfiguration.create! company_id: id, position_name: '会计/出纳'
    PositionConfiguration.create! company_id: id, position_name: '人资行政总监'
    PositionConfiguration.create! company_id: id, position_name: 'HR/行政'

    SaleCourse.create! company_id: id, sale_type: '周边商品', name: '动漫玩具'
    SaleCourse.create! company_id: id, sale_type: '周边商品', name: '潮流服饰'
    SaleCourse.create! company_id: id, sale_type: '周边商品', name: '品牌周边'
  end
end
