class CourseReckon < ApplicationRecord
  belongs_to :teacher
  belongs_to :course_info
  belongs_to :course
  belongs_to :studio
end
