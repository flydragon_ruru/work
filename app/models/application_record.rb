class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true


  #获取头像的url,待有秘钥的链接
  def avatar_url
    avatar.present? ? Qiniu::Auth.authorize_download_url(avatar) : nil
  end
end
