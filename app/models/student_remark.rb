class StudentRemark < ApplicationRecord
  belongs_to :market_employee, class_name: "Employee", foreign_key: "market_employee_id", optional: true
  belongs_to :service_employee, class_name: "Employee", foreign_key: "service_employee_id", optional: true
  belongs_to :teacher_employee, class_name: "Employee", foreign_key: "teacher_employee_id", optional: true
  belongs_to :receptionist_employee, class_name: "Employee", foreign_key: "receptionist_employee_id", optional: true
  belongs_to :potential_student, optional: true
  belongs_to :student, optional: true
  
  # 获取学生评价或者回访员工id＝》name,kevin
  def remark_people_name
  	employee_id = 0; 
  	if market_employee_id
  		employee_id = market_employee_id;
  	elsif service_employee_id
  		employee_id = service_employee_id;
  	elsif teacher_employee_id
  		employee_id = teacher_employee_id;
  	elsif receptionist_employee_id
  		employee_id = receptionist_employee_id;
  	end
  	return Employee.find_by_id(employee_id).name;
  end
end
