class SalaryMonthStatistic < ApplicationRecord

  belongs_to :company
  belongs_to :employee
end
