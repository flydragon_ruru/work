class CourseRoomConfiguration < ApplicationRecord
  belongs_to :company
  belongs_to :studio, foreign_key: "studio_id"
end
