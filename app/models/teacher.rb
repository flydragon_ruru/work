class Teacher < ApplicationRecord
  belongs_to :employee_of_studio


  # 导师本月工资,kevin
  def current_salary
  	
  	teacher = self;

  	p "导师本月动态工资记录 = >基本工资 ＋ 课时费 ＋ 超出指标提成 ＋ 私教提成"
    teacher_salary = {"course_fixed_student" => 0};
    teacher_salary_array = teacher.employee_of_studio.employee.salaries;

    # 导师阶段提成单独处理
    teacher_ladder_salary = Array.new();
    student_ladder_count = 0;

    for salary in teacher_salary_array.to_a
        p "----#{salary}";
        p salary.salary_type;
        
        # 工资类型
        salary_type = salary.salary_type;
        salary.amount = salary.amount ? salary.amount : 0;

        if salary_type == "基本工资"
            p teacher_salary["base_salary"] = salary.amount;
        end
        
        # 导师课表详情私教 TODO
        if salary_type == "导师_私教提成"
            private_percent = salary.amount;
            p teacher_salary["private_teaching"] = (0) * private_percent / 100; 
        end
        
        # 课时费／课，分为固定提成和阶段提成／人
        if salary_type == "导师_课时费"
            teacher_check_ins_size = CheckIn.where(teacher_id: teacher.id).where("check_ins.created_at 
                between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}' 
                and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").size;
            p teacher_salary["course_check_all"] = salary.amount * teacher_check_ins_size;
        end

        # 本月导师授课学生人数
        p course_ids = Course.where(teacher_id: teacher.id).where("courses.day_of_week 
                between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}' 
                and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").ids;
        p student_count = CheckIn.where(course_id: course_ids).where(teacher_id: nil).size;

        student_ladder_count = student_count;

        if salary_type == "导师_固定提成"
            p teacher_salary["course_fixed_student"]  = salary.amount * student_count;
        end

        if salary_type == "导师_阶段提成"
            teacher_ladder_salary.push(salary);
        end


        # 导师超出当月指标提成，累计阳历年度指标 TODO
        p teacher_salary["courese_index_bouns"]  = 0;

    end

    p "单独计算导师阶梯提成"
    p teacher_ladder_salary;
    teacher_ladder_sum = 0;
    p "学生数量#{student_ladder_count}";
    if student_ladder_count > 0
        for ladder in teacher_ladder_salary
            # p ladder
            tmp = student_ladder_count;
            # p student_ladder_count = student_ladder_count - ladder.range_max;

            if student_ladder_count >= 0
                teacher_ladder_sum += ladder.amount * ladder.range_max;
            else
                teacher_ladder_sum += ladder.amount * tmp;
                break;
            end
        end
    end
    p teacher_salary["course_ladder_student_all"]  = teacher_ladder_sum;
    p teacher_salary["month"] = Time.new().strftime("%Y-%m");
    Rails.logger.debug "base_salary:#{teacher_salary["base_salary"]}"
    Rails.logger.debug "course_check_all:#{teacher_salary["course_check_all"]}"
    Rails.logger.debug "course_fixed_student:#{teacher_salary["course_fixed_student"]}"
    Rails.logger.debug "course_ladder_student_all:#{teacher_salary["course_ladder_student_all"]}"
    Rails.logger.debug "courese_index_bouns:#{teacher_salary["courese_index_bouns"]}"
    Rails.logger.debug "private_teaching:#{teacher_salary["private_teaching"]}"

    p teacher_salary["sum"] = teacher_salary["base_salary"] + teacher_salary["course_check_all"] + 
          teacher_salary["course_fixed_student"] + teacher_salary["course_ladder_student_all"] +
          teacher_salary["courese_index_bouns"] + teacher_salary["private_teaching"];
  	return teacher_salary;
  end
end
