class PositionConfiguration < ApplicationRecord
  belongs_to :company
  has_and_belongs_to_many :roles

  
  scope :defaults, -> { where(position_name: self.get_default_positon_info.keys) }
  scope :privates, -> { where.not(position_name: self.get_default_positon_info.keys) }


  #侯飞龙  默认职务操作方法提供
  class << self

    #获取默认的职务信息，职务名称为健，角色列表为值
  	def get_default_positon_info

      if @default_positon_info.present?
      	return @default_positon_info 
      else
      	@default_positon_info = {}
      end

      YAML.load_file("#{Rails.root.to_s}/config/position_permit.yml").values.map{|v| 
      	@default_positon_info[v['name']] = v['roles'] 
      }
      @default_positon_info
  	end


    #根据职务名称判断是否为默认职务
  	def default_position?(position_name)

  	  self.get_default_positon_info.keys.include?(position_name)
  	end


    #根据默认职务的名称获取对应的角色列表
    def get_default_positions_roles(position_name)
      
      self.default_position?(position_name) ? self.get_default_positon_info[position_name] : []
    end


  end

end
