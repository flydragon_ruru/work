class Course < ApplicationRecord
  belongs_to :course_info
  has_many :appointment_records
  has_many :check_ins

  def teacher_checked(teacher)
     return self.check_ins.where(teacher_id: teacher.id).first.present?
  end
end
