class CheckIn < ApplicationRecord
  belongs_to :student, optional: true
  belongs_to :course
  belongs_to :teacher, optional: true
  belongs_to :operator, class_name: "Employee", foreign_key: "operated_by", optional: true
end
