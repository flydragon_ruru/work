class Target < ApplicationRecord
  scope :month_target, ->(month) { where("month_of_target >= ? and month_of_target < ? ", Date.parse(month), Date.parse(month) + 1.month)}
  belongs_to :employee_of_studio
  belongs_to :studio, optional: true
  
end
