class Discount < ApplicationRecord
  belongs_to :good

  def discount_percent unit
    if self.discount_type == '时间优惠'
      self.percent if Date.current.between?(self.begin_at,self.end_at)
    elsif self.discount_type == '数量优惠'
      self.percent if unit > self.amount
    end
  end

end
