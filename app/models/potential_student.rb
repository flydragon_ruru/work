class PotentialStudent < ApplicationRecord
  belongs_to :student_group_configuration, foreign_key: "student_group",optional: true
  belongs_to :intention_studio, class_name: "Studio", foreign_key: "willing_studio",optional: true
  belongs_to :studio
  belongs_to :market_employee, class_name: "Employee", foreign_key: "market_employee_id",optional: true
  belongs_to :company
  has_many :student_remarks
  has_one :student
  has_many :potential_orders, class_name: "Order", foreign_key: :potential_student_id 

  def student_age_group
    if self.birthday.blank?
      '-'
    else
      if (self.birthday < (Date.current - 3.year)) && (self.birthday > (Date.current - 6.year))
        '幼儿'
      elsif (self.birthday < (Date.current - 6.year)) && (self.birthday > (Date.current - 12.year))
        "少年"
      elsif (self.birthday < (Date.current - 12.year)) && (self.birthday > (Date.current - 18.year))
        "青少年"
      elsif (self.birthday < (Date.current - 18.year))
        "成年"
      else
        "-"
      end
    end
  end
  
  
  #获取头像的url,待有秘钥的链接
  def avatar_url

    avatar.present? ? Qiniu::Auth.authorize_download_url(avatar) : nil
  end

end
