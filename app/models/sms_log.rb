class SmsLog < ApplicationRecord

	class << self
		# 验证码无效
		def sn_invalid?(mobile, sn_code)
			sms = SmsLog.where(mobile: mobile).last
			return true if sms.blank? || (sms.sms_type != 'sn_code') || (sms.expired_at < Time.now)

			hash = JSON.parse(sms.content_hash).with_indifferent_access rescue {}
			!(hash[:sn_code] == sn_code)
		end
	end
end
