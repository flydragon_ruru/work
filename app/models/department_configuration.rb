class DepartmentConfiguration < ApplicationRecord
  belongs_to :company

  scope :defaults, -> { where(department_name: %w(主理人 财务部 人资部 产品部 市场部 运营部)) }
  scope :privates, -> { where.not(department_name: %w(主理人 财务部 人资部 产品部 市场部 运营部)) }
end
