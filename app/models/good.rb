class Good < ApplicationRecord
  has_many :discounts
  has_many :orders
  belongs_to :company

  def good_status
    if self.stock == 0
      '缺货'
    else
      self.status
    end
  end

  def good_info
    SaleCourse.find(self.good_info_id).name
  end

  def course_unit
    name = self.good_info
    if name == '年卡'
      '年'
    elsif name == '月卡'
      '月'
    elsif name == '次卡'
      '次'
    elsif name == '充值'
      '元'
    else
      '课时'
    end
  end

  def time_discount_price
    percent = self.discounts.where(discount_type: '时间优惠')
                  .where('begin_at <= ? and end_at >= ?', Date.current, Date.current)
                  .minimum(:percent)
    percent = percent || 100
    self.price * percent/100
  end

end
