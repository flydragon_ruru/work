class CourseInfo < ApplicationRecord
  belongs_to :teacher
  has_many :pack_sales
  has_many :student_cards
  has_many :paying_ways
  has_one :sale_course
  belongs_to :company
  has_many :courses
  has_many :check_ins, through: :courses
  has_many :appointment_records, through: :courses
  belongs_to :room, class_name: "CourseRoomConfiguration", foreign_key: "room_id"

  SALE_TYPE = {"单次售卖课程" => "单次售卖课程", "打包售卖课程" => "打包售卖课程"}
  COURSE_TYPE = {"常规课" => "常规课", "特色课" => "特色课", "私教" => "私教"}
  COURSE_STATUS = {"正常" => "正常", "结课" => "结课"}

  def appointmented
    if self.appointmented_or_not
      "需要预约"
    else
      "无需预约"
    end
  end
end
