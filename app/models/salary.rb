class Salary < ApplicationRecord
  validates_presence_of :employee_id, :employee_user_id, :salary_type

  belongs_to :employee
end
