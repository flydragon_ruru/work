#员工资表数据服务类  flydragon
class Finances::LaborageService
  delegate :params, :session, :raw, :link_to, :number_to_currency, to: :@view
  include HeiPisService

  #view 视图上下文
  #company 公司
  def initialize(view,company)
    @view = view
    @company = company
  end

  #执行方法 返回值
  def call

    page = params[:page].to_i
    pagesize = params[:pagesize].to_i
    studio_id = params[:studio_id]
    department_id = params[:department_id]
    position_name = params[:position_name]
    employee_name = params[:employee_name]
    month = params[:month]

    filter_sql = "company_id = :company_id"
    filter_params = {company_id: @company.id}
    if studio_id.present?
      filter_sql += " and studio_ids REGEXP :studio_id"
      filter_params[:studio_id] = "(#{studio_id})"
    end

    if department_id.present?
      filter_sql += " and department_ids REGEXP :department_id"
      filter_params[:department_id] = "(#{department_id})"
    end

    if position_name.present?
      filter_sql += " and position_names REGEXP :position_name"
      filter_params[:position_name] = position_name
    end

    if employee_name.present?
      filter_sql += " and employee_name like :employee_name"
      filter_params[:employee_name] = "%#{employee_name}%"
    end

    if month.present?
      filter_sql += " and month like :month"
      filter_params[:month] = "%#{month}%"
    end
    


    searchRecord = SalaryMonthStatistic.where(filter_sql,filter_params)
    
    count = searchRecord.count
    data = searchRecord.offset((page-1)*pagesize).limit(pagesize).order("month desc")

    DataTableResult.new(searchRecord.count,data)
  end

end
