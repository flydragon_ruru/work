#公司财务日系统统计服务类  flydragon
class Finances::CompanyDateStatisticsService
  include HeiPisService

  # company 公司
  # date 日期时间
  def initialize(company,date)
    @company = company
    @date = date
    @start_date = date.beginning_of_day
    @end_date = date.end_of_day
  end

  #执行方法 返回值
  def call

    flag = true

    begin

      set_total_income()
      set_total_expenditure()
    rescue Exception => e
      Rails.logger.info "公司财务明细统计出现异常#{e.message}!"
      flag = false
    end
    flag
  end

  private

  # 收入总计  工工作室维护+工作室订单
  def set_total_income

    group_where = {"studios.company_id" => @company.id,"orders.created_at" => @start_date..@end_date}
    group_select = "sum(orders.total_price) as total_price,orders.studio_id"

    order_studio_groups = Order.joins(:studio).where(group_where).select(group_select).group("orders.studio_id")
    order_studio_groups.each do |ord_group|

      init_params = {company_id: company.id,studio_id: ord_group.studio_id,setdate: @date.strftime("%Y-%m-%d")}
      init_params[:is_system] = 1
      init_params[:finance_type] = "收入"
      init_params[:finance_category] = "订单收入"
      init_params[:remark] = "订单收入"
      finace = Finance.find_or_create_by(init_params)
      finace.amount = ord_group.total_price
      finace.save
    end

  end


  # 支出＝ 固定支出 + 员工薪资
  def set_total_expenditure
    
    # 设置固定支出
    set_fixedcost()

    # 员工薪资
    set_employee_salary()
  end

  # 设置固定支出
  def set_fixedcost

    group_where = {"studios.company_id" => @company.id}
    group_select = "sum(fixed_costs.amount/fixed_costs.cycle) as pre_cycle,fixed_costs_studios.studio_id"

    # 固定支出
    # 每月的固定支出
    fixed_costs_groups = FixedCost.joins(:studios).where(group_where).select(group_select).group("fixed_costs_studios.studio_id")
    
    # 获取当月的天数
    month_days = Time.days_in_month(@date.month,@date.year)
  
    fixed_costs_groups.each do |cost_group|

      init_params = {company_id: @company.id,studio_id: cost_group.studio_id,setdate: @date.strftime("%Y-%m-%d")}
      init_params[:is_system] = 1
      init_params[:finance_type] = "支出"
      init_params[:finance_category] = "固定支出"
      init_params[:remark] = "固定支出"
      
      finace = Finance.find_or_create_by(init_params)
      finace.amount = cost_group.pre_cycle/month_days.to_f
      finace.save!
    end

  end


  # 设置工资 默认为月份的开始日期 (只统计员工月工资,不统计员工的每天工资)
  def set_employee_salary

    month = @date.strftime("%Y-%m")

    group_info = SalaryMonthStatistic.where(company_id:1,month: month).group("month").sum("sum_salary")

    # p group_info

    init_params = {company_id: @company.id,setdate: @date.beginning_of_month.strftime("%Y-%m-%d")}
    init_params[:is_system] = 1
    init_params[:finance_type] = "支出"
    init_params[:finance_category] = "员工工资"
    init_params[:remark] = "员工工资"
    init_params[:studio_id] = Studio.where(company_id: @company.id).first.id
    finace = Finance.find_or_create_by(init_params)
    finace.amount = (group_info[month]||=0)
    finace.save!
  end


end
