class UserService

  USER_REDIS_PREFIX = "hepishi_yundiudiu_users_"

  def initialize(current_user)

    @current_user = current_user
  end

  #设置当前用户的范围到公司(值1 公司,2,分店)
  def reset_scope_to_company
    Redis.current.set("#{USER_REDIS_PREFIX}#{@current_user.id}_scope","company")
    Rails.logger.debug "设置当前用户的范围到公司,值为:#{get_user_scope}"
  end  


  #设置当前的范围到分店(值1 公司,2,分店)
  def reset_scope_to_studio
    Redis.current.set("#{USER_REDIS_PREFIX}#{@current_user.id}_scope","studio")
    Rails.logger.debug "设置当前用户的范围到分店,值为:#{get_user_scope}"
  end

  def scope_to_company?
     Rails.logger.debug "设置当前用户的范围值为:#{get_user_scope}"
     get_user_scope == "company"
  end

  def scope_to_studio?
    Rails.logger.debug "设置当前用户的范围值为:#{get_user_scope}"
    get_user_scope == "studio"
  end

  def get_user_scope
    Redis.current.get("#{USER_REDIS_PREFIX}#{@current_user.id}_scope")
  end

end
