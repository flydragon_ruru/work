#意向学员年龄比例  flydragon
class StatisticalDatum::RecruitPotentialStudentAgeScaleService
  include HeiPisService

  #company 单位
  #studio 分店
  #status 状态 所有,  转化, 未转化 
  def initialize(company, studio,status = "")
    @company = company
    @studio = studio
    @status = status
  end


  #执行方法 返回值
  def call

    age_stages = %w(幼儿 少年 青少年 成年)

    result_infos = []

    filter_params = {:company_id => @company.id }

    sql_where = "company_id = :company_id and (birthday between :start_date and :end_date)"
    if @studio.present?
      filter_params[:studio_id] = @studio.id 
      sql_where += " and studio_id = :studio_id"
    end

    if @status.present?
      filter_params[:status] = @status
      sql_where += " and status = :status"
    end


    age_stages.each do |stage_name|
      result_info = {}
      filter_params.merge! AgeStageService.call(stage_name)
      result_info[:value] = PotentialStudent.where(sql_where,filter_params).count
      result_info[:name] = stage_name
      result_infos << result_info
    end

    result_infos
  end

end
