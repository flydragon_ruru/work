#招生数据总计服务类  flydragon
class StatisticalDatum::RecruitTotalService
  include HeiPisService

  #company 单位
  #studio 分店
  def initialize(company, studio)
    @company = company
    @studio = studio
  end


  #执行方法 返回值
  def call

  	# company = current_company
  	scope_obj = @studio.present? ? @studio : @company

    #=======最近30天数据=======#
    #时间数组
    dates = []
    #新增意向学员数量数组
    grow_potential_counts = []
    #新增学员数量数组
    grow_counts = []
    for day in ((Date.current - 30.day)..Date.current)
      
      dates << day.strftime('%Y-%m-%d')

      Rails.logger.info "scope_obj class is: #{scope_obj.to_json}"

      filter_params = {created_at: ((day.to_datetime)..(day.to_datetime + 1.day - 1.second))}
      grow_potential_counts << scope_obj.potential_students.where(filter_params).count
      grow_counts << scope_obj.students.where(filter_params).count

    end

    Result.new(dates,grow_potential_counts,grow_counts)

  end

  #内部类,指定结果返回值
  class Result
  	attr_reader :dates, :grow_potential_counts,:grow_counts

	  def initialize(dates, grow_potential_counts,grow_counts)
      @dates = dates
      @grow_potential_counts = grow_potential_counts
      @grow_counts = grow_counts
    end 

  end

end
