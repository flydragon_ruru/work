#学员性别比例  flydragon
class StatisticalDatum::RecruitPotentialStudentSexScaleService
  include HeiPisService

  #company 单位
  #studio 分店
  def initialize(company, studio)
    @company = company
    @studio = studio
  end


  #执行方法 返回值
  def call

    result_infos = []

    filter_params = {:company_id => @company.id }

    sql_where = "where po_stu.company_id = :company_id "
    if @studio.present?
      filter_params[:studio_id] = @studio.id 
      sql_where += " and po_stu.studio_id = :studio_id"
    end

    sql = %Q{
      select count(po_stu.id) as value, po_stu.sex as name
      from potential_students po_stu  
      #{sql_where} 
      group by po_stu.sex
    }

    #生成默认的比例
    scale_infos = {}
    %w(男 女).each do |source_from|
      scale_infos[source_from] = {"value" => 0,"name" => source_from}
    end

    PotentialStudent.find_by_sql([sql,filter_params]).each do |record|
      scale_infos[record.name]["value"] = record['value']
    end
    
    scale_infos.values
  end

  

end
