#转换学员比例服务类  flydragon
class StatisticalDatum::RecruitTransformStudentScaleService
  include HeiPisService

  #company 单位
  #studio 分店
  def initialize(company, studio)
    @company = company
    @studio = studio
  end


  #执行方法 返回值
  def call

    
    month_begin_at = Date.new(Date.current.year, Date.current.month)

    filter_params = {:company_id => @company.id,:start_date => Time.new(month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}
    filter_params[:end_date] = Time.now

    sql_where = "where po_stu.company_id = :company_id and (po_stu.created_at between :start_date and :end_date)"
    if @studio.present?
      filter_params[:studio_id] = @studio.id 
      sql_where += " and po_stu.studio_id = :studio_id"
    end

    sql = %Q{
      select count(po_stu.id) as value, po_stu.status as name
      from potential_students po_stu  
      #{sql_where} 
      group by po_stu.status
    }

    #比例信息
    scale_infos = {"未转化" => {"value" => 0,"name" => "未转化学员"},"已转化" => {"value" => 0,"name" => "已转化学员"}}
    PotentialStudent.find_by_sql([sql,filter_params]).each do |record|
      scale_infos[record.name]["value"] = record['value']
    end

    scale_infos.values
  end


end
