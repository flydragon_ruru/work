#工资月统计服务类  flydragon
class StatisticalDatum::FinanceSalariesService
  include HeiPisService

  #company 单位
  #studio 分店
  def initialize(company, studio,start_date,end_date)
    @company = company
    @studio = studio
    @start_date = start_date
    @end_date = end_date
  end


  #执行方法 返回值
  def call

    
    data = {}

    # 获取当前单位的员工和工资信息
    employees = Employee.includes(:salaries).where("employees.company_id" => @company.id)

    month_strs_for_dates(@start_date,@end_date).each do |month_str|
      employees.each do |employee|

        # 获取员工的工资信息 
        result = Employees::MonthSalaryService.call(employee,month_str)

        data[month_str]=0 if !data[month_str].present?
        data[month_str] += result.sum_salary
      end
    end
    data
  end

end
