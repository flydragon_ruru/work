#人资数据总计服务类  flydragon
class StatisticalDatum::ResourceService
  include HeiPisService

  #company 单位
  #studio 分店
  def initialize(company, studio)
    @company = company
    @studio = studio
  end


  #执行方法 返回值
  def call
     
    sex_data = sex_statistics()
    position_data = position_statistics()
    status_data = status_statistics()
    leave_position_data = position_statistics("离职")

    Result.new(sex_data,position_data,status_data,leave_position_data)
  end


  #职务统计
  def position_statistics(status="在职")

    position_data = {}
    PositionConfiguration.get_default_positon_info.keys.map{|positon_name|
         position_data[positon_name] = 0
    }

    filter_params = {:company_id => @company.id ,:status => status}

    sql_where = " where em.company_id = :company_id and em.status = :status "
    if @studio.present?
      filter_params[:studio_id] = @studio.id 
      sql_where += " and em_of_stu.studio_id = :studio_id "
    end
    sql = %Q{
      select count(distinct(em.id)) as position_count,em_of_stu.position_name from employees em  
      join employee_of_studios em_of_stu on em.id = em_of_stu.employee_id 
       #{sql_where}
      group by em_of_stu.position_name;
    }

    Employee.find_by_sql([sql,filter_params]).each do |record|
      position_data[record.position_name] = record.position_count
    end
    position_data
  end



  #离职在职统计
  def status_statistics

  	status_data = {"在职" => 0, "离职" => 0}

    filter_params = {:company_id => @company.id }

    sql_where = " where em.company_id = :company_id"
    if @studio.present?
      filter_params[:studio_id] = @studio.id 
      sql_where += " and em_of_stu.studio_id = :studio_id "
    end
    sql = %Q{
      select count(distinct(em.id)) as status_count,em.status from employees em  
      join employee_of_studios em_of_stu on em.id = em_of_stu.employee_id  
      #{sql_where}
      group by em.status;
    }

    Employee.find_by_sql([sql,filter_params]).each do |record|
      status_data[record.status] = record.status_count
    end
    status_data
  end

  #性别统计
  def sex_statistics

  	sex_data = {"男" => 0, "女" => 0}

    filter_params = {:company_id => @company.id }

    sql_where = " where em.company_id = :company_id and em.status = '在职' "
    if @studio.present?
      filter_params[:studio_id] = @studio.id 
      sql_where += " and em_of_stu.studio_id = :studio_id "
    end
    sql = %Q{
      select count(distinct(em.id)) as sex_count,em.sex from employees em  
      join employee_of_studios em_of_stu on em.id = em_of_stu.employee_id  
      #{sql_where}
      group by em.sex;
    }

    Employee.find_by_sql([sql,filter_params]).each do |record|
      sex_data[record.sex] = record.sex_count
    end
    sex_data
  end

  #内部类,指定结果返回值
  class Result
  	attr_reader :sex_data, :position_data,:status_data,:leave_position_data

    #sex_data 性别数据统计
    #position_data 职务数据统计
    #status_data 离职在职数据统计
    #leave_position_data 离职职务人员统计
	  def initialize(sex_data, position_data, status_data,leave_position_data)
      @sex_data = sex_data
      @position_data = position_data
      @status_data = status_data
      @leave_position_data = leave_position_data
    end 

  end

end
