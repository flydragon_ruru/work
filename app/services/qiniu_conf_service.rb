#七牛云配置服务类
class QiniuConfService

  #执行方法 返回值
  def self.call

    return @result if @result.present?
    
    Rails.logger.info "七牛云配置信息为加载,开始读取配置文件"

    qiniu_conf = YAML.load_file("#{Rails.root.to_s}/config/qiniu.yml")[Rails.env]

    Rails.logger.info "七牛云配置文件加载,环境为:#{Rails.env},信息为:#{qiniu_conf.to_json}"
    Rails.logger.info "七牛云配置信息为:#{@result.to_json}"

    @result = Result.new(qiniu_conf["domain"], qiniu_conf["access_key"], qiniu_conf["secret_key"], qiniu_conf["bucket"])

    Rails.logger.info "七牛云配置信息为1111111111111:#{@result.to_json}"
    @result
  end


  #定义七牛云配置信息
  class Result
    attr_reader :domain, :access_key,:secret_key,:bucket

	  def initialize(domain, access_key,secret_key,bucket)
      @domain = domain
      @access_key = access_key
      @secret_key = secret_key
      @bucket = bucket
    end  	

  end


end
