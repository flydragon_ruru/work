#员工月工资统计服务类  flydragon
class Employees::EditService
  include HeiPisService

  #employee 员工
  def initialize(employee)
    @employee = employee
  end

  #执行方法 返回值
  def call

    employee_data = @employee.serializable_hash.except("updated_at","created_at","user_id","company_id","dismissed_at","disabled","joined_at")
    employee_data["avatar_url"] = @employee.avatar_url
    Rails.logger.info "图片地址为:#{ @employee.avatar_url}"
    
    # 定义提成节点的排序块
    info_sort = lambda { |array| 
      array.sort_by!{|x| x["start"].to_i}  if array.class==Array
    }

    result = Result.new(employee_data,get_employee_of_studio_data,get_teacher_salary(&info_sort))

    # 设置导师除外的 超出指标提成
    salary_type_params = {:fuwuguwen_salary => "服务顾问_超出指标提成",
            :chanpinzongjian_salary => "产品总监_超出指标提成",
            :yunyingzongjian_salary => "运营总监_超出指标提成",
            :shichangzongjian_salary => "市场总监_超出指标提成",
            :shichangzhuanyuan_salary => "市场专员_超出指标提成"}
    salary_type_params.each do |key,salary_type|
      info = get_ticheng_info(key,salary_type,&info_sort)
      result.send("#{key}=".to_sym,info)
    end

    # 设置基本工资
    base_salary = Salary.where(employee_id: @employee.id ,salary_type: "基本工资").first
    result.base_salary = base_salary.present? ? base_salary.amount : 0
    # 设置福利补助
    result.welfare_info = Salary.where("employee_id = #{@employee.id} and salary_type like '福利补助_%'").map{|v| v.salary_type.gsub(/^福利补助_/,"")}

    result
  end


  #内部类,指定结果返回值
  class Result
    attr_reader :employee,:employee_of_studio,:teacher_salary
    attr_accessor :fuwuguwen_salary,:chanpinzongjian_salary,:yunyingzongjian_salary,:shichangzongjian_salary,:shichangzhuanyuan_salary,:welfare_info,:base_salary

    #employee 基本信息
    def initialize(employee,employee_of_studio,teacher_salary)
      @employee = employee
      @employee_of_studio = employee_of_studio
      @teacher_salary = teacher_salary
    end 

  end


  private

  # 获取职位信息
  # {
  #     "position_id": "2",
  #     "position_name": "产品总监",
  #     "department_id": "2",
  #     "department_name": "财务部",
  #     "studio_infos": [
  #         {
  #             "studio_id": "1",
  #             "studio_name": "化工路店1111"
  #         },
  #         {
  #             "studio_id": "2",
  #             "studio_name": "祥云小镇个店333"
  #         }
  #     ],
  #     "teacher_level_name": "",
  #     "age_group": "请选择授课对象",
  #     "dance_type": "请选择舞种"
  # }
  def get_employee_of_studio_data

    employee_of_studio_datas = []
    employee_of_studios = EmployeeOfStudio.where(employee_id: @employee.id).select("group_concat(id) as ids,group_concat(studio_id) as studio_ids,position_id,department_id,position_name").group("position_id ,position_name, department_id")
    employee_of_studios.each do |employee_of_studio|
      data = {"teacher_level_name":"","age_group":"","dance_type":""}
      data["position_id"] = employee_of_studio.position_id
      data["position_name"] = employee_of_studio.position_name
      data["department_id"] = employee_of_studio.department_id
      data["department_name"] = DepartmentConfiguration.find(employee_of_studio.department_id).department_name
      data["position_id"] = employee_of_studio.position_id
      data["studio_infos"] = []
      Studio.where("id in (#{employee_of_studio.studio_ids})").each do |studio|
        data["studio_infos"] << {"studio_id"=> studio.id,"studio_name"=> studio.name}
      end

      if employee_of_studio.position_name=="导师"

        teacher = Teacher.where("employee_of_studio_id in (#{employee_of_studio.ids})").first

        if teacher.present?
          teacher_level_conf = teacher.teacher_level.present? ? TeacherLevelConfiguration.find_by_id(teacher.teacher_level) : nil

          data["teacher_level_name"] = teacher_level_conf.present? ? teacher_level_conf.teacher_level : "请选择等级"
          data["teacher_level"] = teacher_level_conf.present? ? teacher_level_conf.id : 0
          data["age_group"] = teacher.age_group
          data["dance_type"] = teacher.dance_type
        end
      end

      employee_of_studio_datas << data
    end
    employee_of_studio_datas
  end

  # 获取导师薪资信息
  # {
  #     "keshi_fee": "123",
  #     "ticheng_type": "阶段提成/固定提成",
  #     "ticheng_info": [
  #         {
  #             "start": "0",
  #             "end": "12",
  #             "scale": "12"
  #         }
  #     ],
  #     "chaochu_info": [
  #         {
  #             "start": "0",
  #             "end": "12",
  #             "scale": "23"
  #         }
  #     ],
  #     "sijiao_scale": "23"
  # }
  def get_teacher_salary(&info_sort)

    salary_info = {}

    teacher_salary = Salary.where("employee_id = :employee_id and salary_type like :salary_type",{:employee_id => @employee.id,:salary_type => "导师_%"})
    teacher_salary.each do |salary|
      if !salary_info.present?
        salary_info["keshi_fee"] = ""
        salary_info["ticheng_type"] = ""
        salary_info["chaochu_info"] = []
      end

      if salary.salary_type=="导师_课时费"

        salary_info["keshi_fee"] = salary.amount
      elsif salary.salary_type=="导师_阶段提成"

        salary_info["ticheng_type"] = "阶段提成"
        salary_info["ticheng_info"] = [] if !salary_info["ticheng_info"].present?
        salary_info["ticheng_info"] << {"start" => salary.range_min,"end" => salary.range_max,"scale" => salary.amount}
      elsif salary.salary_type=="导师_超出指标提成"
        
        salary_info["chaochu_info"] << {"start" => salary.range_min,"end" => salary.range_max,"scale" => salary.amount}
        # p "ceshi@@@@@@@@@@@@@@/@@@@@@@@@@@2#{salary_info["chaochu_info"]}"
      elsif salary.salary_type=="导师_私教提成"

        salary_info["sijiao_scale"] = salary.amount
      elsif salary.salary_type=="导师_固定提成"
        salary_info["ticheng_type"] = "固定提成"
        salary_info["ticheng_info"] = salary.amount
      end
    end
    info_sort.call(salary_info["ticheng_info"])
    info_sort.call(salary_info["chaochu_info"])

    salary_info
  end


  def get_ticheng_info(return_key,salary_type,&info_sort)

    salary_info = {"chaochu_info" => []}            
    teacher_salary = Salary.where(employee_id: @employee.id ,salary_type: salary_type)
    teacher_salary.each do |salary|
      salary_info["chaochu_info"] << {"start" => salary.range_min,"end" => salary.range_max,"scale" => salary.amount}
    end
    # 排序
    info_sort.call(salary_info["chaochu_info"])
    salary_info
  end

end
