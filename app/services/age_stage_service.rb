#根据中年,幼年年龄阶段生成对应的起止时间
class AgeStageService
  include HeiPisService

  #阶段名称
  def initialize(stage_name)
    @stage_name = stage_name
  end


  #执行方法 返回值
  def call

    age_info = {}

    case @stage_name
    when "幼儿" #0~6岁
        age_info = {:start_date => Date.current-6.year,:end_date => Date.current}
    when "少年" #6~12
        age_info = {:start_date => Date.current-12.year,:end_date => Date.current-6.year}
    when "青少年" #12~18
        age_info = {:start_date => Date.current-18.year,:end_date => Date.current-12.year}
    when "成年" #18~n
        age_info = {:start_date => Date.current-200.year,:end_date => Date.current-18.year}
    end
    
    age_info
  end

end
