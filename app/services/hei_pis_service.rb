#service 业务扩展基础模块 flydragon
module HeiPisService
  extend ActiveSupport::Concern

  #当此模块被mixin 时,定义类方法
  included do
    def self.call(*args)
      new(*args).call
    end


    # 获取指定时间段内的月数
    def months_for_dates(start_date,end_date)

      data = 0
      month_info = {}
      (start_date..end_date).each{|i|
        year = i.year
        month = i.month
        key = "#{year}_#{month}"
        if !month_info[key].present?
          month_info[key] = {}
          month_info[key][:day] = 1
          month_info[key][:days] = Time.days_in_month(month,year)
        else
          month_info[key][:day] += 1
        end
      }
    
      month_info.values.each{|k|
        data+= k[:day]/k[:days].to_f  
      }
      data
    end


    # 获取指定时间段内的月信息字符数组
    def month_strs_for_dates(start_date,end_date)

      months = []
      (start_date..end_date).each{|i|
        months << i.strftime("%Y-%m")
      }
      months.uniq
    end

  end

  # 列表记录结果类
  class DataTableResult

    attr_reader :totalRecord, :recordData

    #totalRecord 总记录数
    #recordData 记录数据
    def initialize(totalRecord, recordData)
      @totalRecord = totalRecord
      @recordData = recordData
    end 
  end


  # 操作结果累
  class OpResult

    attr_accessor :success, :msg , :data

    #success 成功表示
    #msg 结果提示
    def initialize(success, msg)
      @success = success
      @msg = msg
    end 
  end

end
