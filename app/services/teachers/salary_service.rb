#老师月工资服务类(只适用于月内的计算)  flydragon
class Teachers::SalaryService
  include HeiPisService

  #company 单位
  #studio 分店
  def initialize(teacher,salary,start_date,end_date)
    @teacher = teacher
    @salary = salary
    @start_date = start_date
    @end_date = end_date
  end


  #执行方法 返回值
  def call

    return 0 if !@teacher.present? || !@salary.present? || !@start_date.present? || !@end_date.present?

    filter_params = {:start_date => @start_date,:end_date => @end_date}
    
    # 员工入职时间
    joined_at = @teacher.created_at.to_datetime

    val_month = 1
    # 如果当前员工未入职,则工资为0
    if joined_at > @end_date
      return 0
    end

    if joined_at>@start_date
      @start_date = joined_at
    end

    val_month = months_for_dates(@start_date,@end_date)


    return 0 if !@salary.amount.present?

    # 还需要考虑  员工入职时间
    if @salary.salary_type=="基本工资"
      return @salary.amount*val_month
    elsif @salary.salary_type=="导师_课时费"

      # 获取导师签到的次数
      teacher_check_ins_size = CheckIn.where(teacher_id: @teacher.id).where("check_ins.created_at 
                between :start_date and :end_date",filter_params).size
      return @salary.amount*teacher_check_ins_size
    elsif @salary.salary_type=="导师_固定提成"

      # 本月导师授课id数组
      courses = Course.where(teacher_id: @teacher.id).where("courses.day_of_week between :start_date and :end_date",
                {:start_date => @start_date.strftime("%Y-%m-%d"),:end_date => @end_date.strftime("%Y-%m-%d")})

      student_count = CheckIn.where(course_id: courses.map(&:id)).where(teacher_id: nil).size
      p "@salary:#{@salary.to_json},student_count:#{student_count}"
      return @salary.amount * student_count
    elsif @salary.salary_type=="导师_阶段提成"

      # 本月导师授课id数组
      courses = Course.where(teacher_id: @teacher.id).where("courses.day_of_week between :start_date and :end_date",
                {:start_date => @start_date.strftime("%Y-%m-%d"),:end_date => @end_date.strftime("%Y-%m-%d")})

      student_count = CheckIn.where(course_id: courses.map(&:id)).where(teacher_id: nil).size

      return @salary.amount * get_vali_count(student_count,@salary.range_min,@salary.range_max)
      # tudo
    elsif @salary.salary_type=="导师_超出指标提成" 
      
      # 课消超出指标
      course_reckon = CourseReckon.where(teacher_id: @teacher.id).sum(:amount).to_f

      return (@salary.amount/100) * get_vali_count(course_reckon,@salary.range_min,@salary.range_max)
    elsif @salary.salary_type=="导师_私教提成" 

      #私教课程课时费的提成
      #获取课时费
      keshi_salary = @salary.employee.salaries.where(salary_type: '导师_课时费').first
      if keshi_salary.present?
 
        filter_params[:teacher_id] = @teacher.id

        sql = %Q{

          SELECT 
              count(check_ins.id) as check_in_count
          FROM
              courses
                  JOIN
              course_infos ON courses.course_info_id = course_infos.id
                  JOIN
              check_ins ON check_ins.course_id = courses.id
          where courses.teacher_id = :teacher_id  
          and (check_ins.created_at between :start_date and :end_date) 
          and course_infos.course_type = '私教'
        }

        # 获取导师签到的次数
        teacher_check_ins_size = 0
        Course.find_by_sql([sql,filter_params]).each{|data| teacher_check_ins_size = data.check_in_count}

        return keshi_salary.amount*teacher_check_ins_size*(@salary.amount/100.to_f) 
      else
        return 0
      end
    elsif @salary.salary_type=="福利补助_全勤奖" 
      return @salary.amount
    else
      Rails.logger.debug "导师未识别工资信息为:#{@salary.to_json}"
      return 0
    end
    
  end


  def get_vali_count(count,start_count,end_count)

    # 未达标时,返回有效数量为0
    return 0 if start_count>count

    return (end_count-start_count) > (count-start_count) ? (count-start_count) : (end_count-start_count)
  end


end
