class SchedulerFixed
  include Sidekiq::Worker
  sidekiq_options queue: 'fixed'

  def perform
    fixed_costs =  FixedCost.all.select  { |fc| fc if(fc.created_at > Time.now - fc.cycle.month)}
    fixed_costs.each do |fixed|
      amount = fixed.amount.to_f/fixed.studios.size
      fixed.studios do |stu|
        finance = Finance.create(finance_type: "支出", finance_category: '固定成本', amount: amount, remark: fixed.name, studio_id: stu.id, company_id: stu.company_id, is_system: true)
      end
    end
  end
end
