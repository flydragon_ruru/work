# 发送邮件
class EmailWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'email'

  def perform(email_type, user_id)
    Maybe(User.find_by(id: user_id)) >-> user {
      email_method = [email_type, '_email'].join
      UserMailer.method_defined?(email_method) && UserMailer.send(email_method, user).deliver_now
    }
  end
end
