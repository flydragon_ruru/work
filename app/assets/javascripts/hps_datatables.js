
//黑皮士表格异步加载
function HpsDataTables(config){

  //分页数
  this.pagesize = config.pagesize;
  //容器id
  this.dataContainerId = typeof(config.dataContainerId)=="string" ? config.dataContainerId : hps_default_datatable_id(); 
  
  //异步请求url
  this.dataSource = $("#"+this.dataContainerId).attr("data_source");
  //lie
  this.columns = config.columns;
  //default request type is get
  this.ajaxType = typeof(config.ajaxType)=="string" ? config.ajaxType : "get";
  this.params = typeof(config.params)=="object" ? config.params : {};
  this.params.page = 1
  this.params.pagesize = this.pagesize

  console.log("dataContainerId:"+this.dataContainerId);

  if (typeof HpsDataTables._initialized == "undefined") {

  	//刷新
    HpsDataTables.prototype.reload = function(req_params) {
        console.log("数据列表重新加载,参数为:"+JSON.stringify(req_params));

        if(typeof(req_params)!="object") req_params={};
        req_params.page = 1;
        req_params.pagesize = this.pagesize;
        this.params = req_params;
        this.doAjax(true);
    };

    //执行渲染
    HpsDataTables.prototype.dataRender = function(record) {

    	// console.log("render start dataContainerId:"+typeof(this));

        // console.log("数据列表render start,参数为:"+JSON.stringify(record));

    	var totalRecord = record.totalRecord;
    	var recordData = record.recordData;
        var bodyHtml = "";
        var dataColumns = this.columns;

        //fetch the data to html_str
        $(recordData).each(function(index,data){
            var html_tmp = "<tr>"
        	// console.log("rend data index:"+index+",data:"+JSON.stringify(data));
            $(dataColumns).each(function(col_i,col){
            	// console.log("col:"+col);
                html_tmp += "<td>"+data[col.data]+"</td>"
            });
            html_tmp += "</tr>"
            bodyHtml += "\n"+ html_tmp
        });

        $("#"+this.dataContainerId+" table tbody").html(bodyHtml);
       
    	console.log("数据列表render end ,info:"+"#"+this.dataContainerId+" table tbody");

    };

    //totalRecord 总记录
    //isClearPag 分页是否清理
    HpsDataTables.prototype.pageRender = function(totalRecord,isClearPag) {

    	  var request_params = this.params;
    	  var doAjax = this.doAjax;
        var currentObj = this;

    	  if(isClearPag){

    		  $("#"+this.dataContainerId+" #pageTool").remove();
          console.log("分页清除,length:"+$("#"+this.dataContainerId+" #pageTool").length);
    	  }

        if($("#"+this.dataContainerId+" #pageTool").length==0){
            $("#"+this.dataContainerId).append("<div id='pageTool' class='apple_pagination'></div>");
            // console.log("totalRecord:"+totalRecord);
            $('#pageTool').Paging({pagesize:this.pagesize,count:totalRecord,callback:function(page,size,count){
		        console.log('当前第 ' +page +'页,每页 '+size+'条,总页数：'+count+'页');
		        request_params.page = page;
		        doAjax.apply(currentObj,[false]);
	        }});
        }
    };


    //params request value
    HpsDataTables.prototype.doAjax = function(isClearPag) {

        console.log("doAjax start dataContainerId:"+this.dataContainerId);
    	
    	  var dataRender = this.dataRender;
        var pageRender = this.pageRender;
        var currentObj = this;

        if(this.ajaxType=="post"){

            $.post(this.dataSource,this.params,function(data,status){
                console.log("Data: " + data + "\nStatus: " + status);
                dataRender.apply(currentObj, [data]);
                pageRender.apply(currentObj, [data.totalRecord,isClearPag]);
            });
        }else{
        	$.get(this.dataSource,this.params,function(data,status){
                // console.log("Data: " + JSON.stringify(data) + "\nStatus: " + status+"\nisClearPag:"+isClearPag);
                dataRender.apply(currentObj,[data]);
                pageRender.apply(currentObj,[data.totalRecord,isClearPag]);
            });
        }
    };

    //
    HpsDataTables.prototype._init = function() {
        this.doAjax(false);
    };

    HpsDataTables._initialized = true;
  }
  this._init();
}