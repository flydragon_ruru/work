
//人资新建初始化方法
function hps_potential_students_new_init(){

  qiniu_init();
}

//人资新建初始化方法
function hps_potential_students_edit_init(){

  qiniu_init();
}

//七牛云初始化
function qiniu_init(){


  var uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',
        browse_button: 'upload_avatar',
        container: 'upload_avatar_container',
        drop_element: 'upload_avatar_container',
        max_file_size: '1000mb',
        flash_swf_url: '/assets/plupload/sMoxie.swf',
        dragdrop: true,
        chunk_size: '4mb',
        multi_selection: !(mOxie.Env.OS.toLowerCase()==="ios"),
        uptoken_url: "/qiniu/uptoken",
        domain: $('#qiniu_domain').val(),
        get_new_uptoken: false,
        auto_start: true,
        log_level: 5,
        init: {
            'FilesAdded': function(up, files) {
                $('table').show();
                $('#success').hide();
                plupload.each(files, function(file) {

                  console.log("等待...  FilesAdded");
                    // var progress = new FileProgress(file, 'fsUploadProgress');
                    // progress.setStatus("等待...");
                    // progress.bindUploadCancel(up);
                });
            },
            'BeforeUpload': function(up, file) {
                
              console.log("BeforeUpload");
            },
            'UploadProgress': function(up, file) {
              console.log("UploadProgress");
            },
            'UploadComplete': function() {
              $('#success').show();
              console.log("上传成功!, UploadComplete");
            },
            'FileUploaded': function(up, file, info) {

              // 每个文件上传成功后，处理相关的事情
               // 其中info是文件上传成功后，服务端返回的json，形式如：
               // {
               //    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
               //    "key": "gogopher.jpg"
               //  }
               //查看简单反馈
               var domain = up.getOption('domain');
               var res = JSON.parse(info);
               var sourceLink = domain +"/"+ res.key; //获取上传成功后的文件的Url
               console.log("上传成功,链接为:"+sourceLink);

               $("#potential_student_avatar").val(sourceLink);

               $.post("/qiniu/private_url",{url:sourceLink},function(data,status){
                    if(status == "success"){
                      $("#potential_student_avatar_img").attr("src",data.url);
                    }else{
                      console.log("获取服务私密链接失败,status:"+status);
                    }
                });
            },
            'Error': function(up, err, errTip) {
               console.log("上传失败!");
            }
            ,
            'Key': function(up, file) {

              // console.log("up:"+JSON.stringify(up));
              // console.log("file:"+JSON.stringify(file));
              var key = $("#current_controller_name").val()+"/"+file.name;
              // do something with key
              return key
            }
        }
    });
}