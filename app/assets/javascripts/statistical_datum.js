
function hps_statistical_datum_resource_init(){

   var studio_name = $("#studio_name").val();
   $("#studio_select .downbtn span").text(studio_name);
   $("#studio_select .dropdown-menu li").click(function(){
     location.href = "/admin/statistical_datum/resource?studio_id="+$(this).attr("_studio_id");
   });
}