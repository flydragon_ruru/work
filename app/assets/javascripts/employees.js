
//人资新建初始化方法
function hps_employees_new_init(){
        
    var uploader = new HpsQiuNiuUploader({
    FileUploaded_callback: function(up, file, info){

       //查看简单反馈
           var domain = up.getOption('domain');
           var res = JSON.parse(info);
           var sourceLink = domain +"/"+ res.key; //获取上传成功后的文件的Url
           console.log("上传成功,链接为:"+sourceLink);

           $("#employee_avatar").val(sourceLink);

           $.post("/qiniu/private_url",{url:sourceLink},function(data,status){
                if(status == "success"){
                  $("#employee_avatar_img").attr("src",data.url);
                }else{
                  console.log("获取服务私密链接失败,status:"+status);
                }
            });
    }
  }).init();
}


//人资管理编辑页面初始化
function hps_employees_edit_init(){

  //时间选择空间初始化
  $('.date-input').date_input();

  default_select_init();
  default_check_init();
  // default_radio_init();

  var employee_id = $("#employee_id").val();
  var employeeBasicInfo;
  var employeePositionInfo;
  var teacherSalaries;
  // 服务顾问
  var fuwuguwenSalaries;
  // 产品总监
  var chanpinzongjianSalaries;

  // 运营总监
  var yunyingzongjianSalaries;
  // 市场总监
  var shichangzongjianSalaries;
  // 市场专员
  var shichangzhuanyuanSalaries;

  // 福利补助
  var welfareSalaries;
  
  $.get("/admin/employees/"+employee_id+"/edit.json",{},function(data,status){
      console.log("回现数据请求信息,Data: " + data + "\nStatus: " + status);
      if(status=="success"){
        console.log("回现数据成功!");
        console.log("回现导师数据:"+JSON.stringify(data.teacher_salary));
        console.log("回现导师数据:"+JSON.stringify(data.employee_of_studio));
        employeeBasicInfo = new EditEmployeeBasicInfo(data.employee);
        employeePositionInfo = new EditEmployeePositionInfo(data.employee_of_studio);
        // var data = {"keshi_fee":"","ticheng_type":"阶段提成","ticheng_info":[{"start":"0","end":"12","scale":"12"},{"start":"13","end":"24","scale":"23"}],"chaochu_info":[{"start":"0","end":"12","scale":"23"},{"start":"13","end":"23","scale":"2323"}],"sijiao_scale":"23"};
        teacherSalaries = new EditEmployeeTeacherSalaries(data.teacher_salary);
        fuwuguwenSalaries = new EditEmployeeChaoChuSalaries("fuwuguwen_salaries",data.fuwuguwen_salary);
        chanpinzongjianSalaries = new EditEmployeeChaoChuSalaries("chanpinzongjian_salaries",data.chanpinzongjian_salary);
        yunyingzongjianSalaries = new EditEmployeeChaoChuSalaries("yunyingzongjian_salaries",data.yunyingzongjian_salary);
        shichangzongjianSalaries = new EditEmployeeChaoChuSalaries("shichangzongjian_salaries",data.shichangzongjian_salary);
        shichangzhuanyuanSalaries = new EditEmployeeChaoChuSalaries("shichangzhuanyuan_salaries",data.shichangzhuanyuan_salary);
        welfareSalaries = new EditEmployeeWelfareSalaries(data.welfare_info);

        // 设置基本工资
        $("#basePay").val(data.base_salary);
        
      }else{
        console.log("编辑失败,信息为:"+JSON.stringify(data));
      }
  });
  
  // //薪资初始化
  // var teacher_salaries = new EditEmployeeTeacherSalaries({});

  // teacher_salaries.init();

  //提交按钮监听
  $("#save_submit").click(function(){
    console.log("提交按钮提交!");
    console.log("基本信息为:"+JSON.stringify(employeeBasicInfo.get_value()));
    console.log("职务信息为:"+JSON.stringify(employeePositionInfo.get_value()));
    console.log("导师薪资信息为:"+JSON.stringify(teacherSalaries.get_value()));

    console.log("服务顾问薪资信息为:"+JSON.stringify(fuwuguwenSalaries.get_value()));
    console.log("产品总监薪资信息为:"+JSON.stringify(chanpinzongjianSalaries.get_value()));

    console.log("运营总监薪资信息为:"+JSON.stringify(yunyingzongjianSalaries.get_value()));
    console.log("市场总监薪资信息为:"+JSON.stringify(shichangzongjianSalaries.get_value()));
    console.log("市场专员薪资信息为:"+JSON.stringify(shichangzhuanyuanSalaries.get_value()));

    console.log("基本工资:"+$("#basePay").val());
    console.log("福利补助:"+JSON.stringify(welfareSalaries.get_value()));

    if(!employeeBasicInfo.validate()){
      console.log("基本信息验证未通过!");
      return;
    }
    

    var postData = {};
    postData.employee = employeeBasicInfo.get_value();
    postData.employee_of_studio = JSON.stringify(employeePositionInfo.get_value());
    // 导师薪资
    postData.teacher_salary = JSON.stringify(teacherSalaries.get_value());

    // 服务顾问薪资
    postData.fuwuguwen_salary = JSON.stringify(fuwuguwenSalaries.get_value());
    // 产品总监薪资
    postData.chanpinzongjian_salary = JSON.stringify(chanpinzongjianSalaries.get_value());
    // 运营总监薪资
    postData.yunyingzongjian_salary = JSON.stringify(yunyingzongjianSalaries.get_value());
    // 市场总监薪资
    postData.shichangzongjian_salary = JSON.stringify(shichangzongjianSalaries.get_value());
    // 市场专员薪资
    postData.shichangzhuanyuan_salary = JSON.stringify(shichangzhuanyuanSalaries.get_value());
    // 福利补助
    postData.welfare_salary = JSON.stringify(welfareSalaries.get_value());

    // 基本工资
    postData.base_salary = $("#basePay").val();

    $.ajax({ url: "/admin/employees/"+employee_id,
      type: "PATCH",
      data: postData ,
      success: function(data){
        if(data.success){
          location.href = data.data
        }else{
          console.log("编辑失败,信息为:"+JSON.stringify(data));
          alter("编辑失败!")
        }
      },
      error: function(data){
        console.log("编辑失败,信息为:"+JSON.stringify(data));
      }
    });

  });

}


//导师薪资
function EditEmployeeTeacherSalaries(info){

  this.keshi_fee = info.keshi_fee;//课时费
  this.ticheng_type = info.ticheng_type;//提成类型
  this.ticheng_info = info.ticheng_info;//提成内容
  this.chaochu_info = info.chaochu_info==undefined||info.chaochu_info==null ? [] : info.chaochu_info;//超出提成比例
  this.sijiao_scale = info.sijiao_scale //私教比例

  if (typeof EditEmployeeTeacherSalaries._initialized == "undefined") {
    //导师界面初始化
    EditEmployeeTeacherSalaries.prototype.init = function() {

      //单选按钮监听
      $("#teacher_salaries .ra-btn").click(function(){
         
         var slaries_type = $(this).siblings("input:radio").val();
         change_radio_by_salaries_type(slaries_type);
         change_ticheng_content(slaries_type);
      });

      //设置课时费用
      $("#teacher_salaries #keshi_fee").val(this.keshi_fee);
      //设置提成类型
      change_ticheng_content(this.ticheng_type)

      //添加阶段提成比例
      $("#jieduan_add").click(function(){
        
        var start_val = $("#teacher_salaries #jieduan_edit :input[name='start']").val();
        var end_val = $("#teacher_salaries #jieduan_end").val();
        var jieduan_scale = $("#teacher_salaries #jieduan_scale").val();
        
        var data = {'start': start_val,'end': end_val,'scale': jieduan_scale};
        console.log("添加的阶段提成为:"+JSON.stringify(data));
        
        $("#teacher_salaries #jieduan_edit .hps_required").removeClass("hps_required");

        var flag = true;

        if(end_val==null || end_val=="" || parseInt(start_val)>parseInt(end_val)){
          $("#teacher_salaries #jieduan_end").css("border", "1px solid rgb(255, 90, 96)");
          flag = false;
        }else{
          $("#teacher_salaries #jieduan_end").css("border", "1px solid rgb(171, 173, 193)");
        }


        if(jieduan_scale==null || jieduan_scale==""){
          $("#teacher_salaries #jieduan_scale").css("border", "1px solid rgb(255, 90, 96)");
          flag = false;
        }else{
          $("#teacher_salaries #jieduan_scale").css("border", "1px solid rgb(171, 173, 193)");
        }

        if(!flag) return flag;
        
        add_jieduan_ticheng(data);
      });


      //添加超出提成按钮监听
      $("#teacher_salaries #out_zhibiao_op .comm-add").click(function(){

        var start_val = $("#teacher_salaries #out_zhibiao_op :input[name='start']").val();
        var end_val = $("#teacher_salaries #out_zhibiao_op :input[name='end']").val();
        var jieduan_scale = $("#teacher_salaries #out_zhibiao_op :input[name='scale']").val();
        
        var data = {'start': start_val,'end': end_val,'scale': jieduan_scale};
        console.log("添加的超出提成为:"+JSON.stringify(data));
        
        var flag = true;

        if(end_val==null || end_val=="" || parseInt(start_val)>parseInt(end_val)){
          $("#teacher_salaries #out_zhibiao_op :input[name='end']").css("border", "1px solid rgb(255, 90, 96)");
          flag = false;
        }else{
          $("#teacher_salaries #out_zhibiao_op :input[name='end']").css("border", "1px solid rgb(171, 173, 193)");
          
        }

        if(jieduan_scale==null || jieduan_scale==""){
          $("#teacher_salaries #out_zhibiao_op :input[name='scale']").css("border", "1px solid rgb(255, 90, 96)");
          flag = false;
        }else{
          $("#teacher_salaries #out_zhibiao_op :input[name='scale']").css("border", "1px solid rgb(171, 173, 193)");
        }
        
        if(!flag) return flag;
        

        add_out_ticheng(data);
      });

      this.ticheng_init();
    }

    //导师提成初始化数据
    EditEmployeeTeacherSalaries.prototype.ticheng_init = function() {
      change_radio_by_salaries_type(this.ticheng_type);
      change_ticheng_content(this.ticheng_type);
      if(this.ticheng_type=="固定提成"){
        $("#teacher_salaries #guding_fee").val(this.ticheng_info);
      }else if (this.ticheng_type=="阶段提成"){
        this.ticheng_info.forEach(function(data){
          add_jieduan_ticheng(data);
        });
      }

      this.chaochu_info.forEach(function(data){
        add_out_ticheng(data);
      });
      
      $("#teacher_salaries #personal").val(this.sijiao_scale);
    }


    //导师薪资数据
    EditEmployeeTeacherSalaries.prototype.get_value = function() {
      var salary_info = {};

      if($("#teacher_salaries").attr("data_status")!="1"){
         return salary_info;
      }

      salary_info.keshi_fee = $("#teacher_salaries #keshi_fee").val();
      salary_info.ticheng_type = $("#teacher_salaries #ticheng_type").val();
      salary_info.ticheng_info = []
      if(salary_info.ticheng_type=="固定提成"){
        salary_info.ticheng_info = $("#teacher_salaries #guding_fee").val();
      }else if(salary_info.ticheng_type=="阶段提成"){
        //获取职务信息数据
        $("#teacher_salaries input[name='jieduan_info']").each(function(){
          if(this.value!=null && this.value!=""){
            salary_info.ticheng_info.push(JSON.parse(this.value));
          }
        });
      }

      salary_info.chaochu_info = []
      //获取职务信息数据
      $("#teacher_salaries input[name='out_zhibiao_info']").each(function(){
        if(this.value!=null && this.value!=""){
          salary_info.chaochu_info.push(JSON.parse(this.value));
        }
      });

      salary_info.sijiao_scale = $("#teacher_salaries #personal").val();
      return salary_info;
    }


    // 添加超出提成
    function add_out_ticheng(out_info){

      if(typeof(out_info)!="object") return;

      var temp = $("#teacher_salaries #out_zhibiao_op #temp").clone();
      temp.find(".start").text(out_info.start);
      temp.find(".end").text(out_info.end);
      temp.find(".scale").text(out_info.scale);
      temp.find(".comm-del").click(function(){

        var last_dom = $(this).parent().parent().prev();
        var last_dom_val = last_dom.find("#out_zhibiao_info").val();
        var start = 0;
        if (last_dom_val!=""){
          var info_val = $(this).parent().parent().find("#out_zhibiao_info").val();
          start = JSON.parse(info_val).start;
          last_dom.find(".comm-del").show();
        }
        
        $("#teacher_salaries #out_zhibiao_edit :input[name='start']").val(start);
        $("#teacher_salaries #out_zhibiao_edit .stage-box").text(start);
        $(this).parent().parent().remove();
      });
      temp.removeAttr("id");
      // 设置超出提成的信息
      temp.find("#out_zhibiao_info").val(JSON.stringify(out_info));
      temp.show();


      $("#teacher_salaries #out_zhibiao_edit").before(temp);
      $("#teacher_salaries #out_zhibiao_edit .stage-box").text(parseInt(out_info.end)+1);
      $("#teacher_salaries #out_zhibiao_edit :input[name='start']").val(parseInt(out_info.end)+1);

      var last_dom = $("#teacher_salaries #out_zhibiao_edit").prev().prev();
      if (last_dom.find("#out_zhibiao_info").val()!=""){
        last_dom.find(".comm-del").hide();
      }
    }

    //添加阶段提成
    function add_jieduan_ticheng(jieduan_info){

      if(typeof(jieduan_info)!="object") return;

      console.log("添加阶段提成:"+JSON.stringify(jieduan_info));


      var temp = $("#teacher_salaries #jieduan_show_temp").clone();
      temp.find(".start").text(jieduan_info.start);
      temp.find(".end").text(jieduan_info.end);
      temp.find(".scale").text(jieduan_info.scale);
      temp.find(".comm-del").click(function(){

        // var last_dom = $(this).parent().parent().prev();
        // if (last_dom.find("#jieduan_info").val()!=""){
        //   last_dom.find(".comm-del").show();
        // }
        var last_dom = $(this).parent().parent().prev();
        var last_dom_val = last_dom.find("#jieduan_info").val();
        console.log("职位:"+last_dom_val);
        var start = 0;
        if (last_dom_val!=""){
          var info_val = $(this).parent().parent().find("#jieduan_info").val();
          start = JSON.parse(info_val).start;
          last_dom.find(".comm-del").show();
        }

        $("#teacher_salaries #jieduan_edit :input[name='start']").val(start);
        $("#teacher_salaries #jieduan_edit .stage-box").text(start);

        $(this).parent().parent().remove();
      });
      temp.removeAttr("id");
      temp.find("#jieduan_info").val(JSON.stringify(jieduan_info));
      temp.show();

      $("#teacher_salaries #jieduan_edit").before(temp);
      $("#teacher_salaries #jieduan_edit .stage-box").text(parseInt(jieduan_info.end)+1);
      $("#teacher_salaries #jieduan_edit :input[name='start']").val(parseInt(jieduan_info.end)+1);
      var last_dom = $("#teacher_salaries #jieduan_edit").prev().prev();
      if (last_dom.find("#jieduan_info").val()!=""){
        last_dom.find(".comm-del").hide();
      }
    }


    //改变提成的内容编辑部分的展示和隐藏
    function change_ticheng_content(slaries_type){

      $("#teacher_salaries #ticheng_type").val(slaries_type);

      if(slaries_type=="固定提成"){
        $("#teacher_salaries #guding_op").show();
        $("#teacher_salaries #jieduan_op").hide();
      }else if(slaries_type=="阶段提成"){
        $("#teacher_salaries #guding_op").hide();
        $("#teacher_salaries #jieduan_op").show();
      }else{
        $("#teacher_salaries #guding_op").hide();
        $("#teacher_salaries #jieduan_op").hide();
      }
    }

    //根据提成类型改变单选框的样式
    function change_radio_by_salaries_type(slaries_type){

      $("#teacher_salaries input:radio").each(function(){

        if(this.value==slaries_type){
          $(this).attr("checked",true).siblings(".ra-btn").find("span").show();
        }else{
          $(this).attr("checked",false).siblings(".ra-btn").find("span").hide();
        }
      });
    }

    EditEmployeeTeacherSalaries._initialized = true;
  }

  this.init();
}






// 基本信息
function EditEmployeeBasicInfo(basic_info){

  // console.log("信息为:"+JSON.stringify(basic_info));

  // 头像 
  this.avatar = nil_to_blank(basic_info.avatar);
  this.avatar_url = nil_to_blank(basic_info.avatar_url);
  this.name = nil_to_blank(basic_info.name);
  this.alias_name = nil_to_blank(basic_info.alias_name);
  this.sex = nil_to_blank(basic_info.sex);
  this.weixin= nil_to_blank(basic_info.weixin);
  this.mobile = nil_to_blank(basic_info.mobile);
  this.birthday = nil_to_blank(basic_info.birthday);
  this.emergency_contact = nil_to_blank(basic_info.emergency_contact);
  this.emergency_mobile = nil_to_blank(basic_info.emergency_mobile);
  this.address = nil_to_blank(basic_info.address);
  this.remark = nil_to_blank(basic_info.remark);

  // 初始化七牛云上传
  var uploader = new HpsQiuNiuUploader({
    FileUploaded_callback: function(up, file, info){

      //查看简单反馈
      var domain = up.getOption('domain');
      var res = JSON.parse(info);
      var sourceLink = domain +"/"+ res.key; //获取上传成功后的文件的Url
      console.log("上传成功,链接为:"+sourceLink);

      $("#employee_avatar").val(sourceLink);
      $.post("/qiniu/private_url",{url:sourceLink},function(data,status){
        if(status == "success"){
          $("#employee_avatar_img").attr("src",data.url);
        }else{
          console.log("获取服务私密链接失败,status:"+status);
        }
      });
    }
  }).init();


  if (typeof EditEmployeeBasicInfo._initialized == "undefined") {
    // 数据初始化
    EditEmployeeBasicInfo.prototype.init = function() {

      console.log("图片地址:"+this.avatar_url);
      
      $("#employee_basic #employee_avatar").val(this.avatar);
      $("#employee_basic #employee_avatar_img").attr("src",this.avatar_url);
      $("#employee_basic #employee_name").val(this.name);
      $("#employee_basic #employee_alias_name").val(this.alias_name);
      $("#employee_basic #employee_sex .downbtn span").text(this.sex)
      $("#employee_basic #employee_weixin").val(this.weixin);
      $("#employee_basic #employee_mobile").val(this.mobile);
      $("#employee_basic #employee_birthday").val(this.birthday);
      $("#employee_basic #employee_emergency_contact").val(this.emergency_contact);
      $("#employee_basic #employee_emergency_mobile").val(this.emergency_mobile);
      $("#employee_basic #employee_address").val(this.address);
      $("#employee_basic #employee_remark").val(this.remark);
    }

    // 同步dom值到对象
    EditEmployeeBasicInfo.prototype.syn_val = function(obj) {

      obj.avatar = $("#employee_basic #employee_avatar").val();
      obj.avatar_url = $("#employee_basic #employee_avatar_img").attr("src");
      obj.name = $("#employee_basic #employee_name").val();
      obj.alias_name = $("#employee_basic #employee_alias_name").val();
      obj.sex = $("#employee_basic #employee_sex .downbtn span").text()
      obj.weixin = $("#employee_basic #employee_weixin").val();
      obj.mobile = $("#employee_basic #employee_mobile").val();
      obj.birthday = $("#employee_basic #employee_birthday").val();
      obj.emergency_contact = $("#employee_basic #employee_emergency_contact").val();
      obj.emergency_mobile = $("#employee_basic #employee_emergency_mobile").val();
      obj.address = $("#employee_basic #employee_address").val();
      obj.remark = $("#employee_basic #employee_remark").val();
      console.log("emergency_mobile_syn_val:"+obj.emergency_mobile)
      return obj;
    }

    // 值验证
    EditEmployeeBasicInfo.prototype.validate = function() {
      this.syn_val(this);

      var flag = true;

      if (!change_border(this.name,"employee_name")) flag = false;
      if (!change_border(this.weixin,"employee_weixin")) flag = false;
      if (!change_border(this.mobile,"employee_mobile")) flag = false;
      if (!change_border(this.birthday,"employee_birthday")) flag = false;
      if (!change_border(this.emergency_contact,"employee_emergency_contact")) flag = false;
      if (!change_border(this.emergency_mobile,"emergency_mobile")) flag = false;
      if (!change_border(this.address,"employee_address")) flag = false;


      console.log("验证标示:"+flag);

      return flag;
    }

    // 获取值
    EditEmployeeBasicInfo.prototype.get_value = function() {
      this.syn_val(this);
      var data_val = {};
      this.syn_val(data_val);
      console.log("emergency_mobile_get_value:"+data_val.emergency_mobile)
      return data_val;
    };

    EditEmployeeBasicInfo._initialized = true;
  }

  
  // 如果为空的话,赋予空字符串
  function nil_to_blank(val){
    return typeof(val)=="string" ? val : "";
  }

  // 判断是否为空
  function is_blank(val){
     return (val==null || val==undefined || val=="");
  }

  function change_border(val,dom_id){

    var flag = true;
    if(is_blank(val)){
      $("#employee_basic #"+dom_id+"").css("border", "1px solid rgb(255, 90, 96)");
      flag = false;
    }

    if(dom_id == "employee_birthday" && flag && (parseInt(val.replace(/-0|-/g,"")) >= get_current_date())){
      $("#employee_basic #"+dom_id+"").css("border", "1px solid rgb(255, 90, 96)");
      flag = false;
    }

    if(flag){
      $("#employee_basic #"+dom_id+"").css("border", "1px solid rgb(171, 173, 193)");
    }

    return flag;
  }

  function get_current_date(){
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    return parseInt(date.getFullYear()+""+month+""+strDate);
  }

  // 执行初始化
  this.init();
}






// 职务信息
function EditEmployeePositionInfo(position_infos){

  this.position_infos = typeof(position_infos)=="object" ? position_infos : [];
  
  
  if (typeof EditEmployeePositionInfo._initialized == "undefined") {
    // 数据初始化
    EditEmployeePositionInfo.prototype.init = function() {
      console.log("职务信息初始化!");
      // 下拉框监听初始化
      this.select_click_init();
      this.add_click_init();

      var currentObj = this;

      this.position_infos.forEach(function(position){
        currentObj.add_position_info(position);
      });
    }

    // 下拉框初始化
    EditEmployeePositionInfo.prototype.select_click_init = function() {
      $("#position_select .dropdown-menu li").click(function(){

        var sp = $(this);
        console.log("职务id:"+sp.attr("data_id")+",职务名称:"+sp.text());
        if(sp.text()=="导师"){
          $(".p-t").show();
        }else{
          $(".p-t").hide();
        }

        // 财务或hr默认选中所有门店
        if(sp.text()=="财务总监" || sp.text()=="会计/出纳" || sp.text()=="人资行政总监" || sp.text()=="HR/行政"){
          $(".pos-branch .check").addClass("ad-checked disabled").attr("hps-readonly","1").find("span").show();
        }
      });
    }

    // 添加按钮初始化
    EditEmployeePositionInfo.prototype.add_click_init = function() {

      var currentObj = this;

      //职务添加按钮监听
      $("#careerInfo").click(function(){

        //获取授课对象
        var age_group = $("#age_group_select .downbtn span").text();
        //获取舞种
        var dance_type = $("#dance_type_select .downbtn span").text();
        //获取导师等级
        var teacher_level_id = $("#teacher_level_select .downbtn span").attr("data_id");
        var teacher_level_name = $("#teacher_level_select .downbtn span").text();
        
        //获取选择门店的信息
        var studio_infos = [];
        $(".pos-branch .ad-checked").parent().parent().parent().each(function(){

          var studio_info = {};
          studio_info.studio_id = $(this).children("span").attr("data_id");
          studio_info.studio_name = $(this).children("span").text();
          studio_infos.push(studio_info);
        });

        var position_info = {};
        position_info.position_id = $("#position_select .downbtn span").attr("data_id");
        position_info.position_name = $("#position_select .downbtn span").text();
        position_info.department_id = $("#department_select .downbtn span").attr("data_id");
        position_info.department_name = $("#department_select .downbtn span").text();
        position_info.studio_infos = studio_infos;
        if(position_info.position_name=="导师"){
          position_info.teacher_level = teacher_level_id;
          position_info.teacher_level_name = teacher_level_name;
          position_info.age_group = age_group;
        }
        
        // position_info.dance_type = dance_type;

        if(studio_infos.length==0){
          $(".not-addpos").show();
          return;
        }else{
          $(".not-addpos").hide();
        }

        //添加数据验证
        if(!_add_positon_validate(position_info)) return;

        console.log("职务信息为:"+JSON.stringify(position_info));
        // add_position_info(position_info);
        currentObj.add_position_info(position_info);

        if(position_info.position_name=="导师"){
          $(".p-t").hide();
        }

        // add_position_info.apply(currentObj,[position_info]);
      });
    }

    // 添加职务信息
    EditEmployeePositionInfo.prototype.add_position_info = function(position_info) {

      // console.log("职务信息为WWWWWWWWWWWWWWWWWWWWWWWWWWWWW:"+JSON.stringify(position_info));

      var position_show_temp = $("#position_show_model").clone();
      var currentObj = this;
      position_show_temp.find(".comm-del").click(function(){
        $(this).parent().remove();
        $("#position_select .dropdown-menu").append("<li data_id='"+position_info.position_id+"'>"+position_info.position_name+"</li>");

        // 根据导师控制导师信息界面的展示
        if(position_info.position_name=="导师"){
          $("#teacher_salaries").hide().attr("data_status","0");
        }else if (position_info.position_name=="服务顾问") {
          $("#fuwuguwen_salaries").hide().attr("data_status","0");
        }else if (position_info.position_name=="产品总监") {
          $("#chanpinzongjian_salaries").hide().attr("data_status","0");
        }else if (position_info.position_name=="运营总监") {
          $("#yunyingzongjian_salaries").hide().attr("data_status","0");
        }else if (position_info.position_name=="市场总监") {
          $("#shichangzongjian_salaries").hide().attr("data_status","0");
        }else if (position_info.position_name=="市场专员") {
          $("#shichangzhuanyuan_salaries").hide().attr("data_status","0");
        }

        currentObj.select_click_init();
      });
      position_show_temp.removeAttr("id");
      position_show_temp.show();
      position_show_temp.find("#positon_info").val(JSON.stringify(position_info));
      position_show_temp.find("#position_info_model").remove();
      
      //门店名称
      var studio_names = [];
      position_info.studio_infos.forEach(function(studio){
        studio_names.push(studio.studio_name);
      });

      //设置部门和职务
      _add_position_label_temp(position_show_temp,
        {name: "部门名称:",value: position_info.department_name},
        {name: "职务名称:",value: position_info.position_name});

      //设置门店和授课对象
      _add_position_label_temp(position_show_temp,
        {name: "所属门店:",value: studio_names.join(',')},
        {name: "授课对象:",value: position_info.age_group});

        //设置舞种和导师等级
      _add_position_label_temp(position_show_temp,
        {name: "导师等级:",value: position_info.teacher_level_name},
        {name: "舞种:",value: null});

      $("#position_show_model").before(position_show_temp);

      // 重置部门
      $("#department_select .downbtn span").attr("data_id",0);
      $("#department_select .downbtn span").text("请选择部门");

      // 已出已添加的职务
      $("#position_select .downbtn span").attr("data_id",0);
      $("#position_select .downbtn span").text("请选择职务");
      $("#position_select .dropdown-menu li[data_id='"+position_info.position_id+"']").remove();

      // 重置门店选择
      $(".pos-branch .check").removeClass("ad-checked").attr("hps-readonly","0").find("span").hide();
      
      // 根据导师控制导师信息界面的展示
      if(position_info.position_name=="导师"){
        $("#teacher_salaries").show().attr("data_status","1");
      }else if (position_info.position_name=="服务顾问") {
        $("#fuwuguwen_salaries").show().attr("data_status","1");
      }else if (position_info.position_name=="产品总监") {
        $("#chanpinzongjian_salaries").show().attr("data_status","1");
      }else if (position_info.position_name=="运营总监") {
        $("#yunyingzongjian_salaries").show().attr("data_status","1");
      }else if (position_info.position_name=="市场总监") {
        $("#shichangzongjian_salaries").show().attr("data_status","1");
      }else if (position_info.position_name=="市场专员") {
        $("#shichangzhuanyuan_salaries").show().attr("data_status","1");
      }

1      // console.log("移除职务!!!"+position_info.position_id);
    }


    //获得职务标签模板
    function _add_position_label_temp(temp,one,two){

      if((one.value==null || one.value=="" || one.value.indexOf("请选择")==0) && (two.value==null || two.value=="" || two.value.indexOf("请选择")==0)) return;
      
      var position_info_temp = $("#position_show_model #position_info_model").clone();
      if(one!=null && one.value!=null && one.value!="" && one.value.indexOf("请选择")!=0){
        position_info_temp.find(".pos-one span").text(one.name);
        position_info_temp.find(".pos-one em").html(one.value);
        position_info_temp.find(".pos-one").show();
      }

      if(two!=null && two.value!=null && two.value!="" && two.value.indexOf("请选择")!=0){
        position_info_temp.find(".pos-two span").text(two.name);
        position_info_temp.find(".pos-two em").html(two.value);
        position_info_temp.find(".pos-two").show();
      }

      position_info_temp.removeAttr("id");

      temp.find(".pos-dbox").append(position_info_temp);
    }


    //添加职务验证
    function _add_positon_validate(position_info){

      var flag = true;
      //职务为空判断
      if(position_info.position_name==null || position_info.position_name.indexOf("请选择")==0){
        $("#position_select").siblings("b").show();
        console.log("职务:"+position_info.position_name);
        flag = false;
      }else{
        $("#position_select").siblings("b").hide();
      }

      //部门为空判断
      if(position_info.department_name==null || position_info.department_name.indexOf("请选择")==0){
        $("#department_select").siblings("b").show();
        console.log("职务:"+position_info.department_name);
        flag = false;
      }else{
        $("#department_select").siblings("b").hide();
      }

      if(position_info.position_name=="导师"){

        //等级为空判断
        if(position_info.teacher_level_name==null || position_info.teacher_level_name.indexOf("请选择")==0){
          $("#teacher_level_select").siblings("b").show();
          console.log("职务:"+position_info.teacher_level_name);
          flag = false;
        }else{
          $("#teacher_level_select").siblings("b").hide();
        }

        //授课对象为空判断
        if(position_info.age_group==null || position_info.age_group.indexOf("请选择")==0){
          $("#age_group_select").siblings("b").show();
          console.log("授课对象:"+position_info.age_group);
          flag = false;
        }else{
          $("#age_group_select").siblings("b").hide();
        }

        // //舞蹈类型为空判断
        // if(position_info.dance_type==null || position_info.dance_type.indexOf("请选择")==0){
        //   $("#dance_type_select").siblings("b").show();
        //   console.log("职务:"+position_info.dance_type);
        //   flag = false;
        // }else{
        //   $("#dance_type_select").siblings("b").hide();
        // }
      }
      return flag;
    }

    // 获取职务的值
    EditEmployeePositionInfo.prototype.get_value = function() {
      var position_infos = [];
      //获取职务信息数据
      $("input[name='positon_info']").each(function(){
        if(this.value!=null && this.value!=""){
          position_infos.push(JSON.parse(this.value));
        }
      });
      this.position_infos = position_infos;
      return this.position_infos;
    }

    EditEmployeePositionInfo._initialized = true;
  }

  this.init();
  
}




// 超出薪资服务类 () 服务顾问薪资
function EditEmployeeChaoChuSalaries(dom_id,info){

  this.chaochu_info = typeof(info.chaochu_info)=="object" ? info.chaochu_info : [];//超出提成比例
  this.salary_dom_id = dom_id;//薪资展示层id

  if (typeof EditEmployeeChaoChuSalaries._initialized == "undefined") {
    //导师界面初始化
    EditEmployeeChaoChuSalaries.prototype.init = function() {

      var currentObj = this;

      //添加超出提成按钮监听
      $("#"+this.salary_dom_id+" #out_zhibiao_op .comm-add").click(function(){

        var start_val = $("#"+currentObj.salary_dom_id+" #out_zhibiao_op :input[name='start']").val();
        var end_val = $("#"+currentObj.salary_dom_id+" #out_zhibiao_op :input[name='end']").val();
        var jieduan_scale = $("#"+currentObj.salary_dom_id+" #out_zhibiao_op :input[name='scale']").val();
        
        var data = {'start': start_val,'end': end_val,'scale': jieduan_scale};
        console.log("添加的超出提成为:"+JSON.stringify(data));

        var flag = true;
        
        if(end_val==null || end_val=="" || parseInt(start_val)>parseInt(end_val)){
          $("#"+currentObj.salary_dom_id+" #out_zhibiao_op :input[name='end']").css("border", "1px solid rgb(255, 90, 96)");
          flag = false;
        }else{
          $("#"+currentObj.salary_dom_id+" #out_zhibiao_op :input[name='end']").css("border", "1px solid rgb(171, 173, 193)");
        }

        if(jieduan_scale==null || jieduan_scale==""){
          $("#"+currentObj.salary_dom_id+" #out_zhibiao_op :input[name='scale']").css("border", "1px solid rgb(255, 90, 96)");
          flag = false;
        }else{
          $("#"+currentObj.salary_dom_id+" #out_zhibiao_op :input[name='scale']").css("border", "1px solid rgb(171, 173, 193)");
        }
        
        if(!flag) return;

        currentObj.add_out_ticheng(data);
      });
      this.ticheng_init();
    }

    //导师提成初始化数据
    EditEmployeeChaoChuSalaries.prototype.ticheng_init = function() {
      var currentObj = this;
      this.chaochu_info.forEach(function(data){
        currentObj.add_out_ticheng(data);
      });
    }

    //导师薪资数据
    EditEmployeeChaoChuSalaries.prototype.get_value = function() {
      var salary_info = {};

      if($("#"+this.salary_dom_id).attr("data_status")!="1"){
         return salary_info;
      }
      salary_info.chaochu_info = [];
      //获取职务信息数据
      $("#"+this.salary_dom_id+" input[name='out_zhibiao_info']").each(function(){
        if(this.value!=null && this.value!=""){
          salary_info.chaochu_info.push(JSON.parse(this.value));
        }
      });
      return salary_info;
    }

    // 添加超出提成
    EditEmployeeChaoChuSalaries.prototype.add_out_ticheng = function(out_info){

      if(typeof(out_info)!="object") return;
      var currentObj = this;

      var temp = $("#"+this.salary_dom_id+" #out_zhibiao_op #temp").clone();
      temp.find(".start").text(out_info.start);
      temp.find(".end").text(out_info.end);
      temp.find(".scale").text(out_info.scale);
      temp.find(".comm-del").click(function(){

        var last_dom = $(this).parent().parent().prev();
        var last_dom_val = last_dom.find("#out_zhibiao_info").val();
        var start = 0;
        if (last_dom_val!=""){
          var info_val = $(this).parent().parent().find("#out_zhibiao_info").val();
          start = JSON.parse(info_val).start;
          last_dom.find(".comm-del").show();
        }
        
        $("#"+currentObj.salary_dom_id+" #out_zhibiao_edit :input[name='start']").val(start);
        $("#"+currentObj.salary_dom_id+" #out_zhibiao_edit .stage-box").text(start);
        $(this).parent().parent().remove();
      });
      temp.removeAttr("id");
      // 设置超出提成的信息
      temp.find("#out_zhibiao_info").val(JSON.stringify(out_info));
      temp.show();


      $("#"+this.salary_dom_id+" #out_zhibiao_edit").before(temp);
      $("#"+this.salary_dom_id+" #out_zhibiao_edit .stage-box").text(parseInt(out_info.end)+1);
      $("#"+this.salary_dom_id+" #out_zhibiao_edit :input[name='start']").val(parseInt(out_info.end)+1);

      var last_dom = $("#"+this.salary_dom_id+" #out_zhibiao_edit").prev().prev();
      if (last_dom.find("#out_zhibiao_info").val()!=""){
        last_dom.find(".comm-del").hide();
      }
    }

    EditEmployeeChaoChuSalaries._initialized = true;
  }

  this.init();
}



// 福利补助信息
function EditEmployeeWelfareSalaries(info){

  this.info = typeof(info)=="object" ? info : [];//福利补助

  if (typeof EditEmployeeWelfareSalaries._initialized == "undefined") {
    //导师界面初始化
    EditEmployeeWelfareSalaries.prototype.init = function() {

      this.info.forEach(function(welfare_name){
        $("#welfare_info .check[_welfare_name='"+welfare_name+"']").addClass("ad-checked").find("span").show();
      })
    }

    //导师薪资数据
    EditEmployeeWelfareSalaries.prototype.get_value = function() {

      var welfare_ids = [];
      $("#welfare_info .ad-checked").each(function(){
         
         welfare_ids.push($(this).attr("_welfare_id"));
      });
      return welfare_ids;
    }

    EditEmployeeWelfareSalaries._initialized = true;
  }

  this.init();
}


