
//工资表页面初始化方法
function hps_finances_laborage_init(){

    var default_search_id = hps_default_search_id();

    var dataTable = new HpsDataTables({
        pagesize: 10,
        columns: [
            { "data": "month" },
            { "data": "employee_name" },
            { "data": "position_names" },
            { "data": "base_salary" },
            { "data": "commission_salary" },
            { "data": "sum_salary" }
        ],
        params: {"month": $("#"+default_search_id+" #search_month").val()}
    });

    $("#"+default_search_id+" .dropdown-menu li").click(function(){
        // 点击下拉框的文本
        var click_text = $(this).text();
        var click_data_id = $(this).attr("data_id");

        // 设置选中的下拉框的值和文本
        $(this).parent().siblings(".downbtn").find("span").text(click_text).attr("data_id",click_data_id);
        do_search();
    });

    $("#"+default_search_id+" #employee_name").change(function() { 
        do_search();
    }); 

    hps_month_pick(function(nowYear,nowMonth){
        console.log("nowYear:"+nowYear+",nowMonth:"+nowMonth);
        if(parseInt(nowMonth)<10){
            nowMonth = "0"+nowMonth;
        }
        $("#search_month").val(nowYear+"-"+nowMonth);
        do_search();
    });


    //内部函数
    function do_search(){

        var studio_id = $("#studio_select .downbtn span").attr("data_id");
        var department_id = $("#department_select .downbtn span").attr("data_id");
        var position_name = $("#position_select .downbtn span").attr("data_id");
        var employee_name = $("#employee_name").val();
        var month = $("#search_month").val();
        

        var params = {};
        if(typeof(studio_id)=="string" && studio_id!="all"){
            params.studio_id = studio_id;
        }

        if(typeof(department_id)=="string" && department_id!="all"){
            params.department_id = department_id;
        }

        if(typeof(position_name)=="string" && position_name!="all"){
            params.position_name = position_name;
        }

        if(typeof(employee_name)=="string" && employee_name!=""){
            params.employee_name = employee_name;
        }

        if(typeof(month)=="string" && month!=""){
            params.month = month;
        }


        console.log("查询参数:"+JSON.stringify(params));

        dataTable.reload(params);
    }

    
    function get_select_val(select_id){
       var select_text = $("#"+select_id+" .downbtn span").attr("data_id");
       var select_id = $("#"+select_id+" .dropdown-menu li:contains('"+select_text+"')").attr('data_id');
       return select_id;
    }

}


//财务明细页面初始化方法
function hps_finances_index_init(){

    var default_search_id = hps_default_search_id();

    var dataTable = new HpsDataTables({
        pagesize: 5,
        columns: [
            { "data": "setdate" },
            { "data": "finance_type" },
            { "data": "finance_category" },
            { "data": "amount" },
            { "data": "studio_name" },
            { "data": "op" }
        ],
        params:{"setdate": $("#"+default_search_id+" #search_month").val()}
    });

    $("#"+default_search_id+" .dropdown-menu li").click(function(){
        // 点击下拉框的文本
        var click_text = $(this).text();
        var click_data_id = $(this).attr("data_id");

        // 设置选中的下拉框的值和文本
        $(this).parent().siblings(".downbtn").find("span").text(click_text).attr("data_id",click_data_id);
        
        if(click_text=="收支"){
            $("#"+default_search_id+" #incomeDetail").hide();
            $("#"+default_search_id+" #payDetail").hide();
        }else if(click_text=="收入"){
            $("#"+default_search_id+" #incomeDetail").show();
            $("#"+default_search_id+" #payDetail").hide();
        }else if(click_text=="支出"){
            $("#"+default_search_id+" #incomeDetail").hide();
            $("#"+default_search_id+" #payDetail").show();
        }

        do_search();
    });

    hps_month_pick(function(nowYear,nowMonth){
        console.log("nowYear:"+nowYear+",nowMonth:"+nowMonth);
        if(parseInt(nowMonth)<10){
            nowMonth = "0"+nowMonth;
        }
        $("#"+default_search_id+" #search_month").val(nowYear+"-"+nowMonth);
        do_search();


        $.get("/admin/finances/month_expenditure.json",{"month":$("#"+default_search_id+" #search_month").val()},function(data,status){
            console.log("Data: " + JSON.stringify(data) + "\nStatus: " + status);
            if("success"==status){
                $("#month_blance_table #income_amount").html(data.income);
                $("#month_blance_table #course_reckon_amount").html(data.course_reckon);
                $("#month_blance_table #expenditure_amount").html(data.expenditure);
                $("#month_blance_table #gain_loss_amount").html(data.gain_loss);
            }
        });
    });

    // $("#"+default_search_id+" #branchType").click(function(){
    //     // 点击下拉框的文本
    //     var click_text = $(this).text();
    //     var click_data_id = $(this).attr("data_id");
    //     if(click_text=="收支"){
    //         $("#incomeDetail").hide();
    //         $("#payDetail").hide();
    //     }else if(click_text=="收入"){
    //         $("#incomeDetail").show();
    //         $("#payDetail").hide();
    //     }else{
    //         $("#incomeDetail").hide();
    //         $("#payDetail").show();
    //     }
    //     console.log()

    // });

    // $("#"+default_search_id+" #employee_name").change(function() { 
    //     do_search();
    // }); 

    // hps_month_pick(function(nowYear,nowMonth){
    //     console.log("nowYear:"+nowYear+",nowMonth:"+nowMonth);
    //     if(parseInt(nowMonth)<10){
    //         nowMonth = "0"+nowMonth;
    //     }
    //     $("#search_month").val(nowYear+"-"+nowMonth);
    //     do_search();
    // });


    //内部函数
    function do_search(){

        // 分店id
        var studio_id = $("#studio_select .downbtn span").attr("data_id");
        // 收支类型
        var finance_type = $("#branchType .downbtn span").attr("data_id");
        // 收入类型
        var income_type = $("#incomeDetail .downbtn span").attr("data_id");
        // 支出类型
        var pay_type = $("#payDetail .downbtn span").attr("data_id");
        var month = $("#"+default_search_id+" #search_month").val();

        var params = {};
        if(typeof(studio_id)=="string" && studio_id!="all"){
            params.studio_id = studio_id;
        }
        
        if(typeof(finance_type)=="string" && finance_type!="all"){
            params.finance_type = finance_type;
        }


        if(finance_type=="收入" && typeof(income_type)=="string" && income_type!="all"){
            params.finance_category = income_type;
        }
        
        if(finance_type=="支出" && typeof(pay_type)=="string" && pay_type!="all"){
            params.finance_category = pay_type;
        }

        if(typeof(month)=="string" && month!=""){
            params.setdate = month;
        }

        console.log("查询参数:"+JSON.stringify(params));

        dataTable.reload(params);
    }

    
    // function get_select_val(select_id){
    //    var select_text = $("#"+select_id+" .downbtn span").attr("data_id");
    //    var select_id = $("#"+select_id+" .dropdown-menu li:contains('"+select_text+"')").attr('data_id');
    //    return select_id;
    // }

}
