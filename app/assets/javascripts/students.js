function hps_students_edit_init(){


	var qiuniu = new HpsQiuNiuUploader({
		FileUploaded_callback: function(up, file, info){

		   //查看简单反馈
           var domain = up.getOption('domain');
           var res = JSON.parse(info);
           var sourceLink = domain +"/"+ res.key; //获取上传成功后的文件的Url
           console.log("上传成功,链接为:"+sourceLink);

           $("#student_avatar").val(sourceLink);

           $.post("/qiniu/private_url",{url:sourceLink},function(data,status){
                if(status == "success"){
                  $("#student_avatar_img").attr("src",data.url);
                }else{
                  console.log("获取服务私密链接失败,status:"+status);
                }
           });
		}
	}).init();
}