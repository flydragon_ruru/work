//公共的js
$(function(){

  //获取当前控制器名称 的名称
  var controller_name = $("#current_controller_name").val();
  //获取当前action 的名称
  var action_name = $("#current_action_name").val();

  //获取约定格式的方法名称
  var action_init_function_name = "hps_"+controller_name+"_"+action_name+"_init";

  console.log("公共初始化方法,开始检测方法!");
  //检测方法是否存在
  if(window.hasOwnProperty(action_init_function_name)){

  	console.log("公共初始化方法,检测到方法:"+action_init_function_name+"存在!");
  	eval(action_init_function_name+"();");
  }else{
  	console.log("公共初始化方法,检测到方法:"+action_init_function_name+"不存在!");
  }

});



function default_select_init(){

  /* 下拉列表 */
  $(".downbtn").on("click", function() {
    if ($(this).next("ul").hasClass("menuopen")) {
      $(this).next("ul").removeClass("menuopen");
    } else {
      $(this).next("ul").addClass("menuopen");
      $(this).parent().hover(function() {

      }, function() {
        $(this).children("ul").removeClass("menuopen");
      });
      $(".menuopen li").on("click", function() {
        $(this).parent().removeClass("menuopen");
        $(this).parent().parent().children("div").children("span").text($(this).text());
        if ($(this).parent().parent().attr("Choice") == "mandatory" || $(this).parent().parent().attr("Choice") == "selected") {
          if ($(this).index() != 0) {
            if ($(this).parent().parent().hasClass("spe")) {
              $(this).parent().parent().children("div").children("span").attr("data_id", $(this).attr("data_id"));
            };
            $(this).parent().parent().attr("Choice", "selected");
            $(this).parent().parent().next("b").css({
              "display" : "none"
            })
          } else {
            $(this).parent().parent().attr("Choice", "mandatory");
          }
        };
      })
    }
  });
}


function default_check_init(){

  /* 复选框 */
  $(".check").on("click", function() {

    if($(this).attr("hps-readonly")=="1") return;

    if ($(this).prev("input").prop("checked")) {
      $(this).prev("input").prop("checked", false);
      $(this).removeClass("ad-checked");
      $(this).children("span").css({
        "display" : "none"
      })
    } else {
      $(this).addClass("ad-checked");
      $(this).prev("input").prop("checked", true);
      $(this).children("span").css({
        "display" : "block"
      })
    }
  })
}


function default_radio_init(){

  /* 单选按钮选中事件 */
  $(".rad-sl .ra-btn").on("click", function() {
    radioChange($(this));
    // $(this).parent().children("input").eq($(this).index()).prop("checked", true);
    $(this).parent().parent().find(".comm-con").removeClass("common-show");
    $(this).parent().find(".comm-con").addClass("common-show");
  });
}

 // 添加员工  薪资信息模块
 /* 单选按钮选中事件 */
 function radioChange(sel, can) {
  $(sel).parent().parent().find(".ra-btn").children("span").css({
    "display" : "none"
  });
  $(sel).prev("input").prop("checked", true);
  $(sel).children("span").css({
    "display" : "block"
  });
 }



// 验证
function clearNoNum(obj) {
    //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d.]/g,"");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,"");
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g,".");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
}
function intNum(obj) {
   //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d]/g,"");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,"");
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g,".");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
}


//骑牛
function HpsQiuNiuUploader(config){

   this.BeforeUpload_callback = config.BeforeUpload_callback;
   this.FilesAdded_callback = config.FilesAdded_callback;
   this.UploadProgress_callback = config.UploadProgress_callback;
   this.UploadComplete_callback = config.UploadComplete_callback;
   this.FileUploaded_callback = config.FileUploaded_callback;
   this.Error_callback = config.Error_callback;
   this.Key_callback = config.Key_callback;
   this.container = typeof(config.container)=="string" ? config.container : "upload_avatar_container";
   this.drop_element = typeof(config.drop_element)=="string" ? config.drop_element : "upload_avatar_container";
   this.browse_button = typeof(config.browse_button)=="string" ? config.browse_button : "upload_avatar";
}

//七牛云初始化函数
HpsQiuNiuUploader.prototype.init=function(){

  console.log("文件上传成功,回调函数类型为:"+typeof(this.FileUploaded_callback));

  var BeforeUpload_callback = this.BeforeUpload_callback;
  var UploadProgress_callback = this.UploadProgress_callback;
  var UploadComplete_callback = this.UploadComplete_callback;
  var FileUploaded_callback = this.FileUploaded_callback;
  var Error_callback = this.Error_callback;
  var Key_callback = this.Key_callback;
  var container = this.container;
  var drop_element = this.drop_element;
  var browse_button = this.browse_button;
  
  var uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',
        browse_button: browse_button,
        container: container,
        drop_element: drop_element,
        max_file_size: '1000mb',
        flash_swf_url: '/assets/plupload/sMoxie.swf',
        dragdrop: true,
        chunk_size: '4mb',
        multi_selection: !(mOxie.Env.OS.toLowerCase()==="ios"),
        uptoken_url: "/qiniu/uptoken",
        domain: $('#qiniu_domain').val(),
        get_new_uptoken: false,
        auto_start: true,
        log_level: 5,
        init: {
            'FilesAdded': function(up, files) {
                // plupload.each(files, function(file) {
                //   console.log("等待...  FilesAdded");
                // });
                if(typeof(FilesAdded_callback)=="function"){
                  FilesAdded_callback(up,file);
                }
            },
            'BeforeUpload': function(up, file) {
                
              console.log("BeforeUpload");
              if(typeof(BeforeUpload_callback)=="function"){
                BeforeUpload_callback(up,file);
              }
            },
            'UploadProgress': function(up, file) {
              console.log("UploadProgress");
              if(typeof(UploadProgress_callback)=="function"){
                UploadProgress_callback(up,file);
              }
            },
            'UploadComplete': function() {
              $('#success').show();
              console.log("上传成功!, UploadComplete");
              if(typeof(UploadComplete_callback)=="function"){
                UploadComplete_callback();
              }
            },
            'FileUploaded': function(up, file, info) {

              // 每个文件上传成功后，处理相关的事情
               // 其中info是文件上传成功后，服务端返回的json，形式如：
               // {
               //    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
               //    "key": "gogopher.jpg"
               //  }
               //查看简单反馈
               // var domain = up.getOption('domain');
               // var res = JSON.parse(info);
               // var sourceLink = domain +"/"+ res.key; //获取上传成功后的文件的Url
               // console.log("上传成功,链接为:"+sourceLink);

               // $("#employee_avatar").val(sourceLink);

               // $.post("/qiniu/private_url",{url:sourceLink},function(data,status){
               //      if(status == "success"){
               //        $("#employee_avatar_img").attr("src",data.url);
               //      }else{
               //        console.log("获取服务私密链接失败,status:"+status);
               //      }
               //  });

              console.log("文件上传成功,回调函数类型为:"+typeof(FileUploaded_callback));

              if(typeof(FileUploaded_callback)=="function"){
                FileUploaded_callback(up, file, info);
              }
            },
            'Error': function(up, err, errTip) {
               console.log("上传失败!");

              if(typeof(Error_callback)=="function"){
                Error_callback(up, err, errTip);
              }
            }
            ,
            'Key': function(up, file) {

              if(typeof(Key_callback)=="function"){
                Key_callback(up,file);
              }else{
                // console.log("up:"+JSON.stringify(up));
                // console.log("file:"+JSON.stringify(file));
                var key = $("#current_controller_name").val()+"/"+file.name;
                // do something with key
                return key
              }
            }
        }
    });

  return uploader;
}


//浮点数验证
function floatNum(obj) {
   //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d|\.]/g,"");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,"");
    //保证只有出现一个.而没有多个.
    // obj.value = obj.value.replace(/\.{2,}/g,"");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
}


//获取随机数
function get_rand_num(){
    return Math.floor(Math.random()*1000);
}


function hps_default_datatable_id(){

  return $("#current_controller_name").val()+"_"+$("#current_action_name").val()+"_datatable";
}

function hps_default_search_id(){

  return $("#current_controller_name").val()+"_"+$("#current_action_name").val()+"_search";
}



 // 月份日历
function hps_month_pick(callback) {
    var t = new Date();
    var nowYear = t.getFullYear();
    var nowMonth = t.getMonth() + 1;
    $(".t-month span").eq(nowMonth - 1).css({
        "color" : "#4584ff"
    });
    $(".t-year strong").text(nowYear + "年");
        // director();
    $(".mdate-btn strong").text(nowYear);
    $(".mdate-btn em").text(nowMonth);
    $(".mar-date").hover(function() {
    },function() {
        $(".mdate-btn").attr("dateCon", "close");
        $(".time-date").css({
            "display" : "none"
        });
        $(this).children("div").eq(0).removeClass("bchose");
    });
    $(".year-r").on("click", function() {
        if (nowYear == 2060) {
            nowYear = 2060;
        } else {
            nowYear++;
        }
        $(".t-year strong").text(nowYear + "年");
        $(".mdate-btn strong").text(nowYear);
        callback(nowYear,nowMonth);
    });
    $(".year-l").on("click", function() {
        if (nowYear == 1970) {
            nowYear = 1970;
        } else {
            nowYear--;
        }
        $(".t-year strong").text(nowYear + "年");
        $(".mdate-btn strong").text(nowYear);
        callback(nowYear,nowMonth);
    });
    $(".t-month span").on("click", function() {
        $(this).css({
            "color" : "#4584ff"
        });
        $(this).siblings().css({
            "color" : "#a8a8a8"
        });
        nowMonth = parseInt($(this).text());
        $(".mdate-btn em").text(nowMonth);
        $(".mdate-btn").attr("dateCon", "close");
        $(".time-date").css({
            "display" : "none"
        });
        $(".t-year strong").text(nowYear + "年");
        $(this).removeClass("bchose");
        callback(nowYear,nowMonth);
    })
   
}


