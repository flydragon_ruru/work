class Admin::StudentRemarksController < ApplicationController
  layout 'studio'
  def index 
    params[:begin_time] ||= "all"
    params[:end_time] ||= "all"
    conditions = ["1=1"]
    if params[:begin_time] != "all" and params[:end_time] != "all"
      conditions << "remark_at between '#{params[:begin_time]}' and '#{params[:end_time]}'"
    elsif params[:begin_time] != "all" and params[:end_time] == "all"
      conditions << "remark_at > '#{params[:begin_time]}'"
    elsif params[:begin_time] == "all" and params[:end_time] != "all"
      conditions << "remark_at < '#{params[:end_time]}'"
    end
    @student = Student.find(params[:student_id])
    @employee = Employee.find_by(user_id: current_user.id, company_id: current_company.id)
    @positions = @employee.employee_of_studios.map { |em| em.position_configuration.position_name }

    @student_remarks = []
    if @positions.include?("服务顾问") or @positions.include?("导师") or @positions.include?("主理人")
      @student_remarks += @student.student_remarks 
        .where(conditions.join(" and ")).order("remark_at desc")
    end
    if @positions.include?("前台") 
      @student_remarks += @student.student_remarks.where( receptionist_employee_id: @employee.id)
      .where(conditions.join(" and ")).order("remark_at desc")
    end
     @student_remarks = @student_remarks.uniq.paginate(page: params[:page], per_page: get_per_page_num)
     respond_to do |format|
       format.html
       format.js
     end
  end

  def get_data_list
    params[:begin_time] ||= "all"
    params[:end_time] ||= "all"
    conditions = ["1=1"]
    if params[:begin_time] != "all" and params[:end_time] != "all"
      conditions << "remark_at between '#{params[:begin_time]}' and '#{params[:end_time]}'"
    elsif params[:begin_time] != "all" and params[:end_time] == "all"
      conditions << "remark_at > '#{params[:begin_time]}'"
    elsif params[:begin_time] == "all" and params[:end_time] != "all"
      conditions << "remark_at < '#{params[:end_time]}'"
    end
    @student = Student.find(params[:student_id])
    @employee = Employee.find_by(user_id: current_user.id, company_id: current_company.id)
    @positions = @employee.employee_of_studios.map { |em| em.position_configuration.position_name }

    @student_remarks = []
    if @positions.include?("服务顾问") or @positions.include?("导师") or @positions.include?("主理人")
      @student_remarks += @student.student_remarks 
        .where(conditions.join(" and ")).order("remark_at desc")
    end
    if @positions.include?("前台") 
      @student_remarks += @student.student_remarks.where( receptionist_employee_id: @employee.id)
      .where(conditions.join(" and ")).order("remark_at desc")
    end
     @student_remarks = @student_remarks.uniq.paginate(page: params[:page], per_page: get_per_page_num)
  end


  def new
    @employee = Employee.find_by(user_id: current_user.id, company_id: current_company.id)
    @positions = @employee.employee_of_studios.map { |em| em.position_configuration.position_name }
    @student = Student.find(params[:student_id])
    @student_remark = StudentRemark.new(student_id: @student.id, remark_at: Date.current)
    if @positions.include?("主理人") or @positions.include?("前台") 
      @student_remark.receptionist_employee_id = @employee.id
    end
    if @positions.include?("服务顾问")
      @student_remark.service_employee_id = @employee.id
    end
    if @positions.include?("导师")
      @student_remark.teacher_employee_id = @employee.id
    end
  end

  def create
    student_remark = StudentRemark.new(student_remark_params)
    student_remark.save!
    @student = student_remark.student 
  end

  private

  def student_remark_params 
    params.require(:student_remark).permit(:remark, :student_id,:remark_at, :market_employee_id, :service_employee_id, :teacher_employee_id, :receptionist_employee_id)
  end
end
