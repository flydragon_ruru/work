class Admin::StatisticsController < ApplicationController
  before_action :studio_filter
  before_action :month_begin_at
  
  #招生统计权限控制
  before_action :authorize_potential_student, only:['recruit'] 
  #学员统计权限控制
  before_action :authorize_student, only:['student']
  #商品统计权限控制
  before_action :authorize_good , only: ['commodity']
  

  layout "studio"

  #综合统计
  def comprehensive 
    company = current_company
    studio = current_studio
    #顶部学员 意向学员 即将到期学员
    @student_count = studio.students.count
    @potential_student_count = studio.potential_students.count
    @expire = studio.students.select {|stu| stu unless stu.is_expire}.size
    ########顶部结束###########
    #市场专员招生进度进度条展示
    @student_progress = studio.students.where(created_at: (@month_begin_at)..(Date.current)).count
    @market_target = studio.targets_of_month(@month_begin_at.to_s, "市场总监").to_i 
    @market_width = "0"
    if @student_progress.to_f > @market_target.to_f
      @market_width = "100"
    else
      if @market_target == 0
        @market_width = "0"
      else
        @market_width = "#{(100*@student_progress.to_f/@market_target.to_f).round(2)}"
      end
    end
    ########招生结束###########
    #服务顾问服务进度进度条展示
    @students_expent = Order.joins(student: [:studio]).where("studios.id = #{studio.id}").where(created_at: (@month_begin_at)..(Date.current)).sum(:total_price) 
    @service_target = studio.targets_of_month(@month_begin_at.to_s, "运营总监").to_i 
    @service_width = "0"
    if @students_expent.to_f > @service_target.to_f
      @service_width = "100"
    else
      if @service_target == 0
        @service_width = "0"
      else
        @service_width = "#{(100*@students_expent.to_f/@service_target.to_f).round(2)}"
      end
    end
    ########服务结束###########
    ###导师课消进度进度条展示##
    @students_reckon = CourseReckon.where("studio_id = #{studio.id}").where(reckon_at: (@month_begin_at)..(Date.current)).sum(:amount) 
    @teacher_target = studio.targets_of_month(@month_begin_at.to_s, "产品总监").to_i 
    @reckon_width = "0"
    if @students_reckon.to_f > @teacher_target.to_f
      @reckon_width = "100"
    else
      if @teacher_target == 0
        @reckon_width = "0"
      else
        @reckon_width = "#{(100*@students_reckon.to_f/@teacher_target.to_f).round(2)}"
      end
    end
    ########课消结束###########
  end

  #商品数据,kevin
  def commodity

    start = Time.now;

    ################################30天之前总计################################
    # 累积营业额(元)
    @total_price = 0;
    # 课程商品金额
    good_ids = Good.where(good_type: "课程商品").ids;
    @total_price_lesson = 0;
    # 周边商品金额
    @total_price_peripheral = 0;

    # 30天数据，累计金额，课程金额，周边商品金额
    @total_price_array = Array.new();
    @total_price_lesson_array = Array.new();
    @total_price_peripheral_array = Array.new();
    @past_month_array = get_month_days_by_today;
    for day in  @past_month_array
        # start = Time.now;
        
        p "今日查询#{day}"
        orders = Order.where("created_at like ?", "%#{day}%").where(studio_id: current_studio.id);
        # p orders;

        total_price = orders.sum("total_price");
        @total_price += total_price;
        @total_price_array.push(total_price);

        total_price_lesson = orders.where(good_id: good_ids).sum("total_price");
        @total_price_lesson += total_price_lesson;
        @total_price_lesson_array.push(total_price_lesson);

        # 优化总和＝课程＋周边
        # total_price_peripheral = orders.where.not(good_id: good_ids).sum("total_price");
        total_price_peripheral = total_price - total_price_lesson;
        @total_price_peripheral += total_price_peripheral;
        @total_price_peripheral_array.push(total_price_peripheral);
        
        # p "查询花费时间：#{(Time.now - start)*1000} ms";
    end

    # p "累计金额:#{@total_price_array}";
    # p "课程累计金额:#{@total_price_lesson_array}";
    # p "周边累计金额:#{@total_price_peripheral_array}";
    # p "日期周期"
    # p @past_month_array;
    # p "累积营业额(元)";
    # p @total_price;
    # p "课程商品金额"
    # p @total_price_lesson;
    # p "周边商品金额"
    # p @total_price_peripheral;

    
    # 处理30天数组数据，转化成string
    tmp_total_price = "";
    tmp_total_price_lesson = "";
    tmp_total_price_peripheral = "";
    tmp_past_day = "";
    # 处理30天数组数据，转化成string
    for i in 0..29
        tmp_total_price            = tmp_total_price + @total_price_array[i].to_s + ",";
        tmp_total_price_lesson     = tmp_total_price_lesson + @total_price_lesson_array[i].to_s + ",";
        tmp_total_price_peripheral = tmp_total_price_peripheral + @total_price_peripheral_array[i].to_s + ",";
        tmp_past_day               = tmp_past_day + @past_month_array[i].to_s + ",";
    end
    p @total_price_array_string = tmp_total_price.chop;
    p @total_price_lesson_array_string = tmp_total_price_lesson.chop;
    p @total_price_peripheral_array_string = tmp_total_price_peripheral.chop;
    p @past_month_array_string = tmp_past_day.chop;


    ################################商品排行榜################################

    # 本月商品数据统计，金额，销售数量排行前十
    today = Time.new().strftime("%Y-%m");
    p "当前日期#{today}";

    # start = Time.now;
    # 商品排行榜数据查询
    results_good_price_orders = get_mysql_client.query("SELECT good_id, sum(total_price) FROM orders 
        WHERE created_at like '%#{today}%' AND studio_id = #{current_studio.id} 
        GROUP BY good_id 
        ORDER BY sum(total_price) desc
        LIMIT 10").to_a;

    results_good_amount_orders = get_mysql_client.query("SELECT good_id, sum(amount) FROM orders 
        WHERE created_at like '%#{today}%' AND studio_id = #{current_studio.id} 
        GROUP BY good_id 
        ORDER BY sum(amount) desc
        LIMIT 10").to_a;

    tmp_total_price_all = "";#排行榜前十商品金额
    tmp_total_price_all_name = "";#排行榜前十商品名称

    tmp_total_amount_all = "";#排行榜前十商品数量
    tmp_total_amount_all_name = "";#排行榜前十商品名称

    # 商品排行榜数据封装
    tmp_num = 9;
    for i in 0..9
        # 金额
        order_total_price = results_good_price_orders[tmp_num - i];
        if order_total_price
            good_id_price = order_total_price["good_id"];
            tmp_total_price_all_name = tmp_total_price_all_name + Good.find_by_id(good_id_price).name + ",";
            tmp_total_price_all = tmp_total_price_all + order_total_price["sum(total_price)"].to_s + ",";
        else
            tmp_total_price_all_name = tmp_total_price_all_name + "" + ",";
            tmp_total_price_all = tmp_total_price_all + "" + ",";    
        end
        
        # 销量
        order_total_amount = results_good_amount_orders[tmp_num - i];
        if order_total_amount
            good_id_price = order_total_amount["good_id"];
            tmp_total_amount_all_name = tmp_total_amount_all_name + Good.find_by_id(good_id_price).name + ",";
            tmp_total_amount_all = tmp_total_amount_all + order_total_amount["sum(amount)"].to_s + ",";
        else
            tmp_total_amount_all_name = tmp_total_amount_all_name + "" + ",";
            tmp_total_amount_all = tmp_total_amount_all + "" + ",";    
        end
    end

    p @good_total_price_array_string = tmp_total_price_all.chop;
    p @good_total_price_name_array_string = tmp_total_price_all_name.chop;

    p @good_total_amount_array_string = tmp_total_amount_all.chop;
    p @good_total_amount_name_array_string = tmp_total_amount_all_name.chop;

    p "查询花费时间：#{(Time.now - start)*1000} ms";    
  end

  #课程数据
  def curriculum
    company = current_company
    studio = current_studio
    #=======本月课消排行前十========#
    #课程名字集合
    @course_name_range_arr = Array.new(10, "未知") 
    #上课次数集合
    @course_reckons_range_arr = Array.new(10, 0) 
    reckons = CourseReckon.joins(:course_info).select("course_infos.name as course_name, count(course_reckons.course_info_id) as reckon_amount").where("course_reckons.studio_id = #{studio.id} and course_reckons.created_at between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}'and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").group("course_reckons.course_info_id").order("count(course_reckons.course_info_id) desc").limit(10)
    reckons = reckons.reverse;
    reckons.each do |re|
      @course_name_range_arr.push(re.course_name)
      @course_reckons_range_arr.push(re.reckon_amount) 
    end
    @course_name_range_arr = @course_name_range_arr[-10,10]
    @course_reckons_range_arr = @course_reckons_range_arr[-10,10]
    #============================
    #本月教室使用排行
    @room_course_reckons_arr = []
    room_course_reckons = CourseReckon.joins(course_info: :room).select("course_room_configurations.room_name as room_name, count(course_reckons.course_info_id) as reckon_amount").where("course_reckons.studio_id = #{studio.id} and course_reckons.created_at between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}'and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").group("course_reckons.course_info_id").order("count(course_reckons.course_info_id) desc").limit(10)
    room_course_reckons.each do |room_course| 
      rc = {value: room_course.reckon_amount, name: "#{room_course.room_name}"}
      @room_course_reckons_arr.push(rc)
    end
    @room_course_reckons_arr = @room_course_reckons_arr.to_json
    #============================
    #本月课程消费类型
    @sale_type_course_reckons_arr = []
    sale_type_course_reckons = CourseReckon.joins(:course_info).select("course_infos.sale_type as sale_name, count(course_reckons.course_info_id) as reckon_amount").where("course_reckons.studio_id = #{studio.id} and course_reckons.created_at between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}'and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").group("course_infos.sale_type").order("count(course_reckons.course_info_id) desc").limit(10)
    sale_type_course_reckons.each do |sale_course| 
      sc = {value: sale_course.reckon_amount, name: "#{sale_course.sale_name}"}
      @sale_type_course_reckons_arr.push(sc)
    end
    @sale_type_course_reckons_arr = @sale_type_course_reckons_arr.to_json
    #============================
    #本月课程类型类型
    @course_type_course_reckons_arr = []
    course_type_course_reckons = CourseReckon.joins(:course_info).select("course_infos.course_type as type_name, count(course_reckons.course_info_id) as reckon_amount").where("course_reckons.studio_id = #{studio.id} and course_reckons.created_at between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}'and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").group("course_infos.course_type").order("count(course_reckons.course_info_id) desc").limit(10)
    course_type_course_reckons.each do |type_course| 
      tc = {value: type_course.reckon_amount, name: "#{type_course.type_name}"}
      @course_type_course_reckons_arr.push(tc)
    end
    @course_type_course_reckons_arr = @course_type_course_reckons_arr.to_json
    #============================
    #本月课程支付类型
    @pay_way_course_reckons_arr = []
    pay_way_course_reckons = CourseReckon.select("pay_way , count(pay_way) as reckon_amount").where("studio_id = #{studio.id} and created_at between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}'and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").group("pay_way").order("count(pay_way) desc").limit(10)
    pay_way_course_reckons.each do |pay_course| 
      pc = {value: pay_course.reckon_amount, name: "#{pay_course.pay_way}"}
      @pay_way_course_reckons_arr.push(pc)
    end
    @pay_way_course_reckons_arr = @pay_way_course_reckons_arr.to_json

  end

  #招生数据
  def recruit
    company = current_company
    studio = current_studio
    #=======最近30天数据=======#
    #时间数组
    @potential_student_date_array = []
    #新增意向学员数组
    @grow_potential_students = []
    #学员数组
    @grow_students = []
    for day in ((Date.current - 30.day)..Date.current)
      nowday = day.strftime('%Y-%m-%d')
      @potential_student_date_array.push(nowday)
      day_potential_students = studio.potential_students.where(created_at: ((day.to_datetime)..(day.to_datetime + 1.day - 1.second))).count
      @grow_potential_students.push(day_potential_students)
      day_students = studio.students.where(created_at: ((day.to_datetime)..(day.to_datetime + 1.day - 1.second))).count
      @grow_students.push(day_students)
    end
    #===========================#
    #=======市场专员业绩========#
    #名字集合
    @market_name_arr = Array.new(10, "未知") 
    #转换人数集合
    @market_students_arr = Array.new(10, 0) 
#   get_mysql_client.query("SELECT employees.name, count(students.market_employee_id) as switch_amount FROM students, employees 
#   where (employees.id = students.market_employee_id and students.created_at between '#{@month_begin_at}'and '#{Time.new}' and students.studio_id = '#{studio.id}')
#      GROUP BY students.market_employee_id
#      ORDER BY count(students.market_employee_id) desc
#      LIMIT 10").to_a;
    students = Student.joins(:market_employee).select("employees.name as market_name, count(students.market_employee_id) as switch_amount").where("students.studio_id = #{studio.id} and students.created_at between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}'and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").group(:market_employee_id).order("count(students.market_employee_id) desc").limit(10)
    students.each do |stu|
      @market_name_arr.push(stu.market_name)
      @market_students_arr.push(stu.switch_amount) 
    end
    @market_name_arr = @market_name_arr[-10,10]
    @market_students_arr = @market_students_arr[-10,10]
    #============================
    #=======市场回访业绩========#
    #名字集合
    @remark_name_arr = Array.new(10, "未知") 
    #回访次数集合
    @remark_students_arr = Array.new(10, 0) 
    remarks = StudentRemark.joins(:market_employee, potential_student: :studio).select("employees.name as market_name, count(student_remarks.market_employee_id) as remark_amount").where("studios.id = #{studio.id} and student_remarks.created_at between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}'and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").group("student_remarks.market_employee_id").order("count(student_remarks.market_employee_id) desc").limit(10)
    remarks.each do |re|
      @remark_name_arr.push(re.market_name)
      @remark_students_arr.push(re.remark_amount) 
    end
    @remark_name_arr = @remark_name_arr[-10,10]
    @remark_students_arr = @remark_students_arr[-10,10]
    #============================
    ##本月新增的意向学员转化比###
    #本月新增意向学员
    @potential_students = studio.potential_students.where(created_at: (@month_begin_at)..(Date.current))
    #意向学员转化数组
    @potential_switch_arr = []
    @potential_students_group = @potential_students.group_by {|s| s.status}
    @potential_students_group.each do |status, p_student| 
      ps_hash = {value: p_student.size, name: "#{status}学员"}
      @potential_switch_arr.push(ps_hash)
    end
    @potential_switch_arr = @potential_switch_arr.to_json 
    ##意向学员年龄分组
    @potential_age_group_arr = []
    #幼儿
    child_count = @potential_students.where("birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'").count
    ps_hash = {value: child_count, name: "幼儿"}
    @potential_age_group_arr.push(ps_hash)
    #少年
    youngster_count = @potential_students.where("birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'").count
    ps_hash = {value: youngster_count, name: "少年"}
    @potential_age_group_arr.push(ps_hash)
    #青少年
    youth_count = @potential_students.where("birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'").count
    ps_hash = {value: youth_count, name: "青少年"}
    @potential_age_group_arr.push(ps_hash)
    #成年
    adult_count = @potential_students.where("birthday < '#{Date.current - 18.year}'").count
    ps_hash = {value: adult_count, name: "成年"}
    @potential_age_group_arr.push(ps_hash)
    @potential_age_group_arr = @potential_age_group_arr.to_json
    #########已转化学员年龄分组########
    @change_potential_age_group_arr = []
    #幼儿
    child_count = @potential_students.where(status: "已转化").where("birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'").count
    ps_hash = {value: child_count, name: "幼儿"}
    @change_potential_age_group_arr.push(ps_hash)
    #少年
    youngster_count = @potential_students.where(status: "已转化").where("birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'").count
    ps_hash = {value: youngster_count, name: "少年"}
    @change_potential_age_group_arr.push(ps_hash)
    #青少年
    youth_count = @potential_students.where(status: "已转化").where("birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'").count
    ps_hash = {value: youth_count, name: "青少年"}
    @change_potential_age_group_arr.push(ps_hash)
    #成年
    adult_count = @potential_students.where(status: "已转化").where("birthday < '#{Date.current - 18.year}'").count
    ps_hash = {value: adult_count, name: "成年"}
    @change_potential_age_group_arr.push(ps_hash)
    @change_potential_age_group_arr = @change_potential_age_group_arr.to_json
    #########未转化学员年龄分组########
    @not_change_potential_age_group_arr = []
    #幼儿
    child_count = @potential_students.where(status: "未转化").where("birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'").count
    ps_hash = {value: child_count, name: "幼儿"}
    @not_change_potential_age_group_arr.push(ps_hash)
    #少年
    youngster_count = @potential_students.where(status: "未转化").where("birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'").count
    ps_hash = {value: youngster_count, name: "少年"}
    @not_change_potential_age_group_arr.push(ps_hash)
    #青少年
    youth_count = @potential_students.where(status: "未转化").where("birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'").count
    ps_hash = {value: youth_count, name: "青少年"}
    @not_change_potential_age_group_arr.push(ps_hash)
    #成年
    adult_count = @potential_students.where(status: "未转化").where("birthday < '#{Date.current - 18.year}'").count
    ps_hash = {value: adult_count, name: "成年"}
    @not_change_potential_age_group_arr.push(ps_hash)
    @not_change_potential_age_group_arr = @not_change_potential_age_group_arr.to_json
    #########学员来源分析########
    @potential_source_arr = []
    @potential_students_source_group = @potential_students.group_by {|s| s.source_from}
    @potential_students_source_group.each do |key, sf_student| 
      ps_hash = {value: sf_student.size, name: "#{key}"}
      @potential_source_arr.push(ps_hash)
    end
    @potential_source_arr = @potential_source_arr.to_json 
    #########意向学员性别比########
    @potential_sex_arr = []
    @potential_students_sex_group = @potential_students.group_by {|s| s.sex}
    @potential_students_sex_group.each do |key, sf_student| 
      ps_hash = {value: sf_student.size, name: "#{key}"}
      @potential_sex_arr.push(ps_hash)
    end
    @potential_sex_arr = @potential_sex_arr.to_json 
    #############################

  end
  
  #服务数据
  def service 
    company = current_company
    studio = current_studio
    #=======最近30天数据=======#
    #时间数组
    @student_spend_date_array = []
    #学员每日花费的数组
    @student_spend_in_day = []
    #学员数组
    for day in ((Date.current - 30.day)..Date.current)
      nowday = day.strftime('%Y-%m-%d')
      @student_spend_date_array.push(nowday)
      day_spend = studio.student_orders.where(created_at: ((day.to_datetime)..(day.to_datetime + 1.day - 1.second))).sum(:total_price)
      potential_day_spend = studio.potential_orders.where(created_at: ((day.to_datetime)..(day.to_datetime + 1.day - 1.second))).sum(:total_price)
      day_spend  = day_spend + potential_day_spend
      @student_spend_in_day.push(day_spend)
    end
    p @student_spend_in_day
    #===========================#
    #名字集合
    @service_name_arr = Array.new(10, "未知") 
    #每个服务顾问业绩集合
    @service_students_arr = Array.new(10, 0) 
    orders = Order.joins(student: [:service_employee, :studio]).select("employees.name as service_name, sum(orders.total_price) as total_amount").where("students.studio_id = #{studio.id} and orders.created_at between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}'and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").group("students.service_employee_id").order("sum(orders.total_price) desc").limit(10)
    orders.each do |tmp|
      @service_name_arr.push(tmp.service_name)
      @service_students_arr.push(tmp.total_amount) 
    end
    @service_name_arr = @service_name_arr[-10,10]
    @service_students_arr = @service_students_arr[-10,10]

    #=======本月服务顾问回访排行========#
    #名字集合
    @remark_name_arr = Array.new(10, "未知") 
    #回访次数集合
    @remark_students_arr = Array.new(10, 0) 
    remarks = StudentRemark.joins(:service_employee, student: :studio).select("employees.name as market_name, count(student_remarks.service_employee_id) as remark_amount").where("studios.id = #{studio.id} and student_remarks.created_at between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}'and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'").group("student_remarks.service_employee_id").order("count(student_remarks.service_employee_id) desc").limit(10)
    remarks.each do |re|
      @remark_name_arr.push(re.market_name)
      @remark_students_arr.push(re.remark_amount) 
    end
    @remark_name_arr = @remark_name_arr[-10,10]
    @remark_students_arr = @remark_students_arr[-10,10]
    
  end

  #学员数据
  def student 
    company = current_company
    studio = current_studio
    #=======最近30天数据=======#
    #时间数组
    @student_date_array = []
    #新增学员数组
    @grow_students = []
    #学员消费数组
    @spend_students = []
    for day in ((Date.current - 30.day)..Date.current)
      nowday = day.strftime('%Y-%m-%d')
      @student_date_array.push(nowday)
      day_students = studio.students.where("students.created_at like '%#{day}%'").count
      @grow_students.push(day_students)
      day_spend = studio.student_orders.where("orders.created_at like '%#{day}%'").select("student_id").distinct.count
      @spend_students.push(day_spend)
    end
    #===========================#
    #==========所有学员课消类型比例==========#
    @reckon_pay_way_key = []
    @reckon_pay_way_value = []
    @course_reckons = studio.course_reckons
    @course_reckons_group = @course_reckons.group_by {|s| s.pay_way}
    @course_reckons_group.each do |key, reckon|
      @reckon_pay_way_key.push(key) 
      ps_hash = {value: reckon.size, name: "#{key}"}
      @reckon_pay_way_value.push(ps_hash)
    end
    @reckon_pay_way_value = @reckon_pay_way_value.to_json 
    @reckon_pay_way_key = @reckon_pay_way_key.to_json 
    #===========================#
    #==========所有学员性别比例==========#
    @all_student_sex_array = []
    students_group = studio.students.group_by {|s| s.sex}
    students_group.each do |key, stus|
      ps_hash = {value: stus.size, name: "#{key}"}
      @all_student_sex_array.push(ps_hash)
    end
    @all_student_sex_array = @all_student_sex_array.to_json 
    p @all_student_sex_array
    #===========================#
     ##学员年龄分组
    @student_age_group_arr = []
    #幼儿
    child_count = studio.students.where("birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'").count
    ps_hash = {value: child_count, name: "幼儿"}
    @student_age_group_arr.push(ps_hash)
    #少年
    youngster_count = studio.students.where("birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'").count
    ps_hash = {value: youngster_count, name: "少年"}
    @student_age_group_arr.push(ps_hash)
    #青少年
    youth_count = studio.students.where("birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'").count
    ps_hash = {value: youth_count, name: "青少年"}
    @student_age_group_arr.push(ps_hash)
    #成年
    adult_count = studio.students.where("birthday < '#{Date.current - 18.year}'").count
    ps_hash = {value: adult_count, name: "成年"}
    @student_age_group_arr.push(ps_hash)
    @student_age_group_arr = @student_age_group_arr.to_json
    #===========================#
    #学员消费能力分组
    @student_shopping_level = []
    #0-5k
    k0_5 = studio.students.where("accumulated_amount between 0 and 4999").count
    k0_5 = {value: k0_5, name: "0-5k"}
    @student_shopping_level.push(k0_5)
    #5-10k
    k5_10 = studio.students.where("accumulated_amount between 5000 and 9999").count
    k5_10 = {value: k5_10, name: "5-10k"}
    @student_shopping_level.push(k5_10)
    #10-20k
    k10_20 = studio.students.where("accumulated_amount between 10000 and 19999").count
    k10_20 = {value: k10_20, name: "10k-20k"}
    @student_shopping_level.push(k10_20)
    #20+k
    k20 = studio.students.where("accumulated_amount > 20000").count    
    k20 = {value: k20, name: "20k以上"}
    @student_shopping_level.push(k20)
    @student_shopping_level = @student_shopping_level.to_json
  end

  #导师数据
  def teacher
    @course_teacher_name_array = Array.new(10, "未知");
    @course_teacher_amount_array = Array.new(10, 0); 
    # 导师课消金额排行
    course_teacher_rank = CourseReckon.select("sum(course_reckons.amount) as amount, teacher_id").
        where("course_reckons.studio_id = #{current_studio.id} and course_reckons.created_at between '#{Time.new(@month_begin_at.to_s).strftime("%Y-%m-%d %H:%M:%S")}' and '#{Time.new.strftime("%Y-%m-%d %H:%M:%S")}'")
        .group("course_reckons.teacher_id")
        .order("sum(course_reckons.amount) asc")
        .limit(10);
    for rank in course_teacher_rank
        @course_teacher_name_array.push(rank.teacher.name);
        @course_teacher_amount_array.push(rank.amount.to_f);
    end
    @course_teacher_name_array = @course_teacher_name_array[-10,10];
    @course_teacher_amount_array = @course_teacher_amount_array[-10,10];
  end


  private

  def month_begin_at
    @month_begin_at = Date.new(Date.current.year, Date.current.month)
  end

  def studio_filter 
    if session[:studio_id].blank?
      redirect_to admin_studios_path
    end
  end

  def authorize_potential_student
    authorize PotentialStudent,:statistics_recruit?
  end

  def authorize_student
    authorize Student,:statistics_student?
  end

  def authorize_good
    authorize Good,:statistics_commodity?
  end



end
