class Admin::MainController < Admin::ApplicationController
  layout "admin"
  def index
  	redirect_to admin_companies_path
  end

  def welcome
  end

end
