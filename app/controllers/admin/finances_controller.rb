class Admin::FinancesController < ApplicationController
  before_action :get_studios, only: [:index, :new, :edit, :get_finance_list, :income, :expense, :edit_expense, :edit_income,:set_initial,:laborage]
  before_action :authorize_finance
  layout "admin"

  def index

    respond_to do |format|
      format.html {
        # 刷新当前月的公司
        # SalaryMonthStatistics::CompanyStatisticsService.call(@company,Time.now.strftime("%Y-%m"))
        @total_info = Finances::IndexTotalService.call(@company)
        # "index"
      }
      format.json { 
        render json: Finances::IndexService.call(view_context,@company) 
      }
    end
  end

  def month_expenditure

    @company = current_company

    respond_to do |format|
      format.json { 
        render json: Finances::MonthExpenditureService.call(view_context,@company) 
      }
    end
  end

  def get_finance_list
    
    params[:studio_id] ||= "all"
    params[:finance_type] ||= "all"
    params[:finance_category] ||="all"
    params[:begin_time] ||="all"
    params[:end_time] ||="all"

    conditions = ["1=1"]
    conditions << "company_id = #{@company.id}" 
    conditions << "studio_id = #{params[:studio_id].to_i}" if params[:studio_id] != "all" && params[:studio_id].present?
    conditions << "finance_type = '#{params[:finance_type]}'" if params[:finance_type] != "all" && params[:finance_type].present?
    conditions << "finance_category = '#{params[:finance_category]}'" if params[:finance_category] != "all" && params[:finance_category].present?
    if params[:begin_time] != "all" and params[:end_time] != "all"
      conditions << "setdate between '#{params[:begin_time]}' and '#{params[:end_time]}'"
    elsif params[:begin_time] != "all" and params[:end_time] == "all"
      conditions << "setdate > '#{params[:begin_time]}'"
    elsif params[:begin_time] == "all" and params[:end_time] != "all"
      conditions << "setdate < '#{params[:end_time]}'"
    end
    @finances = Finance.where(conditions.join(" and ")).order("setdate desc")
      .paginate(page: params[:page], per_page: 5)
  end

  def new
    @finance = Finance.new()
  end

  def create
    studios_arr = params[:studio_ids].split(",") 
    amount = params[:amount].to_f/studios_arr.size
    studios_arr.each do |sid|
      @finance = Finance.new(finance_params)
      @finance.studio_id = sid.to_i
      @finance.company_id = current_company.id 
      @finance.amount = amount 
      @finance.save
    end
    respond_to do |format|
      format.html  { redirect_to admin_finances_path }
    end		
  end

  def edit 
    @finance = Finance.find(params[:id])
  end

  def update
    @finance = Finance.find(params[:id])
    @finance.update(finance_params)
    respond_to do |format|
      format.js { render :js => "window.location.href = '/admin/finances'" }
    end
  end

  def initial_state
    @initial_amount =  Finance.where(finance_type: "初始金额").first
    @remaining_course = Finance.where(finance_type: "未课消").first	
  end

  def set_initial
    studio_ids = @studios.collect {|s| s.id }
    unless params[:initial_amount].blank?
      @initial_amount =  Finance.where(finance_type: "初始金额").first
      p @initial_amount
      if @initial_amount.blank?
        @initial_amount = Finance.create(finance_type: '初始金额', finance_property: '其他',finance_category: '其他', finance_detail: '其他', amount: params[:initial_amount], remarks: "工作室初始资金", initial_state: true)
      else
        @initial_amount.amount = params[:initial_amount]
        @initial_amount.save
      end
      @initial_amount.studio_ids = studio_ids
    end
    unless params[:remaining_course].blank?
      @remaining_course = Finance.where(finance_type: "未课消").first	
      if @remaining_course.blank?
        @remaining_course = Finance.create(finance_type: '未课消', finance_property: '其他',finance_category: '其他', finance_detail: '其他', amount: params[:remaining_course], remarks: "未课消金额", initial_state: true)
      else
        @remaining_course.amount = params[:remaining_course]
        @remaining_course.save
      end
      @remaining_course.studio_ids = studio_ids			
    end
    respond_to do |format|
      format.html  { redirect_to admin_finances_path }
    end		
  end

  def laborage

    respond_to do |format|
      format.html{
        # 刷新当前月的公司
        SalaryMonthStatistics::CompanyStatisticsService.call(@company,Time.now.strftime("%Y-%m"))
      }
      format.json { render json: Finances::LaborageService.call(view_context,@company) }
    end
  end

  private

  def get_studios
    @company = current_company 
    @studios = @company.studios 
  end


  def finance_params
    params.require(:finance).permit(:finance_category,:setdate, :amount, :remark, :finance_type)
  end

  def authorize_finance
    authorize Finance,:index?
  end

end
