# 公司
class Admin::CompaniesController < Admin::ApplicationController
  layout "company"
  def index
  end

  def new
  end

  def new_studio
    @studio = Studio.new()
  end

  def recharge
    @studios = current_company.studios
  end

  def switch_company
    if session[:company_id]
    session.delete(:company_id)
    @current_company = nil
    end
    if session[:studio_id]
    session.delete(:studio_id)
    @current_studio = nil
    end
    redirect_to admin_companies_path
  end

  def finish_paying
  end

end
