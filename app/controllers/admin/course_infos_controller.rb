class Admin::CourseInfosController < ApplicationController

  layout "studio"
  before_action :authorize_course_info


  def index
    @company = current_company
    # @teachers = Teacher.where(company_id: @company.id)
    # kevin,只可以选择本店导师
    @teachers = Teacher.where(studio_id: current_studio.id)

    params[:teacher] ||= "all"
    params[:dance_type] ||= "all"
    params[:sale_type] ||= "all"
    params[:course_type] ||= "all"
    params[:age_group] ||= "all"
    params[:status] ||= "all"

    conditions = ["1=1"]
    conditions << "teachers.name = '#{params[:teacher]}'" if params[:teacher] != "all" && params[:teacher].present?
    conditions << "course_infos.dance_type = '#{params[:dance_type]}'" if params[:dance_type]!= "all" && params[:dance_type].present?
    conditions << "course_infos.sale_type = '#{params[:sale_type]}'" if params[:sale_type] != "all" && params[:sale_type].present?
    conditions << "course_infos.course_type = '#{params[:course_type]}'" if params[:course_type] != "all" && params[:course_type].present?
    conditions << "course_infos.age_group = '#{params[:age_group]}'" if params[:age_group] != "all" && params[:age_group].present?
    conditions << "course_infos.status = '#{params[:status]}'" if params[:status] != "all" && params[:status].present?
    @course_infos = CourseInfo.joins("left join teachers on teachers.id = course_infos.teacher_id")
      .where(conditions.join(" and ")).uniq.order("id desc")
      .paginate(page: params[:page], per_page: 5)
  end

  def get_course_list
    @company = current_company
    # @teachers = Teacher.where(company_id: @company.id)
    # kevin,只可以选择本店导师
    @teachers = Teacher.where(studio_id: current_studio.id)

    params[:teacher] ||= "all"
    params[:dance_type] ||= "all"
    params[:sale_type] ||= "all"
    params[:course_type] ||= "all"
    params[:age_group] ||= "all"
    params[:status] ||= "all"

    conditions = ["1=1"]
    conditions << "teachers.name = '#{params[:teacher]}'" if params[:teacher] != "all" && params[:teacher].present?
    conditions << "course_infos.dance_type = '#{params[:dance_type]}'" if params[:dance_type]!= "all" && params[:dance_type].present?
    conditions << "course_infos.sale_type = '#{params[:sale_type]}'" if params[:sale_type] != "all" && params[:sale_type].present?
    conditions << "course_infos.course_type = '#{params[:course_type]}'" if params[:course_type] != "all" && params[:course_type].present?
    conditions << "course_infos.age_group = '#{params[:age_group]}'" if params[:age_group] != "all" && params[:age_group].present?
    conditions << "course_infos.status = '#{params[:status]}'" if params[:status] != "all" && params[:status].present?

    @course_infos = CourseInfo.joins("left join teachers on teachers.id = course_infos.teacher_id")
      .where(conditions.join(" and ")).uniq
      .paginate(page: params[:page], per_page: 5)
  end

  def new
    @company = current_company
    # @teachers = Teacher.where(company_id: @company.id)
    # kevin,只可以选择本店导师
    @teachers = Teacher.where(studio_id: current_studio.id)
    @rooms = @company.course_room_configurations
  end

  def create
    pay_ways = params[:courseinfo]['0']["pay_ways"]['0']
    course_set = params[:courseinfo]['0']["course_set"]
    @course_info = CourseInfo.new(course_info_params[0])
    @course_info.pay_ways = pay_ways.to_hash if pay_ways.present?
    @course_info.course_set = course_set.to_hash if course_set.present?
    @course_info.sale_or_not = pay_ways["直接售卖"].to_b
    @course_info.company_id = current_company.id
    room = CourseRoomConfiguration.find(@course_info.room_id)
    @course_info.max_contain = room.max_contain
    @course_info.save!
    #新建可售卖类型
    if @course_info.sale_or_not
      sale_course = SaleCourse.new
      sale_course.name = @course_info.name
      sale_course.sale_type = '课程商品'
      sale_course.company_id = @course_info.company_id
      sale_course.course_info_id = @course_info.id
      sale_course.save!
    end

    #支付方式
    pay_ways.each do |type,amount|
     unless type == "直接售卖"
       unless amount == "false"
         if amount == "true"
           pw = PayingWay.new(pay_type: type, course_info_id: @course_info.id, course_info_teacher_id: @course_info.teacher_id)
           pw.save
         else
           pw = PayingWay.new(pay_type: type, amount: amount, course_info_id: @course_info.id, course_info_teacher_id: @course_info.teacher_id)
           pw.save
         end
       end
     end
    end
    unless @course_info.course_type == "私教"
      #固定课时需要先创建课程
      course_set.each do |list|
        if course_set[list][:type] == "每周循环"
          begin_date = Date.parse(course_set[list]["course_begin_at"])
          end_date = Date.parse(course_set[list]["course_over_at"])
          @course_info.course_begin_at = begin_date
          @course_info.course_end_at = end_date
          course_set[list]["时间"].each do |c_time|
            course_time = course_set[list]["时间"][c_time]
            for wk in begin_date..end_date
              if wk.days_to_week_start == course_time["星期"].to_i
                course_date = wk + course_time["小时"].to_i.hour + course_time["分钟"].to_i.minute
                c = Course.new(course_time: course_date,day_of_week: wk,teacher_id: @course_info.teacher_id, course_info_id: @course_info.id)
                c.save
              end
            end
          end
        elsif course_set[list][:type] == "单天设置"
          course_set[list]["时间"].each do |c_time|
            course_time = course_set[list]["时间"][c_time]
            course_date  = Date.parse(course_time["星期"]) + course_time["小时"].to_i.hour + course_time["分钟"].to_i.minute
            c = Course.new(course_time: course_date,day_of_week: course_time["星期"],teacher_id: @course_info.teacher_id, course_info_id: @course_info.id)
            c.save
          end
        end
      end
    end
    @course_info.save
    respond_to do |format|
      format.js { render :js => "window.location.href = '/admin/course_infos'" }
    end
  end

  def show
    @course_info = CourseInfo.find(params[:id])
    @teacher_check_ins =  @course_info.check_ins.where(student_id: nil)
    @student_check_ins =  @course_info.check_ins.where(teacher_id: nil)
    @appointment_records =  @course_info.appointment_records
  end


  def edit
    @course_info = CourseInfo.find(params[:id])
    # @teachers = Teacher.where(company_id: current_company.id)
    # kevin,只可以选择本店导师
    @teachers = Teacher.where(studio_id: current_studio.id)
    @rooms = current_company.course_room_configurations
  end

  def update_course
    @course_info = CourseInfo.find(params[:course_info_id])
    @course_info.update!(course_info_params[0])
  end

  def destroy_appointment_record 
    appointment_record = AppointmentRecord.find_by_id(params[:appointment_record_id])
    @course_info = appointment_record.course.course_info
    appointment_record.destroy
  end

  def close_course
    @course_info = CourseInfo.find(params[:id])
    @course_info.courses.where("day_of_week > :now_time", now_time: Date.current).delete_all
    @course_info.status = "结课"
    @course_info.save!
  end

  private

  def course_info_params
    params.permit(courseinfo: [:sale_type, :course_type, :room_id, :teacher_id, :name, :dance_type, :age_group, :max_contain, :duration, :appointmented_or_not, :sale_or_not, :pay_ways, :period, :course_set])[:courseinfo].values

    #params.permit(courseinfo: [:sale_type, :course_type])[:courseinfo].values
  end

  def course_set_params
    #params.permit(courseinfo: [{pay_ways: [:type, :amount]}])[:courseinfo].values
  end

  def authorize_course_info
    authorize CourseInfo ,:index?
  end

end
