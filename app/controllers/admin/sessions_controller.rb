class Admin::SessionsController < ApplicationController

  def new
    unless current_user
      respond_to do |format|
        format.html
      end
    else
      redirect_to '/admin' 
    end
  end

  def create
    user =  User.find_by(mobile: params[:username])
    if user && user.authenticate(params[:password])
      log_in user
      #redirect_to admin_user_path(user)
      respond_to do |format|
        format.js { render :js => "window.location.href = '/admin'" }
      end
    else
      render  :json=> {"message" => '账号或密码错误'}, status=>'200'
    end
  end

  def logout
    log_out
    redirect_to root_url
  end


  def authoriz_failed

    #根据用户不同的文字渲染不用的布局
    if current_user.scope_company?
      Rails.logger.info "渲染公司"
      render(layout: "admin") 
    else
      Rails.logger.info "渲染分店"
      render(layout: 'studio')
    end
  end

end
