class Admin::ShopDetailController < ApplicationController
  
    def index
    end


    # 支付宝PC端即时到账支付
	def go_to_alipay_pc_direct_pay  
		p "支付宝即时到账支付"
		self.create_alipay_pc_direct_charge
	end

	# 生成支付宝PC端即时到帐订单－ping++ 订单
	def create_alipay_pc_direct_charge
		order_no = self.order_no_create

		# success_url = "http://kevin.tunnel.2bdata.com/"
		# success_url = "http://www.streetdance.wang/"
		success_url = "http://www.heipis.com/"

		@array = Pingpp::Charge.create(
		 :subject   => "HeiPis会员",
		 :body      => "HeiPis会员年卡",
		 :amount    => 1,
		 :order_no  => order_no,
		 :channel   => "alipay_pc_direct",
		 :currency  => "cny",
		 :client_ip => '127.0.0.1',
		 :app => {'id' => "app_5q1aTGy5OmfT8C4i"},
		 :extra => {'success_url' => success_url } #支付宝二维码支付回调url
		)

		p "输出支付宝PC网页即时支付订单返回信息1:"
		p @array

		# 必须是json格式，ping＋＋
		@charge = @array.to_json

		response_body = @array.to_json

		p "alipayPC端即时到帐查询所有订单："
		# 查询ping服务器订单信息
		chs = Pingpp::Charge.all(:limit => 5, :paid => false)
		puts chs

		# 存储订单
		self.order_save(order_no, @array)
	end 

    # 商户订单号，适配每个渠道对此参数的要求，必须在商户系统内唯一。
	# ( alipay : 1-64 位，  wx : 2-32 位， bfb : 1-20 位， upacp : 8-40 位， yeepay_wap :1-50 位， 
	#   jdpay_wap :1-30 位， cnp_u :8-20 位， cnp_f :8-20 位， qpay :1-30 位， cmb_wallet :10 位纯数字字符串。
	#   注：除  cmb_wallet 外的其他渠道推荐使用 8-20 位，要求数字或字母，不允许特殊字符)。
    # private
	def order_no_create
		chars = ("a".."z").to_a + ("A".."Z").to_a + ("0".."9").to_a
		order_no = ""
		for i in 0..20
		    order_no = order_no + chars[rand(chars.size-1)]
		end

		p "随机订单号码:"
		p order_no

		return order_no
	end


	# 订单存储
	def order_save(number, order)
		p "打印订单参数传输"
		p number
		p order

		@order = ChargeOrder.new()
        
        # todo设置传递过来的参数
        @order.user_id = "007"
		@order.number = number
		@order.title = order

		if @order
		  @order.save
		  p "正常存储订单"
		  p @order
		end
	end

end
