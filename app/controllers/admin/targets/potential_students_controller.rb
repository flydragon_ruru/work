class Admin::Targets::PotentialStudentsController < ApplicationController
  layout 'admin'
  def index 
    @potential_students = PotentialStudent.where(company_id: current_company.id, market_employee_id: nil).uniq
    p @potential_students
  end

  def show
    @potential_student = PotentialStudent.find_by(id: params[:id], company_id: current_company.id)
    if @potential_student.present?
      respond_to do |format|
        format.html
      end		
    else
      redirect_to admin_targets_potential_students_path
    end
  end


  def assign
    @potential_student = PotentialStudent.find(params[:id])
    user_employee = current_user.employees.find_by(company_id: current_company.id)
    studio_ids = user_employee.employee_of_studios.map {|es| es.studio_id if es.position_configuration.position_name == "市场总监"}
    studio_ids = studio_ids.uniq.compact
    conditions = ["1=1"]
    conditions << "studios.id in (#{studio_ids.join(",")})" if studio_ids.present?
    conditions << "position_configurations.position_name = '市场专员'"
    @employees = Employee.joins(:employee_of_studios => [:studio, :position_configuration]).where(conditions.join(" and ")).distinct
  end

  def update
    @potential_student = PotentialStudent.find(params[:id])
    @potential_student.update!(potential_student_params)
    
    respond_to do |format|
      format.js { render :js => "window.location.href = '/admin/targets/potential_students'" }
    end
  end

  private
  def potential_student_params
    params.require(:potential_student).permit(:market_employee_id)
  end


end
