class Admin::BranchsController < ApplicationController
  layout 'studio'

  def index 

    # #权限验证
    # authorize Studio,:branchs_index?


    # 进入分店控制台，kevin

    if params[:studio_id]
      @studio = Studio.find(params[:studio_id]);  
      enter_studio(@studio);
    elsif current_studio
      @studio = current_studio
    end
  	
    # if current_studio
    #   @studio = current_studio
    # else
    #   @studio = Studio.find(params[:studio_id]) 
    #   enter_studio(@studio)
    # end

    #设置当前用户的访问范围,公司or分店
    current_user.scope_to_studio
  end
end
