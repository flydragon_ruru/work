class Admin::UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:index, :show, :edit, :update]
  layout 'sign_layout'
  # GET /users_url
  # GET /users.json
  def index
    @users = User.all
  end

  def change_pwd

  end

  def update_pwd
    user = User.mobile_with(params[:user][:mobile])
    unless SmsLog.sn_invalid?(params[:user][:mobile],params[:verf])
      if user.update(user_params)
        log_in user
        render :json =>{'message' => '修改成功'}.to_json, status: '200'
      else
        render :json =>{'message' => '修改失败'}.to_json, status: '200'
      end
    end

  end
  # GET /users/1
  # GET /users/1.json
  def show
  end

  def search_user_by_mobile
    user = User.mobile_with(params[:mobile])
    if user.present?
      json_str = {"message" => "手机号已存在"}.to_json
      render :json=>json_str, status: '200'
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def user_present_or_not
    user = User.mobile_with(params[:mobile])
    if user.present?
      render json:  { present: true}
    else
      render json:  { present: false}
    end
  end

  def reset_password
    
  end

  def update_new_password
    user = User.mobile_with(params[:mobile])
    if user && user.authenticate(params[:old_password])
      respond_to do |format|
        if user.update(user_params)
          log_in user
          render :json =>{'message' => '修改成功'}.to_json, status: '200'
        else
          render :json =>{'message' => '修改失败'}.to_json, status: '200'
        end
      end
    else
      respond_to do |format|
        render :json =>{'message' => '修改失败'}.to_json, status: '200'
      end
    end
  end



  # GET /users/new
  def new
    @user = User.new
  end

  def email
    @user = User.new
  end
  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    unless SmsLog.sn_invalid?(params[:user][:mobile],params[:verf])
      respond_to do |format|
        if @user.save
          log_in @user
          #redirect_to  admin_user_path(@user)
          format.js { render :js => "window.location.href = '#{root_path}'" }
          #format.html { redirect_to admin_user_path(@user) }
        else
          render :json =>  {"message" => "注册失败"}.to_json, status: '200'
        end
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    # respond_to do |format|
    # 	if @user.update(user_params)
    # 		format.html { redirect_to @user, notice: 'User was successfully updated.' }
    # 		format.json { render :show, status: :ok, location: @user }
    # 	else
    # 		format.html { render :edit }
    # 		format.json { render json: @user.errors, status: :unprocessable_entity }
    # 	end
    # end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    # @user.destroy
    # respond_to do |format|
    # 	format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
    # 	format.json { head :no_content }
    # end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:name, :email,:mobile, :password, :password_confirmation)
  end

  def logged_in_user
    unless logged_in?
      redirect_to  sign_in_admin_sessions_path
    end
  end
end
