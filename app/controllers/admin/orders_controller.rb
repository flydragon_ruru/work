class Admin::OrdersController < Admin::ApplicationController
  def new
    @order = Order.new
    @good = Good.find(params[:good_id])
    @potential_students = current_company.potential_students.where(status: "未转化")
    @students = current_company.students
  end

  def create
    good = Good.find(params[:order][:good_id])
    if good.stock.blank?
      good.sale_amount += params[:order][:amount].to_i
      good.save!
    elsif good.stock.present? && good.stock > params[:order][:amount].to_i
      good.sale_amount += params[:order][:amount].to_i
      good.stock -= params[:order][:amount].to_i
      good.save!
    else
      #库存不足 如何实现还未定
      render js: "alert('库存不足!')"
      return 
    end

    order = Order.new(order_params)
    if params[:order][:order_type] == '意向学员购买'
      potential = PotentialStudent.find(params[:order][:student_id])

      if good.good_type != "课程商品"
        # 不会转化为正式学员，只记录意向学员交易记录
        order.potential_student_id = potential.id;
        order.student_id = 0;
      else
      	names = Student.column_names - (Student.column_names - PotentialStudent.column_names)
    		names -= %w(id created_at updated_at)
    		ps = Hash[names.map { |x| [x, potential.send(x)] }]
    		ps['potential_student_id'] = potential.id
    		student = Student.create! ps
        
            
            # 意向转化，删除意向学员
    		potential_student = PotentialStudent.find_by_id(student.potential_student_id);
    		# 意向学员周边商品，消费金额
    		total_price = Order.where(potential_student_id: potential_student.id).sum("total_price");
    		# potential_student.destroy;
    		potential_student.status = "已转化";
    		potential_student.save!;
            # 学员金额
    		student.accumulated_amount = total_price + order.total_price;
    		student.save!;

      	order.student_id = student.id
      	order.potential_student_id = 0;
	    end

    else
      student = Student.find(params[:order][:student_id]);
      # 学员金额
      student.accumulated_amount += order.total_price;
      student.save!;
    end
    order.studio_id = current_studio.id

    # kevin,商品交易记录添加employee_id
    order.employee_id = Employee.find_by_user_id(current_user.id).id;
    order.save!

    sale_course = SaleCourse.find(good.good_info_id)
    card_type_name = if sale_course.company_id.blank?
                       sale_course.name
                     elsif sale_course.course_info_id.present?
                       '直接售卖'
                     end

    if sale_course.sale_type != '周边商品'
      order.amount.times do
        StudentCard.create!(student_id: student.id,
                            card_type: card_type_name,
                            amount: good.amount,
                            price: order.real_price/good.amount,
                            course_info_id: sale_course.try(:course_info_id),
                            invalidated_at: good.month_of_validity && good.month_of_validity.months.since)
      end
    end

    redirect_to admin_good_path(order.good_id)
  end

  private
  def order_params
    params.require(:order).permit(:order_type, :student_id, :amount, :real_price, :total_price, :good_id, :studio_id)
  end
end
