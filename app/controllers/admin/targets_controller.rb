class Admin::TargetsController < ApplicationController
  layout 'admin'

  def index 

  end

  #======主理人给总监设置指标模块==========
  def director 
   positions = user_positions 
   unless positions.include?("主理人")
      respond_to do |format|
        format.html  { redirect_to admin_studios_path(company_id: current_company.id) }
      end
   end
  end


  def director_target 
    @month = Date.new(params[:year].to_i,params[:month].to_i).strftime("%Y-%m-%d")
    director_list
    respond_to do |format|
      format.js
    end
  end

  def new_director_target 
    @post = EmployeeOfStudio.find(params[:post_id]) 
    @month = params[:month]
    @employee = @post.employee
    @employee_of_studios = @employee.employee_of_studios.where(position_id: @post.position_id) 
    respond_to do |format|
      format.js
    end
  end

  def save_director_target 
    params[:directors_studio].zip(params[:directors_target]).each do |sid,amount|
      Target.create(month_of_target: Date.parse(params[:month]),amount: amount, employee_of_studio_id: params[:post_id], studio_id: sid )
    end
    @month = params[:month] 
    director_list
    respond_to do |format|
      format.js
    end
  end

  def edit_director_target 
    @target = Target.find(params[:target_id])
    @employee = @target.employee_of_studio.employee
    @month = params[:month]
    @post = @target.employee_of_studio

    @targets = Target.where(employee_of_studio_id: @target.employee_of_studio_id, month_of_target: @target.month_of_target)
    respond_to do |format|
      format.js
    end
  end

  def update_director_target 
    params[:target_ids].zip(params[:target_amounts]).each do |target, amount|
      @target = Target.find(target)
      @target.amount = amount
      @target.save
    end
    @month = params[:month] 
    director_list
    respond_to do |format|
      format.js
    end
  end
  #=end======主理人总监模块========end=


  #=begin=========市场总监给专员分配指标==========begin=
  def market
   positions = user_positions 
   unless positions.include?("市场总监")
      respond_to do |format|
        format.html  { redirect_to admin_studios_path(company_id: current_company.id) }
      end
   end
  end

  def market_target
    @month = Date.new(params[:year].to_i,params[:month].to_i).strftime("%Y-%m-%d")
    get_member_list("市场总监", "市场专员")
    respond_to do |format|
      format.js
    end
  end

  def new_market_target
    @post = EmployeeOfStudio.find(params[:post_id])
    @month = params[:month]
    @employee = @post.employee
  end

  def save_market_target
    target = Target.new(target_params)
    target.month_of_target = Date.parse(params[:month]) 
    target.save
    @month = params[:month] 
    get_member_list("市场总监", "市场专员")
    respond_to do |format|
      format.js
    end
  end

  def edit_market_target
    @target = Target.find(params[:target_id])
    @employee = @target.employee_of_studio.employee
    @month = params[:month]
  end

  def update_market_target
    @target = Target.find(params[:target_id])
    @target.amount = params[:amount]
    @target.save
    @month = params[:month] 
    get_member_list("市场总监", "市场专员")
  end


  #这一块需要意向学员的一下东西 暂时放一下
  def detail_market 
    @target = Target.find(params[:target_id])
    @employee_of_studio = @target.employee_of_studio 
    @student_group = current_company.student_group_configurations
    @studios = current_company.studios
    @month = params[:month]
    conditions = ["1=1"]
    conditions << "potential_students.market_employee_id = '#{@employee_of_studio.employee_user_id}'"
    conditions << "potential_students.created_at between '#{Date.parse(params[:month])}' and '#{Date.parse(params[:month]) + 1.month}'"
    @potential_students = PotentialStudent.where(conditions.join(" and ")).distinct
  end
  
  def get_market_detail_list 
    @target = Target.find(params[:target_id])
    @employee_of_studio = @target.employee_of_studio 
    @month = params[:month]
    params[:sex] ||= "all"
    params[:age_group] ||= "all"
    params[:willing_studio] ||= "all"
    params[:status] ||= "all"
    conditions = ["1=1"]
    conditions << "potential_students.market_employee_id = '#{@employee_of_studio.employee_user_id}'"
    conditions << "potential_students.created_at between '#{Date.parse(params[:month])}' and '#{Date.parse(params[:month]) + 1.month}'"
    if params[:age_group].present?
      if params[:age_group] == "幼儿"
        conditions << "potential_students.birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'"
      elsif params[:age_group] == "少年"
        conditions << "potential_students.birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'"
      elsif params[:age_group] == "青少年"
        conditions << "potential_students.birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'"
      elsif params[:age_group] == "成年"
        conditions << "potential_students.birthday < '#{Date.current - 18.year}'"
      end
    end
    conditions << "potential_students.willing_studio = #{params[:willing_studio]}" if params[:willing_studio] != "all" && params[:willing_studio].present?
    conditions << "potential_students.sex = '#{params[:sex]}'" if params[:sex] != "all" && params[:sex].present?
    conditions << "potential_students.student_group = #{params[:student_group]}" if params[:student_group] != "all" && params[:student_group].present?
    conditions << "potential_students.status = '#{params[:status]}'" if params[:status] != "all" && params[:status].present?
    @potential_students = PotentialStudent.where(conditions.join(" and ")).distinct
  end


  def assign_potential_students
    @potential_students = PotentialStudent.where(company_id: current_company.id, market_employee_id: nil).uniq
    p @potential_students
  end

  #=end=====市场专员的指标======end=
  

  #=begin=======服务顾问=======begin=

  def operator 
   positions = user_positions 
   unless positions.include?("运营总监")
      respond_to do |format|
        format.html  { redirect_to admin_studios_path(company_id: current_company.id) }
      end
   end
  end

  def operator_target 
    @month = Date.new(params[:year].to_i,params[:month].to_i).strftime("%Y-%m-%d")
    get_member_list("运营总监", "服务顾问")
    p @employee_of_studios
    respond_to do |format|
      format.js
    end
  end

  def new_operator_target
    @post = EmployeeOfStudio.find(params[:post_id])
    @month = params[:month]
    @employee = @post.employee
  end

  def save_operator_target 
    target = Target.new(target_params)
    target.month_of_target = Date.parse(params[:month]) 
    target.save
    @month = params[:month] 
    get_member_list("运营总监", "服务顾问")
  end

  def edit_operator_target 
    @target = Target.find(params[:target_id])
    @employee = @target.employee_of_studio.employee
    @month = params[:month]
  end

  def update_operator_target 
    @target = Target.find(params[:target_id])
    @target.amount = params[:amount]
    @target.save
    @month = params[:month] 
    get_member_list("运营总监", "服务顾问")
  end

  def detail_operator
    @target = Target.find(params[:target_id])
    @employee_of_studio = @target.employee_of_studio 
    @student_group = current_company.student_group_configurations
    @studios = current_company.studios
    @month = params[:month]
    conditions = ["1=1"]
    conditions << "students.service_employee_id = '#{@employee_of_studio.employee_user_id}'"
    conditions << "students.created_at between '#{Date.parse(params[:month])}' and '#{Date.parse(params[:month]) + 1.month}'"
    @students = Student.where(conditions.join(" and ")).distinct
    p @students
  end

  def get_operator_detail_list
    @target = Target.find(params[:target_id])
    @employee_of_studio = @target.employee_of_studio 
    @month = params[:month]
    params[:sex] ||= "all"
    params[:age_group] ||= "all"
    params[:studio_id] ||= "all"
    params[:status] ||= "all"
    
    conditions = ["1=1"]
    conditions << "students.service_employee_id = '#{@employee_of_studio.employee_user_id}'"
    conditions << "students.created_at between '#{Date.parse(params[:month])}' and '#{Date.parse(params[:month]) + 1.month}'"
    if params[:age_group].present?
      if params[:age_group] == "幼儿"
        conditions << "students.birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'"
      elsif params[:age_group] == "少年"
        conditions << "students.birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'"
      elsif params[:age_group] == "青少年"
        conditions << "students.birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'"
      elsif params[:age_group] == "成年"
        conditions << "students.birthday < '#{Date.current - 18.year}'"
      end
    end
    conditions << "students.studio_id = '#{params[:studio_id]}'" if  params[:studio_id] != "all" && params[:studio_id].present?
    conditions << "students.sex = '#{params[:sex]}'" if params[:sex] != "all" && params[:sex].present?
    conditions << "students.student_group = #{params[:student_group]}" if params[:student_group] != "all" && params[:student_group].present?
    @students = Student.where(conditions.join(" and ")).distinct
    p @students
  end

  #=end=========服务顾问========end==
  

  #=begin=======产品总监=======begin=
  def teacher
   positions = user_positions 
   unless positions.include?("产品总监")
      respond_to do |format|
        format.html  { redirect_to admin_studios_path(company_id: current_company.id) }
      end
   end
  end

  def teacher_target
    @month = Date.new(params[:year].to_i,params[:month].to_i).strftime("%Y-%m-%d")
    get_member_list("产品总监", "导师")
  end

  def new_teacher_target 
    @post = EmployeeOfStudio.find(params[:post_id])
    @month = params[:month]
    @employee = @post.employee
  end

  def save_teacher_target
    target = Target.new(target_params)
    target.month_of_target = Date.parse(params[:month]) 
    target.save
    @month = params[:month] 
    get_member_list("产品总监", "导师")
  end

  def edit_teacher_target
    @target = Target.find(params[:target_id])
    @employee = @target.employee_of_studio.employee
    @month = params[:month]
  end

  def update_teacher_target
    @target = Target.find(params[:target_id])
    @target.amount = params[:amount]
    @target.save
    @month = params[:month] 
    get_member_list("产品总监", "导师")
  end

  #=end=========产品总监========end==
  private

  def director_list
    company = current_company 
    position_ids = []
    PositionConfiguration.where(company_id: company.id).each do |p|
      if %w{产品总监 市场总监 运营总监}.include?(p.position_name) 
        position_ids << p.id
      end
    end
    #找到在这个公司左右属于总监位置的员工
    @employee_of_studios = EmployeeOfStudio.find_by_sql("select * from employee_of_studios where id in (select min(id) from employee_of_studios where position_id in (#{position_ids.join(',')}) group by employee_id, studio_company_id)")
    @studios = company.studios
  end

#  def market_list
#    company = current_company 
#    eos = EmployeeOfStudio.where(employee_user_id: current_user.id).
#      select{|es|  es if es.position_configuration.position_name == "市场总监"}
#    unless eos.blank?
#      studio_ids = []
#      eos.map {|es| studio_ids << es.studio_id}
#      market = PositionConfiguration.where(company_id: company.id, position_name: "市场专员").first
#      studio_ids = studio_ids.uniq.compact
#      @studios = Studio.find(studio_ids)
#      @director = eos.first
#      @employee_of_studios = EmployeeOfStudio.find_by_sql("select * from employee_of_studios where id in (select min(id) from employee_of_studios where position_id = #{market.id} and studio_id in (#{studio_ids.join(',')}) group by employee_id, studio_company_id)")
#    end
#  end
#
#  def operator_list 
#    company = current_company
#    operator_director = PositionConfiguration.where(company_id: company.id, position_name: "运营总监").first
#    eos = EmployeeOfStudio.where(employee_user_id: current_user.id,position_id: operator_director.id)
#    unless eos.blank?
#      studio_ids = []
#      eos.map {|es| studio_ids<< es.studio_id }
#      operator = PositionConfiguration.where(company_id: company.id, position_name: "服务顾问").first
#      @employee_of_studios = EmployeeOfStudio.where(position_id: operator.id).where("studio_id IN (#{studio_ids.join(",")})")
#    end
#  end
#
#  def teacher_list
#    company = current_company
#    teacher_director = PositionConfiguration.where(company_id: company.id, position_name: "产品总监").first
#    eos = EmployeeOfStudio.where(employee_user_id: current_user.id,position_id: teacher_director.id)
#    unless eos.blank?
#      studio_ids = []
#      eos.map {|es| studio_ids<< es.studio_id }
#      teacher = PositionConfiguration.where(company_id: company.id, position_name: "导师").first
#      @employee_of_studios = EmployeeOfStudio.where(position_id: teacher.id).where("studio_id IN (#{studio_ids.join(",")})")
#    end
#  end
#
  def get_member_list(director_poistion, member_poistion)
    company = current_company 
    eos = EmployeeOfStudio.where(employee_user_id: current_user.id).
      select{|es|  es if es.position_configuration.position_name == director_poistion}
    unless eos.blank?
      studio_ids = []
      eos.map {|es| studio_ids << es.studio_id}
      market = PositionConfiguration.where(company_id: company.id, position_name: member_poistion).first
      studio_ids = studio_ids.uniq.compact
      @studios = Studio.find(studio_ids)
      @director = eos.first
      @employee_of_studios = EmployeeOfStudio.find_by_sql("select * from employee_of_studios where id in (select min(id) from employee_of_studios where position_id = #{market.id} and studio_id in (#{studio_ids.join(',')}) group by employee_id, studio_company_id)")
    end
  end

  def user_positions 
    company = current_company 
    posts = EmployeeOfStudio.where(employee_user_id: current_user.id, studio_company_id: company.id)
    positions = posts.map {|p| p.position_configuration.position_name }
    return positions
  end

  def target_params
    params.require(:target).permit(:month, :amount,:employee_of_studio_id, :studio_company_id, :studio_id)
  end
end
