# 公司设置
class Admin::CompanyConfigurationsController < Admin::ApplicationController
  layout "admin"
  before_action :set_current_company
  #before_action :set_current_company, only: [:show, :edit, :update, :destroy]

  #删除公司设置
  def destroy_configuration
    if ['StudentGroupConfiguration', 'PotentialStudentGroupConfiguration', 'DepartmentConfiguration', 'PositionConfiguration', 'TeacherLevelConfiguration', 'EmployeeWelfareConfiguration', 'CourseRoomConfiguration'].include?params[:model_name]
      Object.const_get(params[:model_name]).destroy(params[:value])
    end
    render :text => 'ok'
  end

####  学员设置 ####
  def index
    @current_company = current_company
    @groups = @current_company.student_group_configurations
    @group = StudentGroupConfiguration.new()
    #@potential_groups = @current_company.potential_student_group_configurations
    #@potential_group= PotentialStudentGroupConfiguration.new()
  end

  #新增学员分组
  def create_student_group
    group = StudentGroupConfiguration.new(student_group_configuration_params)
    group.company_id = @current_company.id
    group.save!
    redirect_to admin_company_configurations_path
  end

  #新增意向学员分组
  #def create_potential_student_group
  #  potential_group = PotentialStudentGroupConfiguration.new(potential_student_group_configuration_params)
  #  potential_group.company_id = @current_company.id
  #  potential_group.save!
  #  redirect_to admin_company_configurations_path,:notice =>  "新增学员分组成功"
  #end

#### 课程设置 ####
  def course
    @course_rooms = @current_company.course_room_configurations
    @course_room = CourseRoomConfiguration.new()
    @current_studios = Studio.where(company_id: @current_company.id)
    @private_education = @current_company.private_education_configuration
    unless @private_education
      @private_education = PrivateEducationConfiguration.new
    end
  end

  #新增舞蹈教室
  def create_course_room
    course_room = CourseRoomConfiguration.new(course_room_params)
    course_room.company_id = @current_company.id
    course_room.save!
    redirect_to course_admin_company_configurations_path(tp: 'course')
  end

  #保存私教设置
  def update_private_education
    @private_education = @current_company.private_education_configuration
    if @private_education.present?
      @private_education.duration = private_education_params[:duration]
    else
      @private_education = PrivateEducationConfiguration.new(private_education_params)
      @private_education.company_id = @current_company.id
    end
    @private_education.save!
    redirect_to course_admin_company_configurations_path(tp: 'course')
  end

#### 商品设置 ####
  def goods
    goods_categories = @current_company.sale_courses.where(sale_type: '周边商品')
    @private_goods = goods_categories.privates
    @default_goods = goods_categories.defaults
    @goods_category = SaleCourse.new()
  end

  #新增商品分类
  def create_goods_category
    goods_category = SaleCourse.new(goods_category_params)
    goods_category.company_id = @current_company.id
    goods_category.sale_type = '周边商品'
    goods_category.save!
    redirect_to goods_admin_company_configurations_path(tp: 'goods')
  end

  #删除商品分类
  def destroy_goods_category
    goods_category = SaleCourse.find(params[:value])
    goods_category.destroy
  end

#### 人资设置 ####
  def employee
    @default_departments= @current_company.department_configurations.defaults
    @private_departments= @current_company.department_configurations.privates
    @department = DepartmentConfiguration.new()
    @default_positions= @current_company.position_configurations.defaults
    @private_positions= @current_company.position_configurations.privates
    @position = PositionConfiguration.new()
    @teacher_levels= @current_company.teacher_level_configurations
    @teacher_level = TeacherLevelConfiguration.new()
    @employee_welfares= @current_company.employee_welfare_configurations
    @employee_welfare = EmployeeWelfareConfiguration.new()
  end

  #新增自定义部门
  def create_department
    department = DepartmentConfiguration.new(department_configuration_params)
    department.company_id = @current_company.id
    department.save!
    redirect_to employee_admin_company_configurations_path(tp: 'employee')
  end

  #新增自定义职位
  def create_position
    position = PositionConfiguration.new(position_configuration_params)
    position.company_id = @current_company.id
    position.save!
    redirect_to employee_admin_company_configurations_path(tp: 'employee')
  end

  #新增导师等级
  def create_teacher_level
    teacher_level = TeacherLevelConfiguration.new(teacher_level_configuration_params)
    teacher_level.company_id = @current_company.id
    teacher_level.save!
    redirect_to employee_admin_company_configurations_path(tp: 'employee')
  end

  #新增员工福利
  def create_employee_welfare
    employee_welfare = EmployeeWelfareConfiguration.new(employee_welfare_configuration_params)
    employee_welfare.company_id = current_company.id
    employee_welfare.save!
    #respond_to do |format|
    #  format.js
    #end
    redirect_to employee_admin_company_configurations_path(tp: 'employee')
  end

#### 奖金设置 ####
  def bonus
    @bonus = @current_company.bonus_configuration
    unless @bonus
      @bonus = BonusConfiguration.new
    end
  end

  #修改公司名称
  def update_company_name
    @current_company = @current_company
    @current_company.update(name: params[:company][:name])
    @current_company.save!
    redirect_to bonus_admin_company_configurations_path(tp: 'bonus')
  end

  # 保存奖金设置
  def update_bonus
    @bonus = @current_company.bonus_configuration
    # 启用奖金池
    if params[:open] == "1"
      if @bonus.present?
        @bonus.update(bonus_percent: bonus_params[:bonus_percent], one_month_sale: bonus_params[:one_month_sale], total_sale: bonus_params[:total_sale])
      else
        @bonus = BonusConfiguration.new(bonus_params)
        @bonus.company_id = @current_company.id
        @bonus.save!
      end
    # 关闭奖金池
    else
      @bonus.destroy if @bonus.present?
    end
    redirect_to bonus_admin_company_configurations_path(tp: 'bonus')
  end

  private
    def set_current_company
      @current_company = current_company
    end
    def student_group_configuration_params
      params.require(:student_group_configuration).permit(:company_id, :group_name)
    end
    def potential_student_group_configuration_params
      params.require(:potential_student_group_configuration).permit(:company_id, :group_name)
    end
    def department_configuration_params
      params.require(:department_configuration).permit(:company_id, :department_name)
    end
    def position_configuration_params
      params.require(:position_configuration).permit(:company_id, :position_name)
    end
    def teacher_level_configuration_params
      params.require(:teacher_level_configuration).permit(:company_id, :teacher_level)
    end
    def employee_welfare_configuration_params
      params.require(:employee_welfare_configuration).permit(:company_id, :welfare_name, :amount)
    end
    def goods_category_params
      params.require(:sale_course).permit(:company_id, :name)
    end
    def course_room_params
      params.require(:course_room_configuration).permit(:company_id, :studio_id, :room_name, :location, :max_contain)
    end
    def private_education_params
      params.require(:private_education_configuration).permit(:company_id, :duration)
    end
    def bonus_params
      params.require(:bonus_configuration).permit(:company_id, :bonus_percent, :one_month_sale, :total_sale)
    end

end
