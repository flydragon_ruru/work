class Admin::StudentsController < ApplicationController

  layout 'studio'
  before_action :authorize_student


  def index
    @service_employees = EmployeeOfStudio.joins(:company, :position_configuration).where("employee_of_studios.studio_company_id = #{current_company.id} and position_configurations.position_name = '市场专员'").select(:employee_id).distinct 
    params[:paying_type] ||= "all"
    params[:sex] ||= "all"
    params[:age_group] ||= "all"
    params[:server] ||= "all"
    params[:learning_status] ||= "all"
    params[:expend_status] ||= "all"
    params[:expend_level] ||= "all"
    conditions = ["1=1"]
    conditions << "students.sex = '#{params[:sex]}'" if params[:sex] != "all" && params[:sex].present?
    conditions << "student_cards.card_type = '#{params[:paying_type]}'" if params[:paying_type] != "all" && params[:paying_type].present?
    conditions << "students.service_employee_id = '#{params[:server]}'" if params[:server] != "all" && params[:server].present?
    if params[:age_group].present?
      if params[:age_group] == "幼儿"
        conditions << "students.birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'"
      elsif params[:age_group] == "少年"
        conditions << "students.birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'"
      elsif params[:age_group] == "青少年"
        conditions << "students.birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'"
      elsif params[:age_group] == "成年"
        conditions << "students.birthday < '#{Date.current - 18.year}'"
      end
    end
    if params[:expend_level].present?
      if params[:expend_level] == "5-10k"
        conditions << "students.accumulated_amount between 5000 and 10000"
      elsif params[:expend_level] == "10-20k"
        conditions << "students.accumulated_amount between 10000 and 20000 "
      elsif params[:expend_level] == "20k以上"
        conditions << "students.accumulated_amount > 20000 "
      end
    end
    conditions << "(students.name like '%#{params[:search_student]}%' or students.mobile like '%#{params[:search_student]}%')" if params[:search_student] != "all" && params[:search_student].present?
    @students =  Student
      .joins("left join student_cards on student_cards.student_id = students.id")
      .where(conditions.join(" and "))
      .where(studio_id: current_studio.id)
      .distinct
      .paginate(page: params[:page], per_page: 5)
  end

  def get_student_list
    params[:paying_type] ||= "all"
    params[:sex] ||= "all"
    params[:age_group] ||= "all"
    params[:server] ||= "all"
    params[:learning_status] ||= "all"
    params[:expend_status] ||= "all"
    params[:expend_level] ||= "all"
    conditions = ["1=1"]
    conditions << "students.sex = '#{params[:sex]}'" if params[:sex] != "all" && params[:sex].present?
    conditions << "student_cards.card_type = '#{params[:paying_type]}'" if params[:paying_type] != "all" && params[:paying_type].present?
    conditions << "students.service_employee_id = '#{params[:server]}'" if params[:server] != "all" && params[:server].present?
    if params[:age_group].present?
      if params[:age_group] == "幼儿"
        conditions << "students.birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'"
      elsif params[:age_group] == "少年"
        conditions << "students.birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'"
      elsif params[:age_group] == "青少年"
        conditions << "students.birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'"
      elsif params[:age_group] == "成年"
        conditions << "students.birthday < '#{Date.current - 18.year}'"
      end
    end
    if params[:expend_level].present?
      if params[:expend_level] == "5-10k"
        conditions << "students.accumulated_amount between 5000 and 10000"
      elsif params[:expend_level] == "10-20k"
        conditions << "students.accumulated_amount between 10000 and 20000 "
      elsif params[:expend_level] == "20k以上"
        p '11111111111'
        conditions << "students.accumulated_amount > 20000 "
      end
    end
    conditions << "(students.name like '%#{params[:search_student]}%' or students.mobile like '%#{params[:search_student]}%')" if params[:search_student] != "all" && params[:search_student].present?
    @students =  Student
      .joins("left join student_cards on student_cards.student_id = students.id")
      .where(conditions.join(" and "))
      .distinct
      .paginate(page: params[:page], per_page: 5)
  end


  def show
    @student = Student.find(params[:id])
  end

  def edit
    @student = Student.find(params[:id])
    @student_group = current_company.student_group_configurations 
  end

  def save_student
    @student = Student.find(params[:student_id])
    @student.update!(student_params)
  end

  def orders
    @student = Student.find(params[:id])
    @orders = Order.where(" student_id = :student_id or potential_student_id = :potential_student_id", student_id: @student.id, potential_student_id: @student.potential_student_id)
      .order("created_at desc")
      .paginate(page: params[:page], per_page: 2)
  end


  private
  def student_params
    params.permit(student: [:name, :alias_name, :sex, :mobile, :weixin, :birthday, :address, :source_from, :student_group, :emergency_contact, :emergency_mobile, :willing_studio, :avatar,:studio_id, :company_id, :status, :remark])[:student].values[0]
  end

  #学员权限验证
  def authorize_student
    authorize Student,:index?
  end

end
