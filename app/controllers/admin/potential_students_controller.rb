class Admin::PotentialStudentsController < ApplicationController
  before_action :user_employee, only: [:index]
  before_action :authorize_potential_student


  layout 'studio'

  def index 
    # @positions = @employee.employee_of_studios.map { |em| em.position_configuration.position_name }
    # if @positions.include?("主理人") or @positions.include?("前台") or @positions.include?("市场总监") or @positions.include?("市场专员")
    #   params[:sex] ||= "all"
    #   params[:age_group] ||= "all"
    #   params[:status] ||= "all"
    #   params[:search_student] ||= ""

    #   conditions = ["1=1"]
    #   conditions << "potential_students.company_id = #{current_company.id}"
    #   conditions << "potential_students.sex = '#{params[:sex]}'" if params[:sex] != "all" && params[:sex].present?
    #   conditions << "potential_students.status = '#{params[:status]}'" if params[:status] != "all" && params[:status].present?
    #   conditions << "(potential_students.name like '%#{params[:search_student]}%' or potential_students.mobile like '%#{params[:search_student]}%')" if params[:search_student] != "all" && params[:search_student].present?
    #   if params[:age_group].present?
    #     if params[:age_group] == "幼儿"
    #       conditions << "potential_students.birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'"
    #     elsif params[:age_group] == "少年"
    #       conditions << "potential_students.birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'"
    #     elsif params[:age_group] == "青少年"
    #       conditions << "potential_students.birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'"
    #     elsif params[:age_group] == "成年"
    #       conditions << "potential_students.birthday < '#{Date.current - 18.year}'"
    #     end
    #   end
    #   @potential_students =  PotentialStudent.where(conditions.join(" and "))
    #     .distinct
    #     .paginate(page: params[:page], per_page: 5)
    # else
    #   respond_to do |format|
    #     format.html  { redirect_to admin_branchs_path(studio_id: current_studio.id) }
    #   end
    # end

    params[:sex] ||= "all"
    params[:age_group] ||= "all"
    params[:status] ||= "all"
    params[:search_student] ||= ""

    conditions = ["1=1"]
    conditions << "potential_students.company_id = #{current_company.id}"
    conditions << "potential_students.sex = '#{params[:sex]}'" if params[:sex] != "all" && params[:sex].present?
    conditions << "potential_students.status = '#{params[:status]}'" if params[:status] != "all" && params[:status].present?
    conditions << "(potential_students.name like '%#{params[:search_student]}%' or potential_students.mobile like '%#{params[:search_student]}%')" if params[:search_student] != "all" && params[:search_student].present?
    if params[:age_group].present?
      if params[:age_group] == "幼儿"
        conditions << "potential_students.birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'"
      elsif params[:age_group] == "少年"
        conditions << "potential_students.birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'"
      elsif params[:age_group] == "青少年"
        conditions << "potential_students.birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'"
      elsif params[:age_group] == "成年"
        conditions << "potential_students.birthday < '#{Date.current - 18.year}'"
      end
    end
    @potential_students =  PotentialStudent.where(conditions.join(" and "))
      .distinct
      .paginate(page: params[:page], per_page: 5)
  end

  def get_student_list
      params[:sex] ||= "all"
      params[:age_group] ||= "all"
      params[:status] ||= "all"
      params[:search_student] ||= "all"

      conditions = ["1=1"]
      conditions << "potential_students.company_id = #{current_company.id}"
      conditions << "potential_students.sex = '#{params[:sex]}'" if params[:sex] != "all" && params[:sex].present?
      conditions << "potential_students.status = '#{params[:status]}'" if params[:status] != "all" && params[:status].present?
      conditions << "(potential_students.name like '%#{params[:search_student]}%' or potential_students.mobile like '%#{params[:search_student]}%')" if params[:search_student] != "all" && params[:search_student].present?
      if params[:age_group].present?
        if params[:age_group] == "幼儿"
          conditions << "potential_students.birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'"
        elsif params[:age_group] == "少年"
          conditions << "potential_students.birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'"
        elsif params[:age_group] == "青少年"
          conditions << "potential_students.birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'"
        elsif params[:age_group] == "成年"
          conditions << "potential_students.birthday < '#{Date.current - 18.year}'"
        end
      end
      @potential_students =  PotentialStudent.where(conditions.join(" and "))
        .distinct
        .paginate(page: params[:page], per_page: 5)
  end

  def new 
    @student_group = current_company.student_group_configurations 
    @teachers = current_company.teachers
    @studios = current_company.studios
  end

  def create 
   @potential_student = PotentialStudent.new(potential_student_params[0])
   
   willing_teacher = params[:potential_student]["0"]["willing_teacher"].join(",") if params[:potential_student]['0']["willing_teacher"]
   @potential_student.willing_teacher = willing_teacher 
   @potential_student.studio_id = current_studio.id
   @potential_student.company_id = current_company.id
    respond_to do |format|
      if @potential_student.save
      format.js { render :js => "window.location.href = '/admin/potential_students'" }
      else
        render json:  { message: "创建意向学员失败"}
      end
    end
  end

  def check_student 
    pstudent = PotentialStudent.find_by(mobile: params[:mobile],company_id: current_company.id)
    if pstudent.present?
      render json:  { present: true}
    else
      render json:  { present: false}
    end
  end


  def show
    @potential_student = PotentialStudent.find(params[:id])
  end


  def edit 
    @potential_student = PotentialStudent.find(params[:id])
    @student_group = current_company.student_group_configurations 
    @teachers = current_company.teachers
    @studios = current_company.studios
  end

  def save_student
    @potential_student = PotentialStudent.find(params[:potential_student_id])
    @potential_student.update!(potential_student_params[0])
    willing_teacher = params[:potential_student]["0"]["willing_teacher"].join(",") if params[:potential_student]['0']["willing_teacher"]
    @potential_student.willing_teacher = willing_teacher 
    @potential_student.save!
  end


  def new_remark 
    @potential_student = PotentialStudent.find(params[:potential_student_id])
    @student_remark = StudentRemark.new(potential_student_id: @potential_student.id, remark_at: Date.current)
    respond_to do |format|
      format.js
    end
  end

  def save_remark
    student_remark = StudentRemark.new(student_remark_params)
    employee =  Employee.find_by(user_id: current_user.id, company_id: current_company.id)
    student_remark.market_employee_id = employee.id
    student_remark.save
  end

  private
  def potential_student_params
    params.permit(potential_student: [:name, :alias_name, :sex, :mobile, :weixin, :birthday, :address, :source_from, :student_group, :emergency_contact, :emergency_mobile, :learning_intention, :state_of_business, :learning_requirement, :willing_studio, :avatar,:studio_id, :company_id, :status])[:potential_student].values
  end


  def user_employee 
    @employee = Employee.find_by(user_id: current_user.id, company_id: current_company.id)
  end

  def student_remark_params 
    params.require(:student_remark).permit(:remark, :potential_student_id,:remark_at, :market_employee_id)
  end

  #招生管理权限验证
  def authorize_potential_student
    authorize PotentialStudent,:statistics_recruit?
  end


end
