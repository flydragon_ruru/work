class Admin::ServiceConsultantsController < ApplicationController
  layout 'studio'

  #学员数据权限验证
  before_action :authorize_student

  def index 
    params[:paying_type] ||= "all"
    params[:sex] ||= "all"
    params[:age_group] ||= "all"
    params[:learning_status] ||= "all"
    params[:expend_status] ||= "all"
    params[:expend_level] ||= "all"
    conditions = ["1=1"]

    employee = current_company.employees.find_by_user_id(current_user.id)
    conditions << "students.sex = '#{params[:sex]}'" if params[:sex] != "all" && params[:sex].present?
    
    # 服务顾问员工id＝ 当前登陆本人员工id
    # conditions << "students.service_employee_id = #{employee.id}" 
    
    if params[:age_group].present?
      if params[:age_group] == "幼儿"
        conditions << "students.birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'"
      elsif params[:age_group] == "少年"
        conditions << "students.birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'"
      elsif params[:age_group] == "青少年"
        conditions << "students.birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'"
      elsif params[:age_group] == "成年"
        conditions << "students.birthday < '#{Date.current - 18.year}'"
      end
    end
    conditions << "(students.name like '%#{params[:search_student]}%' or students.mobile like '%#{params[:search_student]}%')" if params[:search_student] != "all" && params[:search_student].present?
    @employee = Employee.find_by(user_id: current_user.id, company_id: current_company.id)
    @positions = @employee.employee_of_studios.map { |em| em.position_configuration.position_name }
    @students =  Student.where(conditions.join(" and "))
      .distinct
      .paginate(page: params[:page], per_page: get_per_page_num)
  end

  def get_student_list

    params[:paying_type] ||= "all"
    params[:sex] ||= "all"
    params[:age_group] ||= "all"
    params[:learning_status] ||= "all"
    params[:expend_status] ||= "all"
    params[:expend_level] ||= "all"
    conditions = ["1=1"]

    employee = current_company.employees.find_by_user_id(current_user.id)
    conditions << "students.sex = '#{params[:sex]}'" if params[:sex] != "all" && params[:sex].present?
    conditions << "students.service_employee_id = #{employee.id}" 
    if params[:age_group].present?
      if params[:age_group] == "幼儿"
        conditions << "students.birthday between '#{Date.current - 6.year}' and '#{Date.current - 3.year}'"
      elsif params[:age_group] == "少年"
        conditions << "students.birthday between '#{Date.current - 12.year}' and '#{Date.current - 6.year}'"
      elsif params[:age_group] == "青少年"
        conditions << "students.birthday between '#{Date.current - 18.year}' and '#{Date.current - 12.year}'"
      elsif params[:age_group] == "成年"
        conditions << "students.birthday < '#{Date.current - 18.year}'"
      end
    end
    conditions << "(students.name like '%#{params[:search_student]}%' or students.mobile like '%#{params[:search_student]}%')" if params[:search_student] != "all" && params[:search_student].present?
    @employee = Employee.find_by(user_id: current_user.id, company_id: current_company.id)
    @positions = @employee.employee_of_studios.map { |em| em.position_configuration.position_name }
    @students =  Student.where(conditions.join(" and "))
      .distinct
      .paginate(page: params[:page], per_page: get_per_page_num)
  end

  def show
    @student = Student.find(params[:id])
  end

  private

  def authorize_student
    authorize Student,:service_consultant?
  end

end
