
class SmsController < ApplicationController
  def captcha		
    target, mobile = params[:target], params[:mobile]
    user = User.mobile_with(mobile)
    if %w(signup bind_mobile).include?(target)
      if user.present?
        json_str = {"message" => "手机号已存在"}.to_json 
        #p flash
        render :json=>json_str, status=>'200'
      else
        SmsWorker.perform_async(mobile, target)
        latest_log = SmsLog.where(mobile: mobile).order('id desc').limit(1).first	
       # if latest_log.status == "send"
       #   render :json =>{message: "发送成功"}.to_json, status => '200'

       # else
       #   render :json =>{message: "发送失败"}.to_json, status => '200'
       # end		
      end
    elsif %w(modify_password reset_password).include?(target)
      if user.blank?
        json_str = {"message" => "手机号不存在，请确认你输入的手机号"}.to_json 
        #p flash
        render :json=>json_str, status=>'200'
      else
        SmsWorker.perform_async(mobile, target)
        latest_log = SmsLog.where(mobile: mobile).order('id desc').limit(1).first	
        if latest_log.status == "send"
          render :json =>{message: "发送成功"}.to_json, status => '200'

        else
          render :json =>{message: "发送失败"}.to_json, status => '200'
        end		
      end
    else
      json_str = {"message" => "参数错误"}.to_json 
      render :json=>json_str, status=>'200'
    end

  end
end
