class QiniuController < ApplicationController
   
  #获取上传的token
  def uptoken

  	qiniu_conf = QiniuConfService.call

  	# 构建上传策略，上传策略的更多参数请参照 http://developer.qiniu.com/article/developer/security/put-policy.html
  	put_policy = Qiniu::Auth::PutPolicy.new(
  	    qiniu_conf.bucket, # 存储空间
  	    nil,    # 指定上传的资源名，如果传入 nil，就表示不指定资源名，将使用默认的资源名
  	    3600    # token 过期时间，默认为 3600 秒，即 1 小时
  	)

	  #生成上传 Token
    uptoken = Qiniu::Auth.generate_uptoken(put_policy)
    
    respond_to do |format|
      format.json { render :json => {:uptoken => uptoken}, status: '200' }
    end
    
  end
   
  #获取下载的私有链接
  def private_url

    respond_to do |format|
      format.json { render :json => {:url => Qiniu::Auth.authorize_download_url(params[:url])}, status: '200' }
    end
  end
end
