#分店权限验证,首页 flydragon
class StudioPolicy < ApplicationPolicy

  def branchs_index?
  	@current_user.authorize_by_role?("#{record_model_name}_BranchsIndex")
  end

end
