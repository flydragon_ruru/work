#导师及导师统计权限控制 flydragon
class TeacherPolicy < ApplicationPolicy
	
  def index?
    @current_user.authorize_by_role?(record_model_name)
  end


  def data?
  	@current_user.authorize_by_role?("#{record_model_name}_StatisticalDatum")
  end

end
