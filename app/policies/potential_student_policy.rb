#招生管理权限验证 flydragon
class PotentialStudentPolicy < ApplicationPolicy
	
  def index?
    @current_user.authorize_by_role?(record_model_name)
  end


  def statistics_recruit?
    @current_user.authorize_by_role?("#{record_model_name}_Statistic")
  end
end
