#员工权限控制 flydragon
class EmployeePolicy < ApplicationPolicy

  def index?
    @current_user.authorize_by_role?(record_model_name)
  end

end
