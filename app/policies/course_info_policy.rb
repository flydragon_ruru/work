#课程管理权限控制 课程列表,课程周表,课程统计 flydragon
class CourseInfoPolicy < ApplicationPolicy

  def index?
    @current_user.authorize_by_role?(record_model_name)
  end

  def course_week?
    @current_user.authorize_by_role?("#{record_model_name}_CourseWeek")
  end

  def data?
  	@current_user.authorize_by_role?("#{record_model_name}_StatisticalDatum")
  end
end
