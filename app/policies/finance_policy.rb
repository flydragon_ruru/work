#财务以及财务统计权限验证 flydragon
class FinancePolicy < ApplicationPolicy
  def index?
    @current_user.authorize_by_role?(record_model_name)
  end


  def data?
  	@current_user.authorize_by_role?("#{record_model_name}_StatisticalDatum")
  end

end
