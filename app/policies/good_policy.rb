#商品以及售卖权限管理  flydragon
class GoodPolicy < ApplicationPolicy

  def index?
    @current_user.authorize_by_role?(record_model_name)
  end

  #商品售卖权限
  def goods_shop?
  	@current_user.authorize_by_role?("#{record_model_name}_goods_shop")
  end

  #统计数据权限
  def statistics_commodity?
     @current_user.authorize_by_role?("#{record_model_name}_Statistics")
  end

  #保存商品权限控制
  def save?
     !@current_user.is_receptionist?
  end

end
