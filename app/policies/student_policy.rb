#学员以及学生服务管理权限控制 flydragon
class StudentPolicy < ApplicationPolicy
  #学生管理权限
  def index?
    @current_user.authorize_by_role?(record_model_name)
  end

  #服务管理权限
  def service_consultant?
  	@current_user.authorize_by_role?("#{record_model_name}_ServiceConsultant")
  end


  #服务数据权限
  def statistics_student?
  	@current_user.authorize_by_role?("#{record_model_name}_Statistics")
  end

  # #学生管理中,消费筛选条件权限控制
  # def consumption_filter?
  #   @current_user.position("前台") && !@current_user.position("运营总监")
  # end


  # #学生管理中,消费筛选条件权限控制
  # def show_comment?
  #   @current_user.position("前台") && !@current_user.position("运营总监")
  # end

end
