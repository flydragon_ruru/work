#员工工资rake命令 员工工资初始化方法
namespace :salary do

  desc "统计上个月所有公司员工的工资"
  task :last_month => :environment do
    p "统计开始"
    month = Time.now.last_month.strftime("%Y-%m")
    
    flag = SalaryMonthStatistics::CompanyStatisticsService.call(nil,month)
    p "统计结束,月份为:#{month},结果为:#{flag}"
  end

  desc "统计当前月所有公司员工的工资"
  task :current_month => :environment do
    p "统计开始"
    month = Time.now.strftime("%Y-%m")

    flag = SalaryMonthStatistics::CompanyStatisticsService.call(nil,month)
    p "统计结束,月份为:#{month},结果为:#{flag}"
  end


  desc "统计上个月和当前月所有公司员工的工资(days )"
  task :last_and_current_month , [:days] => [:environment]  do |t,args|
    p "统计开始"

    days = args[:days].to_i
    
    last_month = Time.now.days_ago(days).strftime("%Y-%m")
    current_month = Time.now.strftime("%Y-%m")

    if last_month!=current_month

      last_month_flag = SalaryMonthStatistics::CompanyStatisticsService.call(nil,last_month)
      p "统计结束,上个月#{last_month}统计结果为#{last_month_flag}"
    end

    current_month_flag = SalaryMonthStatistics::CompanyStatisticsService.call(nil,current_month)

    p "统计结束,当月#{current_month}统计结果为#{current_month_flag}"
  end
end