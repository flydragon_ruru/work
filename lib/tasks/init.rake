#角色rake命令 角色初始化方法
namespace :init do

  desc "角色初始化命令"
  task :roles => :environment do
    p "角色初始化开始"
    roles_info = YAML.load_file('config/roles.yml')
    roles_info.each do |k,v|
      role_rows = Role.where(:code => k.to_s).count
      next if role_rows>0
      v[:code] = k
      Role.new(v).save
    end
    p "角色初始化结束"
  end


  desc "课程类型初始化命令"
  task :sale_course => :environment do
    p "课程类型初始化开始"
    %w{年卡 月卡 次卡 充值 课程商品}.each do |card|

      save_data = {:name => card,:company_id => nil,:sale_type => "课程商品"}
      next if SaleCourse.where(save_data).count > 0
      SaleCourse.new(save_data).save
    end
    p "课程类型初始化结束"
  end
end