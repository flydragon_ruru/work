#财务收支rake命令
namespace :finance do

  desc "统计所有公司当天的财务收支"
  task :current_day => :environment do
    p "统计开始"
    
    Company.all.each do |company|
      Finances::CompanyDateStatisticsService.call(company,Time.now)
    end
    p "统计结束"
  end

  desc "统计所有公司昨天的财务收支"
  task :yesterday => :environment do
    p "统计开始"
    
    Company.all.each do |company|
      Finances::CompanyDateStatisticsService.call(company,Time.now.yesterday)
    end
    p "统计结束"
  end

  desc "统计所有公司所有的财务收支(用于初始化时使用)"
  task :all => :environment do
    p "统计开始"
    
    Company.all.each do |company|
      (company.created_at.to_datetime..DateTime.current).each do |date|
        Finances::CompanyDateStatisticsService.call(company,date)
      end
    end
    p "统计结束"
  end
  
end