require 'net/http'
require 'uri'

module Yunpian
	module SmsClient
		# 模板接口发短信
		class << self
			@@send_sms_uri = URI.parse('https://sms.yunpian.com/v2/sms/single_send.json')

			def send_to(params)
				result = Net::HTTP.post_form(@@send_sms_uri,params)				
			end
		end
	end
end