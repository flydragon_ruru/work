# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

 user = User.new(mobile: '15123983041', name: "朦胧", password: '123456789', password_confirmation: '123456789')
 user.save

 user1 = User.new(mobile: '13121703261', name: "东瑶", password: '123456789', password_confirmation: '123456789')
 user1.save

 user2 = User.new(mobile: '18210241667', name: "可乐", password: '123456789', password_confirmation: '123456789')
 user2.save

 user3 = User.new(mobile: '13341015173', name: "Kevin", password: '123456789', password_confirmation: '123456789')
 user3.save
#
#
#  50.times do |i|
#  phone_num =	13121703261 + i
#  user = User.new(mobile: phone_num.to_s, name: "测试号_#{i+1}", password: '123456789', password_confirmation: '123456789')
#  user.save
#  end
#
#  user = User.first
#
#  company = Company.create(owner_id: 1,name: '我是测设一号')
#
#  3.times do |m|
#  	studio = Studio.create(name: "#{company.name}-分店#{m}", invalidate_at: DateTime.now.since(1.years), company_id: company.id)
#  end
#
#  %w{运营部 市场部 财务部 人资部 运营部}.each do |dept|
#  	DepartmentConfiguration.create(company_id: company.id, department_name: dept)
#  end
#
#  %w{主理人 市场总监 产品总监 运营总监 前台 导师 市场专员 服务顾问 财务总监 财务总监 会计 出纳 人资行政总监 HR 行政 }.each do |position|
#  	PositionConfiguration.create(company_id: company.id,position_name: position)
#  end
#
#  User.all.each do |u|
#  	Employee.create(name: "员工#{u.id}",alias_name: "employee#{u.id}", sex: "male",  mobile: "#{u.mobile}", user_id: u.id)
#  end
#
#
#  Employee.all.each do |em|
#  	EmployeeOfStudio.create(employee_id: em.id, studio_id: rand(3)+1 , studio_company_id: 1, employee_user_id: em.user.id, position_id: rand(15)+1, department_id: rand(5)+1)
#  end

# employee_teachers = EmployeeOfStudio.where(position_id: 6)
# employee_teachers.each do |employee_teacher|
#   teacher = Teacher.create(employee_of_studio_id: employee_teacher.id, name: employee_teacher.employee_user_name, studio_id: employee_teacher.studio_id, company_id: employee_teacher.studio_company_id, age_group: "成年", teacher_level: "初级导师", dance_type: "House")
#   p teacher
# end

#Employee.all.each do |em|
#  if em.id % 2 == 0
#    em.sex = "男"
#  else
#    em.sex = "女"
#  end
#  em.save
#end

#%w{年卡 月卡 次卡 充值 课程商品}.each do |card|
#  SaleCourse.create!(name: card)
#end


#   PotentialStudent.all.each do |ps|
#    Student.create!(potential_student_id: ps.id, name: ps.name, alias_name: ps.alias_name, sex: ps.sex, mobile: ps.mobile, weixin: ps.weixin, student_group: ps.student_group, birthday: ps.birthday, address: ps.address, source_from: ps.source_from, emergency_contact: ps.emergency_contact, emergency_mobile: ps.emergency_mobile, learning_requirement: ps.learning_requirement, state_of_business: ps.state_of_business, learning_intention: ps.learning_intention,willing_studio: ps.willing_studio, willing_teacher: ps.willing_teacher, avatar: ps.avatar, studio_id: ps.studio_id, company_id: ps.company_id, market_employee_id: ps.market_employee_id)
#   end

# Course.all.each do |co|
#   co.day_of_week = Date.parse(co.course_time.strftime('%Y-%m-%d'))
#   co.save
# end


# Employee.each do |ee|
# ee.joined_at = Date.current
# ee.save
# end