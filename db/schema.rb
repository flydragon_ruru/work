# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170227104613) do

  create_table "appointment_records", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "course_id",                   comment: "课程ID"
    t.integer  "student_id",                  comment: "学生ID"
    t.integer  "course_info_id",              comment: "课程配置ID"
    t.datetime "appointment_at",              comment: "预约时间"
    t.integer  "operated_by",                 comment: "操作人员"
    t.integer  "studio_id",                   comment: "分店ID"
    t.integer  "company_id",                  comment: "公司ID"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "bonus_configurations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",                  comment: "公司ID"
    t.integer  "bonus_percent",               comment: "奖金比例"
    t.boolean  "one_month_sale",              comment: "单月业绩奖金"
    t.boolean  "total_sale",                  comment: "累计业绩奖金"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "charge_orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "user_id",                               comment: "用户唯一标示ID"
    t.string   "number",                                comment: "订单号"
    t.text     "title",      limit: 65535,              comment: "订单信息"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "check_ins", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "course_id",                comment: "课程id"
    t.integer  "student_id",               comment: "学生id"
    t.integer  "teacher_id",               comment: "导师id"
    t.integer  "operated_by",              comment: "操作员工id，employee_id"
    t.datetime "checked_at",               comment: "签到时间"
    t.string   "pay_way",                  comment: "支付方式"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["course_id"], name: "index_check_ins_on_course_id", using: :btree
    t.index ["operated_by"], name: "index_check_ins_on_operated_by", using: :btree
    t.index ["student_id"], name: "index_check_ins_on_student_id", using: :btree
    t.index ["teacher_id"], name: "index_check_ins_on_teacher_id", using: :btree
  end

  create_table "companies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name",                    comment: "公司名称"
    t.integer  "owner_id",                comment: "主理人"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_companies_on_name", using: :btree
    t.index ["owner_id"], name: "index_companies_on_owner_id", using: :btree
  end

  create_table "course_infos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "sale_type",                                         default: "单次售卖",              comment: "售卖类型"
    t.string   "course_type",                                       default: "常规课",               comment: "课程类型"
    t.string   "name",                                              default: "",                  comment: "课程名字"
    t.integer  "company_id",                                                                      comment: "公司ID"
    t.integer  "teacher_id",                                                                      comment: "导师"
    t.integer  "room_id",                                                                         comment: "教室编号"
    t.string   "dance_type",                                        default: "",                  comment: "舞种"
    t.string   "age_group",                                         default: "成年",                comment: "授课对象(年龄分组)"
    t.integer  "max_contain",                                                                     comment: "最大人数"
    t.decimal  "duration",                           precision: 10, default: 0,                   comment: "课程时长"
    t.boolean  "appointmented_or_not",                              default: false,               comment: "是否需要预约"
    t.date     "course_begin_at",                                                                 comment: "开始时长"
    t.date     "course_end_at",                                                                   comment: "结束时长"
    t.boolean  "sale_or_not",                                       default: false,               comment: "是否销售"
    t.text     "pay_ways",             limit: 65535,                                              comment: "消费方式 json数据串"
    t.string   "status",                                            default: "正常",                comment: "是否结课"
    t.text     "course_set",           limit: 65535,                                              comment: "课程时间设置的json串"
    t.integer  "period",                                                                          comment: "课时周期"
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
  end

  create_table "course_reckons", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "teacher_id",                                                          comment: "导师ID"
    t.integer  "course_info_id",                                                      comment: "课程ID"
    t.decimal  "amount",         precision: 8, scale: 2, default: "0.0",              comment: "这次课消费的金额"
    t.date     "reckon_at",                                                           comment: "课消时间"
    t.integer  "studio_id",                                                           comment: "分店ID"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.integer  "student_id",                                                          comment: "学生ID"
    t.string   "pay_way",                                                             comment: "本次课消费方式"
    t.integer  "course_id",                                                           comment: "具体什么时候的课"
  end

  create_table "course_records", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "course_id",                comment: "课程ID"
    t.integer  "student_id",               comment: "学生ID"
    t.string   "paying_way",               comment: "支付方式"
    t.integer  "operator_id",              comment: "操作人员"
    t.string   "record_type",              comment: "暂时忘了"
    t.integer  "studio_id",                comment: "分店ID"
    t.integer  "company_id",               comment: "公司ID"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "course_room_configurations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",               comment: "公司ID"
    t.integer  "studio_id",                comment: "分店ID"
    t.string   "room_name",                comment: "教室名"
    t.string   "location",                 comment: "教室位置"
    t.integer  "max_contain",              comment: "人数上限"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "courses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.datetime "course_time",                 comment: "上课时间"
    t.string   "day_of_week",                 comment: "星期几"
    t.integer  "course_info_id",              comment: "哪个课程的基本配置"
    t.integer  "teacher_id",                  comment: "哪个老师的课"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "department_configurations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",                   comment: "公司ID"
    t.string   "department_name",              comment: "部门名称"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "discounts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "discount_type",              comment: "优惠类型"
    t.date     "begin_at",                   comment: "时间优惠的 起始时间"
    t.date     "end_at",                     comment: "时间优惠的截止时间"
    t.integer  "amount",                     comment: "数量优惠的数量"
    t.integer  "percent",                    comment: "折扣比例"
    t.integer  "good_id",                    comment: "商品ID"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "employee_of_studios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "employee_id",                    comment: "员工ID"
    t.integer  "studio_id",                      comment: "分店ID"
    t.integer  "employee_user_id",               comment: "用户ID"
    t.integer  "studio_company_id",              comment: "公司ID"
    t.integer  "position_id",                    comment: "职位信息ID"
    t.string   "position_name",                  comment: "职位名"
    t.integer  "department_id",                  comment: "部门ID"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "employee_welfare_configurations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",                                           comment: "公司ID"
    t.string   "welfare_name",                                         comment: "福利名"
    t.decimal  "amount",       precision: 10, default: 0,              comment: "福利金额"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "employees", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id",                         default: 0,                  comment: "用户ID"
    t.integer  "company_id",                                                   comment: "公司ID"
    t.string   "name",              limit: 15,    default: "",                 comment: "姓名"
    t.string   "alias_name",        limit: 15,    default: "",                 comment: "别名"
    t.string   "avatar",            limit: 100,   default: "",                 comment: "头像"
    t.string   "sex",               limit: 15,    default: "",                 comment: "性别"
    t.string   "mobile",            limit: 11,    default: "",                 comment: "手机号码"
    t.date     "birthday",                                                     comment: "出生日期"
    t.string   "address",           limit: 50,    default: "",                 comment: "联系地址"
    t.string   "weixin",            limit: 15,    default: "",                 comment: "微信号码"
    t.string   "emergency_contact", limit: 15,    default: "",                 comment: "紧急联系人"
    t.string   "emergency_mobile",  limit: 15,    default: "",                 comment: "联系人电话"
    t.text     "remark",            limit: 65535,                              comment: "备注信息"
    t.string   "status",            limit: 15,    default: "",                 comment: "状态"
    t.boolean  "disabled",                        default: false,              comment: "状态, 是否被禁用"
    t.datetime "joined_at",                                                    comment: "入职时间"
    t.datetime "dismissed_at",                                                 comment: "离职时间"
    t.datetime "deleted_at",                                                   comment: "paranoia软删除"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.index ["alias_name"], name: "index_employees_on_alias_name", using: :btree
    t.index ["birthday"], name: "index_employees_on_birthday", using: :btree
    t.index ["company_id"], name: "index_employees_on_company_id", using: :btree
    t.index ["deleted_at"], name: "index_employees_on_deleted_at", using: :btree
    t.index ["disabled"], name: "index_employees_on_disabled", using: :btree
    t.index ["mobile"], name: "index_employees_on_mobile", using: :btree
    t.index ["name"], name: "index_employees_on_name", using: :btree
    t.index ["sex"], name: "index_employees_on_sex", using: :btree
    t.index ["status"], name: "index_employees_on_status", using: :btree
    t.index ["user_id"], name: "index_employees_on_user_id", using: :btree
  end

  create_table "finances", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.integer  "studio_id",                                                                           comment: "工作室ID"
    t.integer  "company_id",                                                                          comment: "公司ID"
    t.string   "finance_type",     limit: 50,                            default: "",                 comment: "账务种类"
    t.string   "finance_category", limit: 50,                            default: "",                 comment: "账务类型"
    t.decimal  "amount",                         precision: 8, scale: 2,                              comment: "账务金额"
    t.text     "remark",           limit: 65535,                                                      comment: "备注"
    t.boolean  "initial_state",                                          default: false,              comment: "是否初始值"
    t.boolean  "is_system",                                              default: false,              comment: "是否是系统录入"
    t.date     "setdate",                                                                             comment: "设置时间"
    t.boolean  "disabled",                                               default: false,              comment: "状态, 是否被禁用"
    t.datetime "deleted_at",                                                                          comment: "paranoia软删除"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.index ["company_id"], name: "index_finances_on_company_id", using: :btree
    t.index ["created_at"], name: "index_finances_on_created_at", using: :btree
    t.index ["deleted_at"], name: "index_finances_on_deleted_at", using: :btree
    t.index ["disabled"], name: "index_finances_on_disabled", using: :btree
    t.index ["finance_category"], name: "index_finances_on_finance_category", using: :btree
    t.index ["finance_type"], name: "index_finances_on_finance_type", using: :btree
    t.index ["initial_state"], name: "index_finances_on_initial_state", using: :btree
    t.index ["studio_id"], name: "index_finances_on_studio_id", using: :btree
  end

  create_table "fixed_costs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name",                      default: "",                 comment: "成本名称"
    t.decimal  "amount",     precision: 10, default: 0,                  comment: "成本费用"
    t.integer  "cycle",                                                  comment: "周期"
    t.boolean  "disabled",                  default: false,              comment: "状态, 是否被禁用"
    t.datetime "deleted_at",                                             comment: "paranoia软删除"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  create_table "fixed_costs_studios", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer "studio_id"
    t.integer "fixed_cost_id"
    t.index ["fixed_cost_id"], name: "index_fixed_costs_studios_on_fixed_cost_id", using: :btree
    t.index ["studio_id"], name: "index_fixed_costs_studios_on_studio_id", using: :btree
  end

  create_table "goods", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",                                                                          comment: "公司ID"
    t.string   "good_type",                                                                           comment: "商品分类"
    t.integer  "good_info_id",                                                                        comment: "商品类型"
    t.string   "name",                                                                                comment: "商品名称"
    t.integer  "amount",                                                                              comment: "商品规格 - 课数"
    t.decimal  "price",                           precision: 8, scale: 2,                             comment: "商品价格"
    t.integer  "stock",                                                                               comment: "库存"
    t.integer  "month_of_validity",                                                                   comment: "商品有效期"
    t.text     "remark",            limit: 65535,                                                     comment: "简介"
    t.string   "avatar",                                                                              comment: "商品图片"
    t.string   "status",                                                  default: "正常",              comment: "商品状态"
    t.integer  "sale_amount",                                             default: 0,                 comment: "商品销量"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
  end

  create_table "orders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "order_type",                                       comment: "订单类型"
    t.integer  "potential_student_id",                             comment: "意向学员ID"
    t.integer  "student_id",                                       comment: "正式学员ID"
    t.integer  "amount",                                           comment: "数量"
    t.decimal  "real_price",           precision: 10,              comment: "单价"
    t.decimal  "total_price",          precision: 10,              comment: "总价"
    t.integer  "good_id",                                          comment: "商品ID"
    t.integer  "studio_id",                                        comment: "分店ID"
    t.integer  "employee_id",                                      comment: "操作员工ID"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["employee_id"], name: "index_orders_on_employee_id", using: :btree
    t.index ["good_id"], name: "index_orders_on_good_id", using: :btree
    t.index ["potential_student_id"], name: "index_orders_on_potential_student_id", using: :btree
    t.index ["student_id"], name: "index_orders_on_student_id", using: :btree
    t.index ["studio_id"], name: "index_orders_on_studio_id", using: :btree
  end

  create_table "pack_sales", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "course_info_id",                          comment: "打包的课程ID"
    t.integer  "amount",         default: 0,              comment: "剩余次数"
    t.integer  "student_id",                              comment: "学生ID"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "paying_ways", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "pay_type",                            comment: "本课程可付款方式"
    t.integer  "amount",                              comment: "本课程需要支付的价格"
    t.integer  "course_info_id",                      comment: "那个课的支付方式"
    t.integer  "course_info_teacher_id",              comment: "这个字段好像可以删掉"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "position_configurations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",                 comment: "公司ID"
    t.string   "position_name",              comment: "职位名"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "position_configurations_roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer "position_configuration_id", comment: "职务外健"
    t.integer "role_id",                   comment: "角色外健"
    t.index ["position_configuration_id"], name: "index_position_configurations_roles_on_position_configuration_id", using: :btree
    t.index ["role_id"], name: "index_position_configurations_roles_on_role_id", using: :btree
  end

  create_table "potential_student_group_configurations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",              comment: "公司ID"
    t.string   "group_name",              comment: "意向学员分组名"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "potential_students", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name",                               default: "",                 comment: "意向学员名字"
    t.string   "alias_name",                         default: "",                 comment: "英文别名"
    t.string   "sex",                                default: "女",                comment: "性别"
    t.string   "mobile",                             default: "",                 comment: "手机号码"
    t.string   "weixin",                             default: "",                 comment: "微信号"
    t.integer  "student_group",                                                   comment: "学员分组"
    t.date     "birthday",                                                        comment: "出生年月"
    t.string   "address",                            default: "",                 comment: "联系地址"
    t.string   "source_from",                        default: "",                 comment: "信息来源"
    t.string   "emergency_contact",                  default: "",                 comment: "紧急联系人"
    t.string   "emergency_mobile",                   default: "",                 comment: "紧急联系电话"
    t.text     "learning_requirement", limit: 65535,                              comment: "学习需求"
    t.text     "state_of_business",    limit: 65535,                              comment: "经济情况"
    t.text     "learning_intention",   limit: 65535,                              comment: "学习意向"
    t.string   "willing_teacher",                    default: "",                 comment: "意向导师"
    t.integer  "willing_studio",                                                  comment: "意向分店"
    t.string   "avatar",                             default: "",                 comment: "头像"
    t.integer  "studio_id",                                                       comment: "工作室id"
    t.integer  "company_id",                                                      comment: "公司ID"
    t.integer  "market_employee_id",                                              comment: "市场专员ID"
    t.string   "status",                             default: "未转化",              comment: "转换状态"
    t.date     "switching_time",                                                  comment: "转换时间"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end

  create_table "private_education_configurations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",              comment: "公司ID"
    t.integer  "duration",                comment: "私教时长"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "code",                    comment: "角色编码，用于逻辑判断"
    t.string   "name",                    comment: "角色名称，用于展示"
    t.string   "describe",                comment: "角色描述，用于展示"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "salaries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "employee_id",                   comment: "员工ID"
    t.integer  "employee_user_id",              comment: "用户ID"
    t.string   "salary_type",                   comment: "工资类型"
    t.integer  "amount",                        comment: "金额"
    t.integer  "range_min",                     comment: "阶段范围最小值"
    t.integer  "range_max",                     comment: "阶段范围最大值"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "salary_month_statistics", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",                                comment: "公司ID"
    t.string   "month",             limit: 20,              comment: "月份"
    t.integer  "employee_id",                               comment: "员工id"
    t.string   "employee_name",     limit: 50,              comment: "员工姓名"
    t.string   "position_names"
    t.float    "base_salary",       limit: 24,              comment: "基本工资"
    t.float    "commission_salary", limit: 24,              comment: "提成工资"
    t.float    "sum_salary",        limit: 24,              comment: "总工资"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "studio_ids",                                comment: "当前员工所拥有门店的id,eg:(1),(2)"
    t.string   "department_ids",                            comment: "当前员工所拥有部门的名称,eg:运营部,销售部"
  end

  create_table "sale_courses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name",                        comment: "售卖课程名称"
    t.string   "sale_type",                   comment: "售卖课程类型"
    t.integer  "course_info_id",              comment: "课程信息ID"
    t.integer  "company_id",                  comment: "公司ID"
    t.datetime "deleted_at",                  comment: "删除时间"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "sms_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "user_id",                 default: 0,               comment: "用户ID"
    t.string   "mobile",       limit: 11, default: "",              comment: "手机号"
    t.string   "sms_type",     limit: 20, default: "",              comment: "短信类型"
    t.string   "content",                                           comment: "短信内容"
    t.string   "content_hash",                                      comment: "短信hash"
    t.datetime "expired_at",                                        comment: "过期时间"
    t.string   "status",                  default: "",              comment: "waiting, sent, received, failed"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["sms_type", "mobile", "expired_at"], name: "index_sms_logs_on_sms_type_and_mobile_and_expired_at", using: :btree
    t.index ["user_id"], name: "index_sms_logs_on_user_id", using: :btree
  end

  create_table "student_cards", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "student_id",                                                          comment: "学生ID"
    t.string   "card_type",                                                           comment: "卡的种类"
    t.decimal  "amount",         precision: 8, scale: 2, default: "0.0",              comment: "金额或次数"
    t.integer  "course_info_id",                                                      comment: "直接售卖会关联课程"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.decimal  "price",          precision: 8, scale: 2, default: "0.0",              comment: "一次次卡的价格或一次打包课的价格"
    t.datetime "invalidated_at"
  end

  create_table "student_group_configurations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",              comment: "公司ID"
    t.string   "group_name",              comment: "学员分组名"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "student_remarks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "potential_student_id",                                comment: "意向学员id"
    t.integer  "student_id",                                          comment: "学员id"
    t.text     "remark",                   limit: 65535,              comment: "回访记录或评价记录"
    t.date     "remark_at",                                           comment: "回访时间或评价时间"
    t.integer  "market_employee_id",                                  comment: "市场专员ID"
    t.integer  "service_employee_id",                                 comment: "服务顾问ID"
    t.integer  "teacher_employee_id",                                 comment: "导师的ID"
    t.integer  "receptionist_employee_id",                            comment: "前台的ID"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "students", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "potential_student_id",                                                                  comment: "意向学员ID"
    t.string   "name",                                                       default: "",               comment: "学员名字"
    t.string   "alias_name",                                                 default: "",               comment: "英文别名"
    t.string   "sex",                                                        default: "女",              comment: "性别"
    t.string   "mobile",                                                     default: "",               comment: "手机号码"
    t.string   "weixin",                                                     default: "",               comment: "微信号"
    t.integer  "student_group",                                                                         comment: "学员分组"
    t.date     "birthday",                                                                              comment: "出生年月"
    t.string   "address",                                                    default: "",               comment: "联系地址"
    t.string   "source_from",                                                default: "",               comment: "信息来源"
    t.string   "emergency_contact",                                          default: "",               comment: "紧急联系人"
    t.string   "emergency_mobile",                                           default: "",               comment: "紧急联系电话"
    t.text     "learning_requirement", limit: 65535,                                                    comment: "学习需求"
    t.text     "state_of_business",    limit: 65535,                                                    comment: "经济情况"
    t.text     "learning_intention",   limit: 65535,                                                    comment: "学习意向"
    t.string   "willing_teacher",                                            default: "",               comment: "意向导师"
    t.integer  "willing_studio",                                                                        comment: "意向分店"
    t.string   "avatar",                                                     default: "",               comment: "头像"
    t.integer  "studio_id",                                                                             comment: "工作室id"
    t.integer  "company_id",                                                                            comment: "公司ID"
    t.integer  "market_employee_id",                                                                    comment: "市场专员ID"
    t.integer  "service_employee_id",                                                                   comment: "服务顾问ID"
    t.text     "remark",               limit: 65535,                                                    comment: "备注"
    t.decimal  "accumulated_amount",                 precision: 8, scale: 2,                            comment: "累计消费金额"
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
  end

  create_table "studios", force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=utf8" do |t|
    t.string   "name",          limit: 15,                               comment: "名称"
    t.string   "uuid",          limit: 32,                               comment: "uuid"
    t.string   "avatar",        limit: 100,                              comment: "头像"
    t.boolean  "is_chain",                  default: false,              comment: "是否连锁工作室"
    t.datetime "invalidate_at",                                          comment: "到期时间"
    t.datetime "last_buy_at",                                            comment: "上次购买时间"
    t.integer  "company_id",                                             comment: "公司ID"
    t.boolean  "disabled",                  default: false,              comment: "状态, 是否被禁用"
    t.datetime "deleted_at",                                             comment: "paranoia软删除"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.index ["company_id"], name: "index_studios_on_company_id", using: :btree
    t.index ["deleted_at"], name: "index_studios_on_deleted_at", using: :btree
    t.index ["disabled"], name: "index_studios_on_disabled", using: :btree
    t.index ["invalidate_at"], name: "index_studios_on_invalidate_at", using: :btree
    t.index ["name"], name: "index_studios_on_name", using: :btree
    t.index ["uuid"], name: "index_studios_on_uuid", using: :btree
  end

  create_table "studios_finances", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer "studio_id"
    t.integer "finance_id"
    t.index ["finance_id"], name: "index_studios_finances_on_finance_id", using: :btree
    t.index ["studio_id"], name: "index_studios_finances_on_studio_id", using: :btree
  end

  create_table "targets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "employee_of_studio_id",                          comment: "职位ID"
    t.integer  "amount",                default: 0,              comment: "课程ID"
    t.date     "month_of_target",                                comment: "月目标数"
    t.integer  "setted_by",                                      comment: "谁设置的"
    t.integer  "company_id",                                     comment: "公司ID"
    t.integer  "studio_id",                                      comment: "分店ID"
    t.integer  "employee_user_id",                               comment: "用户ID"
    t.integer  "employee_id",                                    comment: "雇员ID"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "teacher_level_configurations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id",                 comment: "公司ID"
    t.string   "teacher_level",              comment: "导师等级"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "teachers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "employee_of_studio_id",              comment: "员工职位中间表ID"
    t.string   "name",                               comment: "导师名"
    t.string   "age_group",                          comment: "授课对象"
    t.string   "teacher_level",                      comment: "导师等级"
    t.string   "dance_type",                         comment: "舞种"
    t.integer  "studio_id",                          comment: "分店ID"
    t.integer  "company_id",                         comment: "公司ID"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name",            limit: 15,                              comment: "姓名"
    t.string   "mobile",          limit: 15,                              comment: "手机号"
    t.string   "email",           limit: 50,                              comment: "邮箱"
    t.string   "password_digest",                                         comment: "密码"
    t.boolean  "disabled",                   default: false,              comment: "状态, 是否被禁用"
    t.datetime "deleted_at",                                              comment: "paranoia软删除"
    t.integer  "last_company_id",                                         comment: "最后一次登录的工作室或分店"
    t.integer  "last_studio_id",                                          comment: "最后一次登录的工作室或分店"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

end
