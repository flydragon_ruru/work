class CreateCourseReckons < ActiveRecord::Migration[5.0]
  def change
    #课消核算
    create_table :course_reckons do |t|
      t.integer      :teacher_id, comment: "导师ID"
      t.integer      :course_info_id, comment: "课程ID"
      t.integer      :amount, comment: "课消金额"
      t.date         :reckon_at, comment: "课消时间"
      t.integer      :studio_id, comment: "分店ID"

      t.timestamps
    end
  end
end
