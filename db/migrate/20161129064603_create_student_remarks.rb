class CreateStudentRemarks < ActiveRecord::Migration[5.0]
  def change
    #学员意向学员回访和评价
    create_table :student_remarks do |t|
      t.integer :potential_student_id,      comment: "意向学员id"
      t.integer :student_id,                comment: "学员id"
      t.text    :remark,                    comment: "回访记录或评价记录"
      t.date    :remark_at,                 comment: "回访时间或评价时间"
      t.integer :market_employee_id,        comment: "市场专员ID"
      t.integer :service_employee_id,       comment: "服务顾问ID"
      t.integer :teacher_employee_id,       comment: "导师的ID"
      t.integer :receptionist_employee_id,  comment: "前台的ID"

      t.timestamps
    end
  end
end
