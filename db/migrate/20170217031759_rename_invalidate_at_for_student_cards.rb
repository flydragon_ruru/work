#重新更新学员卡到期时间字段  flydragon
class RenameInvalidateAtForStudentCards < ActiveRecord::Migration[5.0]
  def change
  	rename_column :student_cards, :invalidate_at, :invalidated_at
  end
end
