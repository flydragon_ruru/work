class CreateDiscounts < ActiveRecord::Migration[5.0]
  def change
    #折扣表
    create_table :discounts do |t|
      t.string          :discount_type,        comment: '优惠类型'
      t.date            :begin_at,             comment: '时间优惠的 起始时间'
      t.date            :end_at,               comment: '时间优惠的截止时间'
      t.integer         :amount,               comment: '数量优惠的数量'
      t.integer         :percent,              comment: '折扣比例'
      t.integer         :good_id,              comment: '商品ID'

      t.timestamps
    end
  end
end
