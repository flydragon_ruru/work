class CreateBonusConfiguration < ActiveRecord::Migration[5.0]
  def change
  	#奖金池
    create_table :bonus_configurations do |t|
      t.integer :company_id,           comment: '公司ID'
      t.integer :bonus_percent,        comment: '奖金比例'
      t.boolean :one_month_sale,       comment: '单月业绩奖金'
      t.boolean :total_sale,           comment: '累计业绩奖金'

      t.timestamps
    end
  end
end
