class CreateTargets < ActiveRecord::Migration[5.0]
  def change
    create_table :targets do |t|
      t.integer :employee_of_studio_id, comment: "职位ID"
      t.integer :amount, default: 0, comment: "课程ID"
      t.date :month_of_target, comment: "月目标数"
      t.integer :setted_by, comment: "谁设置的"
      t.integer :company_id, comment: "公司ID"
      t.integer :studio_id, comment: "分店ID"
      t.integer :employee_user_id, comment: "用户ID"
      t.integer :employee_id, comment: "雇员ID"

      t.timestamps
    end
  end
end
