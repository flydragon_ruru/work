class CreateStudentCards < ActiveRecord::Migration[5.0]
  def change
    #学生说持有卡种
    create_table :student_cards do |t|
      t.integer   :student_id,                                    comment: "学生ID"
      t.string    :card_type,                                     comment: "卡的种类"
      t.decimal   :amount, precision: 8, scale: 2, default: 0.00, comment: "金额或次数"
      t.datetime  :invalidate_at,                                 comment: "到期时间"
      t.integer   :course_info_id,                                comment: "直接售卖会关联课程"

      t.timestamps
    end
  end
end
