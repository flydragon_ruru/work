class RenamePositionNameToSalaryMonthStatistics < ActiveRecord::Migration[5.0]
  def change
  	rename_column(:salary_month_statistics, :position_name, :position_names)
  end
end
