class CreateTeacherLevelConfiguration < ActiveRecord::Migration[5.0]
  def change
  	#导师设置
    create_table :teacher_level_configurations do |t|
      t.integer :company_id,           comment: '公司ID'
      t.string  :teacher_level,        comment: '导师等级'

      t.timestamps
    end
  end
end
