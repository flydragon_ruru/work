class CreateEmployeeOfStudios < ActiveRecord::Migration[5.0]
  def change
    #职位表
    create_table :employee_of_studios do |t|
      t.integer :employee_id, comment: "员工ID"
      t.integer :studio_id, comment: "分店ID"
      t.integer :employee_user_id, comment: "用户ID"
      t.integer :studio_company_id, comment: "公司ID"
      t.integer :position_id, comment: "职位信息ID"
      t.string  :position_name, comment: "职位名"
      t.integer :department_id, comment: "部门ID"

      t.timestamps
    end
  end
end
