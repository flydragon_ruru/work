class CreateCourses < ActiveRecord::Migration[5.0]
  def change
  	#具体课程时间表
    create_table :courses do |t|
      t.datetime    :course_time,           comment: "上课时间"
      t.string  :day_of_week,           comment: "星期几"
      t.integer :course_info_id,        comment: "哪个课程的基本配置"
      t.integer :teacher_id,            comment: "哪个老师的课"

      t.timestamps
    end
  end
end
