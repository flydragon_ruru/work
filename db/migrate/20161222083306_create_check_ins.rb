class CreateCheckIns < ActiveRecord::Migration[5.0]
  def change
    #学生或导师签到表
    create_table :check_ins do |t|
      t.integer   :course_id,     comment: "课程id"
      t.integer   :student_id,    comment: "学生id"
      t.integer   :teacher_id,    comment: "导师id"
      t.integer   :operated_by,   comment: "操作员工id，employee_id"
      t.datetime  :checked_at,    comment: "签到时间"
      t.string    :pay_way,       comment: "支付方式"

      t.timestamps
    end
    add_index :check_ins, :course_id;
    add_index :check_ins, :student_id;
    add_index :check_ins, :teacher_id;
    add_index :check_ins, :operated_by;
  end
end
