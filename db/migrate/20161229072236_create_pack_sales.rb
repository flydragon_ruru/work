class CreatePackSales < ActiveRecord::Migration[5.0]
  def change
  	#打包售卖剩余次数
    create_table :pack_sales do |t|
      t.integer :course_info_id, comment: "打包的课程ID"
      t.integer :amount, default: 0, comment: "剩余次数"
      t.integer :student_id, comment: "学生ID"

      t.timestamps
    end
  end
end
