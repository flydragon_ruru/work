class CreateStudentGroupConfiguration < ActiveRecord::Migration[5.0]
  def change
  	#学员分组信息
    create_table :student_group_configurations do |t|
      t.integer :company_id,           comment: '公司ID'
      t.string  :group_name,           comment: '学员分组名'

      t.timestamps
    end
  end
end
