class CreateSalaries < ActiveRecord::Migration[5.0]
  def change
    #工资核算方式
    create_table :salaries do |t|
      t.integer      :employee_id,         comment: '员工ID'
      t.integer      :employee_user_id,    comment: '用户ID'
      t.string       :salary_type,         comment: '工资类型'
      t.integer      :amount,              comment: '金额'
      t.integer      :range_min,           comment: '阶段范围最小值'
      t.integer      :range_max,           comment: '阶段范围最大值'

      t.timestamps
    end
  end
end
