class Updatefinancetable < ActiveRecord::Migration[5.0]
  def change
    drop_table :finances
    #财务表
    create_table(:finances, options: 'ENGINE=MyISAM DEFAULT CHARSET=utf8') do |t|
      t.integer   :studio_id,                                      comment: '工作室ID'
      t.integer   :company_id,                                     comment: '公司ID'
      t.string    :finance_type,      limit: 50,  default: '',     comment: '账务种类'
      t.string    :finance_category,  limit: 50,  default: '',     comment: '账务类型'
      t.decimal   :amount,         precision: 8,    scale: 2,      comment:'账务金额'
      t.text      :remark,                                         comment: '备注'
      t.boolean   :initial_state,                 default: false,  comment: '是否初始值'
      t.boolean   :is_system,                 default: false,      comment: '是否是系统录入'
      t.date      :setdate,                                        comment: "设置时间"
      t.boolean   :disabled,                  default: false,      comment: '状态, 是否被禁用'
      t.datetime  :deleted_at,                                     comment: 'paranoia软删除'
      t.timestamps
    end

    add_index :finances, :studio_id
    add_index :finances, :company_id
    add_index :finances, :finance_type
    add_index :finances, :finance_category
    add_index :finances, :disabled
    add_index :finances, :deleted_at
    add_index :finances, :created_at
    add_index :finances, :initial_state
  end
end
