class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    #学生订单表w
    create_table :orders do |t|
      t.string             :order_type,                  comment: '订单类型'
      t.integer            :potential_student_id,        comment: '意向学员ID'
      t.integer            :student_id,                  comment: '正式学员ID'
      t.integer            :amount,                      comment: '数量'
      t.decimal            :real_price,                  comment: '单价'
      t.decimal            :total_price,                 comment: '总价'
      t.integer            :good_id,                     comment: '商品ID'
      t.integer            :studio_id,                   comment: '分店ID'
      t.integer            :employee_id,                 comment: '操作员工ID'

      t.timestamps
    end
    add_index :orders, :good_id;
    add_index :orders, :student_id;
    add_index :orders, :potential_student_id;
    add_index :orders, :studio_id;
    add_index :orders, :employee_id;
  end
end
