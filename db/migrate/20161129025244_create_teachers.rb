class CreateTeachers < ActiveRecord::Migration[5.0]
  def change
    #导师表
    create_table :teachers do |t|
      t.integer     :employee_of_studio_id,       comment: '员工职位中间表ID'
      t.string      :name,                        comment: "导师名"
      t.string      :age_group,                   comment: '授课对象'
      t.string      :teacher_level,               comment: '导师等级'
      t.string      :dance_type,                  comment: '舞种'
      t.integer     :studio_id,  comment: "分店ID"
      t.integer     :company_id, comment: "公司ID"

      t.timestamps
    end
  end
end
