class AddPriceToStudnetCard < ActiveRecord::Migration[5.0]
  def change
    add_column :student_cards, :price, :decimal, precision: 8, scale: 2,default: 0.00, comment: "一次次卡的价格或一次打包课的价格"
  end
end
