class CreatePositionConfiguration < ActiveRecord::Migration[5.0]
  def change
  	#职位设置
    create_table :position_configurations do |t|
      t.integer :company_id,            comment: '公司ID'
      t.string  :position_name,         comment: '职位名'

      t.timestamps
    end
  end
end
