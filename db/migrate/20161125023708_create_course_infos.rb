class CreateCourseInfos < ActiveRecord::Migration[5.0]
  def change
    #课程的基本信息表
    create_table :course_infos do |t|
      t.string   :sale_type,            default: "单次售卖",    comment: "售卖类型"
      t.string   :course_type,          default: "常规课",      comment: "课程类型"
      t.string   :name,                 default: "",            comment: "课程名字"
      t.integer  :company_id,                                   comment: "公司ID"
      t.integer  :teacher_id,                                   comment: "导师"
      t.integer  :room_id,                                      comment: "教室编号"
      t.string   :dance_type,           default: "",            comment: "舞种"
      t.string   :age_group,            default: "成年",        comment: "授课对象(年龄分组)"
      t.integer  :max_contain,                                  comment: "最大人数"
      t.decimal  :duration,             default: 0,             comment: "课程时长"
      t.boolean  :appointmented_or_not, default: false,         comment: "是否需要预约"
      t.date     :course_begin_at,                              comment: "开始时长"
      t.date     :course_end_at,                                comment: "结束时长"
      t.boolean  :sale_or_not,          default: false,         comment: "是否销售"
      t.text     :pay_ways,                                  comment: "消费方式 json数据串"
      t.string   :status,               default: "正常",        comment: "是否结课"
      t.text     :course_set,                                   comment: "课程时间设置的json串"
      t.integer  :period,               default: "",            comment: "课时周期"

      t.timestamps
    end
  end
end
