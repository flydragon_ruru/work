# 财务管理
class CreateFinances < ActiveRecord::Migration[5.0]
  def change
    create_table(:finances, options: 'ENGINE=MyISAM DEFAULT CHARSET=utf8') do |t|
      t.integer   :studio_id,                           default: 0,          comment: '工作室ID'
      t.string    :finance_type,      limit: 50,        default: '',         comment: '账务种类'
      t.string    :finance_property,  limit: 50,        default: '',         comment: '账务性质'
      t.string    :finance_category,  limit: 50,        default: '',         comment: '账务类型'
      t.string    :finance_detail,    limit: 50,        default: '',         comment: '账务细分'
      t.integer   :amount,                              default: 0,          comment: '金额'
      t.text    :remarks,   comment: '备注'
      t.string  :initial_state, default: false, comment: '是否初始值'
      t.boolean   :disabled,                            default: false,      comment: '状态, 是否被禁用'
      t.datetime  :deleted_at,                                               comment: 'paranoia软删除'
      t.timestamps
    end

    add_index :finances, :studio_id
    add_index :finances, :finance_type
    add_index :finances, :finance_property
    add_index :finances, :finance_category
    add_index :finances, :finance_detail
    add_index :finances, :disabled
    add_index :finances, :deleted_at
    add_index :finances, :created_at
    add_index :finances, :initial_state
  end
end
