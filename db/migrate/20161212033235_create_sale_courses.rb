class CreateSaleCourses < ActiveRecord::Migration[5.0]
  def change
    #这个貌似是商品的分类
    create_table :sale_courses do |t|
      t.string     :name,                                    comment:'售卖课程名称'
      t.string     :sale_type,                               comment:'售卖课程类型'
      t.integer    :course_info_id,                          comment:'课程信息ID'
      t.integer    :company_id,                              comment:'公司ID'
      t.datetime   :deleted_at,                              comment:'删除时间'

      t.timestamps
    end
  end
end
