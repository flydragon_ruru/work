class CreatePotentialStudentGroupConfiguration < ActiveRecord::Migration[5.0]
  def change
  	#意向学员信息 已弃用
    create_table :potential_student_group_configurations do |t|
      t.integer :company_id,           comment: '公司ID'
      t.string  :group_name,        comment: '意向学员分组名'

      t.timestamps
    end
  end
end
