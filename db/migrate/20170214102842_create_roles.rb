class CreateRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :roles do |t|
      t.string :code, comment: "角色编码，用于逻辑判断"
      t.string :name, comment: "角色名称，用于展示"
      t.string :describe, comment: "角色描述，用于展示"

      t.timestamps
    end
  end
end
