class CreateSmsLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :sms_logs do |t|
      t.integer   :user_id, 		  default: '0',                    comment: '用户ID'
      t.string    :mobile,        default: '',    limit: 11,       comment: '手机号'
      t.string    :sms_type, 		  default: '',    limit: 20,       comment: '短信类型'
      t.string    :content,                                        comment: '短信内容'
      t.string    :content_hash,                                   comment: '短信hash'
      t.datetime  :expired_at,                                     comment: '过期时间'
      t.string    :status,      default: '',                       comment: 'waiting, sent, received, failed'

      t.timestamps
    end
    add_index :sms_logs, :user_id
    add_index :sms_logs, [:sms_type, :mobile, :expired_at]
  end
end
