class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string    :name,              limit: 15,                  comment: '姓名'
      t.string    :mobile,            limit: 15,                  comment: '手机号'
      t.string    :email,             limit: 50,                  comment: '邮箱'
      t.string	   :password_digest, comment: '密码'
      t.boolean   :disabled,        default: false,               comment: '状态, 是否被禁用'
      t.datetime  :deleted_at,                                    comment: 'paranoia软删除'
      t.integer :last_company_id,  comment: '最后一次登录的工作室或分店'
      t.integer :last_studio_id,  comment: '最后一次登录的工作室或分店'
      t.timestamps
    end
  end
end
