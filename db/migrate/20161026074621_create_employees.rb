class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table(:employees, options: 'ENGINE=MyISAM DEFAULT CHARSET=utf8') do |t|
      t.integer   :user_id,                             default: 0,          comment: '用户ID'
      t.integer   :company_id,                                               comment: '公司ID'
      t.string    :name,              limit: 15,        default: '',         comment: '姓名'
      t.string    :alias_name,        limit: 15,        default: '',         comment: '别名'
      t.string    :avatar,            limit: 100,       default: '',         comment: '头像'
      t.string    :sex,               limit: 15,        default: '',         comment: '性别'
      t.string    :mobile,            limit: 11,        default: '',         comment: '手机号码'
      t.date      :birthday,                                                 comment: '出生日期'
      t.string    :address,           limit: 50,        default: '',         comment: '联系地址'
      t.string    :weixin,            limit: 15,        default: '',         comment: '微信号码'
      t.string    :emergency_contact,     limit: 15,    default: '',         comment: '紧急联系人'
      t.string    :emergency_mobile,      limit: 15,    default: '',         comment: '联系人电话'
      t.text      :remark,                                                   comment: '备注信息'
      t.string    :status,            limit: 15,        default: '',         comment: '状态'
      t.boolean   :disabled,                            default: false,      comment: '状态, 是否被禁用'
      t.datetime  :joined_at,                                                comment: '入职时间'
      t.datetime  :dismissed_at,                                             comment: '离职时间'
      t.datetime  :deleted_at,                                               comment: 'paranoia软删除'
      t.timestamps
    end

    add_index :employees, :user_id
    add_index :employees, :company_id
    add_index :employees, :name
    add_index :employees, :alias_name
    add_index :employees, :sex
    add_index :employees, :mobile
    add_index :employees, :birthday
    add_index :employees, :status
    add_index :employees, :disabled
    add_index :employees, :deleted_at
  end
end
