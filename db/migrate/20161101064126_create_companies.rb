class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string     :name,           comment: '公司名称'
      t.integer    :owner_id,       comment: '主理人'
      t.timestamps
    end

    add_index :companies, :name
    add_index :companies, :owner_id
  end
end
