# 工作室
class CreateStudios < ActiveRecord::Migration[5.0]
  def change
    create_table(:studios, options: 'ENGINE=MyISAM DEFAULT CHARSET=utf8') do |t|
      t.string    :name,              limit: 15,                              comment: '名称'
      t.string    :uuid,              limit: 32,                              comment: 'uuid'
      t.string    :avatar,            limit: 100,                             comment: '头像'
      t.boolean   :is_chain,                            default: false,       comment: '是否连锁工作室'
      t.datetime  :invalidate_at,   comment: "到期时间"
      t.datetime  :last_buy_at,     comment: "上次购买时间"
      t.integer   :company_id, comment: "公司ID"
      t.boolean   :disabled,                            default: false,       comment: '状态, 是否被禁用'
      t.datetime  :deleted_at,                                                comment: 'paranoia软删除'
      t.timestamps
    end

    add_index :studios, :name
    add_index :studios, :uuid
    add_index :studios, :disabled
    add_index :studios, :deleted_at
    add_index :studios, :invalidate_at
    add_index :studios, :company_id
  end
end
