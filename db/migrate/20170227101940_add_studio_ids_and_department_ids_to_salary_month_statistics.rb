class AddStudioIdsAndDepartmentIdsToSalaryMonthStatistics < ActiveRecord::Migration[5.0]
  def change
  	add_column :salary_month_statistics, :studio_ids, :string, comment: "当前员工所拥有门店的id,eg:(1),(2)"
    add_column :salary_month_statistics, :department_ids, :string, comment: "当前员工所拥有部门的名称,eg:运营部,销售部"
  end
end
