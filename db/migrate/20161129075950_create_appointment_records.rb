class CreateAppointmentRecords < ActiveRecord::Migration[5.0]
  def change
    #预约表
    create_table :appointment_records do |t|
      t.integer    :course_id,           comment: "课程ID"
      t.integer    :student_id,          comment: "学生ID"
      t.integer    :course_info_id,      comment: "课程配置ID"
      t.datetime   :appointment_at,      comment: "预约时间"
      t.integer    :operated_by,         comment: "操作人员"
      t.integer    :studio_id,           comment: "分店ID"
      t.integer    :company_id,          comment: "公司ID"

      t.timestamps
    end
  end
end
