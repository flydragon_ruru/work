class CreateDepartmentConfiguration < ActiveRecord::Migration[5.0]
  def change
  	#部门设置
    create_table :department_configurations do |t|
      t.integer :company_id,           comment: '公司ID'
      t.string  :department_name,      comment: '部门名称'

      t.timestamps
    end
  end
end
