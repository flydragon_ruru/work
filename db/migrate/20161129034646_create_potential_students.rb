class CreatePotentialStudents < ActiveRecord::Migration[5.0]
  def change
    #意向学员表
    create_table :potential_students do |t|
      t.string :name,               default: "",        comment: "意向学员名字"
      t.string :alias_name,         default: "",        comment: "英文别名"
      t.string :sex,                default: "女",       comment: "性别" 
      t.string :mobile,             default: "",        comment: "手机号码"
      t.string :weixin,             default: "",        comment: "微信号"
      t.integer :student_group,                         comment: "学员分组"
      t.date :birthday, comment: "出生年月"
      t.string :address,            default: "",        comment: "联系地址"
      t.string :source_from,        default: "",        comment: "信息来源"
      t.string :emergency_contact,  default: "",        comment: "紧急联系人"
      t.string :emergency_mobile,   default: "",        comment: "紧急联系电话"
      t.text :learning_requirement,                     comment: "学习需求"
      t.text :state_of_business,                        comment: "经济情况"
      t.text :learning_intention,                       comment: "学习意向"
      t.string :willing_teacher,    default: "",        comment: "意向导师"
      t.integer :willing_studio,                        comment: "意向分店"
      t.string :avatar,             default: "",        comment: "头像"
      t.integer :studio_id,                             comment: "工作室id"
      t.integer :company_id,                            comment: "公司ID"
      t.integer :market_employee_id,                    comment: "市场专员ID"
      t.string :status, default: "未转化",               comment: "转换状态"
      t.date :switching_time,                           comment: "转换时间"

      t.timestamps
    end
  end
end
