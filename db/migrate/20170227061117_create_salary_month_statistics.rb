class CreateSalaryMonthStatistics < ActiveRecord::Migration[5.0]
  def change
    create_table :salary_month_statistics do |t|
      t.integer :company_id,                                          comment:'公司ID'
      t.string :month, limit: 20,comment: '月份'
      t.integer :employee_id, comment: '员工id'
      t.string :employee_name, limit: 50,comment: '员工姓名'
      t.string :position_name,comment: '职位名称,多个使用逗点分割'
      t.float :base_salary,comment: '基本工资'
      t.float :commission_salary,comment: '提成工资'
      t.float :sum_salary,comment: '总工资'

      t.timestamps
    end
  end
end
