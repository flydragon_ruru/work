class CreatePayingWays < ActiveRecord::Migration[5.0]
  def change
  	#课程支付方式
    create_table :paying_ways do |t|
      t.string :pay_type, 					comment: "本课程可付款方式"
      t.integer :amount,  					comment: "本课程需要支付的价格"
      t.integer :course_info_id, 			comment: "那个课的支付方式"
      t.integer :course_info_teacher_id, 	comment: "这个字段好像可以删掉"	

      t.timestamps
    end
  end
end
