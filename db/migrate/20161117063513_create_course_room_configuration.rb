class CreateCourseRoomConfiguration < ActiveRecord::Migration[5.0]
  def change
    #教室设置
    create_table :course_room_configurations do |t|
      t.integer :company_id,           comment: '公司ID'
      t.integer :studio_id,            comment: '分店ID'
      t.string  :room_name,            comment: '教室名'
      t.string  :location,             comment: '教室位置'
      t.integer :max_contain,          comment: '人数上限'

      t.timestamps
    end
  end
end
