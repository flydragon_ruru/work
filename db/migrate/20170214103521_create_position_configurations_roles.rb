class CreatePositionConfigurationsRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :position_configurations_roles, id: false  do |t|
      t.belongs_to :position_configuration, comment: "职务外健"
      t.belongs_to :role, comment: "角色外健"
    end
  end
end
