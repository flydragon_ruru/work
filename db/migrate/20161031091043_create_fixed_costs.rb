class CreateFixedCosts < ActiveRecord::Migration[5.0]
  def change
    create_table :fixed_costs do |t|
      t.string :name, default: '',  comment: '成本名称'
      t.decimal :amount, default: 0.00, comment: '成本费用'
      t.integer :cycle, default: '', comment: '周期'
      t.boolean   :disabled, default: false,  comment: '状态, 是否被禁用'
      t.datetime  :deleted_at,  comment: 'paranoia软删除'

      t.timestamps
    end

    create_table :fixed_costs_studios, id: false do |t|
      t.belongs_to :studio
      t.belongs_to :fixed_cost
    end
  end
end
