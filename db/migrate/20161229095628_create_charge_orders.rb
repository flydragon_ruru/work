class CreateChargeOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :charge_orders do |t|

      t.string :user_id ,comment: '用户唯一标示ID'
      t.string :number ,comment: '订单号'
      t.text :title ,comment: '订单信息'

      t.timestamps
    end
  end
end
