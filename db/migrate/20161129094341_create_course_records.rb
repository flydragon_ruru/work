class CreateCourseRecords < ActiveRecord::Migration[5.0]
  def change
    #上课记录？ 好像弃用了
    create_table :course_records do |t|
      t.integer   :course_id,       comment: "课程ID"
      t.integer   :student_id,      comment: "学生ID"
      t.string    :paying_way,      comment: "支付方式"
      t.integer   :operator_id,     comment: "操作人员"
      t.string    :record_type,     comment: "暂时忘了"
      t.integer   :studio_id,       comment: "分店ID"
      t.integer   :company_id,      comment: "公司ID"

      t.timestamps
    end
  end
end
