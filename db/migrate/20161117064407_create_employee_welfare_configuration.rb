class CreateEmployeeWelfareConfiguration < ActiveRecord::Migration[5.0]
  def change
  	#公司福利
    create_table :employee_welfare_configurations do |t|
      t.integer :company_id,                     comment: '公司ID'
      t.string  :welfare_name,                   comment: '福利名'
      t.decimal :amount,        default: 0.00,   comment: '福利金额'

      t.timestamps
    end
  end
end
