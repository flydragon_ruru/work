class CreateGoods < ActiveRecord::Migration[5.0]
  def change
    #商品表
    create_table :goods do |t|
      t.integer      :company_id,                                          comment:'公司ID'
      t.string       :good_type,                                           comment:'商品分类'
      t.integer      :good_info_id,                                        comment:'商品类型'
      t.string       :name,                                                comment:'商品名称'
      t.integer      :amount,                                              comment:'商品规格 - 课数'
      t.decimal      :price,               precision: 8,    scale: 2,      comment:'商品价格'
      t.integer      :stock,                                               comment:'库存'
      t.integer      :month_of_validity,                                   comment:'商品有效期'
      t.text         :remark,                                              comment:'简介'
      t.string       :avatar,                                              comment:'商品图片'
      t.string       :status,              default: '正常',                comment:'商品状态'
      t.integer      :sale_amount,         default: 0,                     comment:'商品销量'

      t.timestamps
    end
  end
end
