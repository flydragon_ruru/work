class AddStudentidToCourseReckon < ActiveRecord::Migration[5.0]
  def change
    add_column :course_reckons, :student_id, :integer, comment: "学生ID"
    add_column :course_reckons, :pay_way, :string, comment: "本次课消费方式"
    add_column :course_reckons, :course_id, :integer, comment: "具体什么时候的课"
    change_column :course_reckons, :amount, :decimal, precision: 8, scale: 2,  default: 0.00, comment: "这次课消费的金额"
  end
end
