class CreatePrivateEducationConfiguration < ActiveRecord::Migration[5.0]
  def change
  	#私教设置
    create_table :private_education_configurations do |t|
      t.integer :company_id,            comment: '公司ID'
      t.integer :duration,              comment: '私教时长'

      t.timestamps
    end
  end
end
