$(".header-con li").hover(function() {
	$(this).children("a").css({
		"color" : "#fff"
	});
}, function() {
	if (!$(this).children("a").hasClass("open-menu")) {
		$(this).children("a").css({
			"color" : "#85879e"
		});
	}
});

$(".peo-message").on("click", function() {
 if ($(this).hasClass("op-peomessage")) {
  $(this).removeClass("op-peomessage");
 } else {
  $(this).addClass("op-peomessage");
 }
});
/* 下拉列表 */
$(".downbtn").on("click", function() {
	if ($(this).next("ul").hasClass("menuopen")) {
		$(this).next("ul").removeClass("menuopen");
	} else {
		$(this).next("ul").addClass("menuopen");
		$(this).parent().hover(function() {

		}, function() {
			$(this).children("ul").removeClass("menuopen");
		});
		$(".menuopen li").on("click", function() {
			$(this).parent().removeClass("menuopen");
			$(this).parent().parent().children("div").children("span").text($(this).text());
			if ($(this).parent().parent().attr("Choice") == "mandatory" || $(this).parent().parent().attr("Choice") == "selected") {
				if ($(this).index() != 0) {
					if ($(this).parent().parent().hasClass("spe")) {
						$(this).parent().parent().children("div").children("span").attr("data_id", $(this).attr("data_id"));
					};
					$(this).parent().parent().attr("Choice", "selected");
					$(this).parent().parent().next("b").css({
						"display" : "none"
					})
				} else {
					$(this).parent().parent().attr("Choice", "mandatory");
				}
			};
		})
	}
});
/* 复选框 */
$(".check").on("click", function() {
	if($(this).attr("hps-readonly")=="1") return;
	if ($(this).prev("input").prop("checked")) {
		$(this).prev("input").prop("checked", false);
		$(this).removeClass("ad-checked");
		$(this).children("span").css({
			"display" : "none"
		})
	} else {
		$(this).addClass("ad-checked");
		$(this).prev("input").prop("checked", true);
		$(this).children("span").css({
			"display" : "block"
		})
	}
})
/* 单选按钮选中事件 */
$(".rad-sl .ra-btn").on("click", function() {
	radioChange($(this));
	// $(this).parent().children("input").eq($(this).index()).prop("checked", true);
	$(this).parent().parent().find(".comm-con").removeClass("common-show");
	$(this).parent().find(".comm-con").addClass("common-show");
});
 // 添加员工  薪资信息模块
 /* 单选按钮选中事件 */
 function radioChange(sel, can) {
 	$(sel).parent().parent().find(".ra-btn").children("span").css({
		"display" : "none"
	});
	$(sel).prev("input").prop("checked", true);
	$(sel).children("span").css({
		"display" : "block"
	});
 }

 // 月份日历
function dateT(position) {
    var t = new Date();
    var nowYear = t.getFullYear();
    var nowMonth = t.getMonth() + 1;
    $(".t-month span").eq(nowMonth - 1).css({
        "color" : "#4584ff"
    });
    $(".t-year strong").text(nowYear + "年");
        director();
    $(".mdate-btn strong").text(nowYear);
    $(".mdate-btn em").text(nowMonth);
    $(".mar-date").hover(function() {
    },function() {
        $(".mdate-btn").attr("dateCon", "close");
        $(".time-date").css({
            "display" : "none"
        });
        $(this).children("div").eq(0).removeClass("bchose");
    });
    $(".year-r").on("click", function() {
        if (nowYear == 2060) {
            nowYear = 2060;
        } else {
            nowYear++;
        }
        $(".t-year strong").text(nowYear + "年");
        $(".mdate-btn strong").text(nowYear);
        director();
    });
    $(".year-l").on("click", function() {
        if (nowYear == 1970) {
            nowYear = 1970;
        } else {
            nowYear--;
        }
        $(".t-year strong").text(nowYear + "年");
        $(".mdate-btn strong").text(nowYear);
        director();
    });
    $(".t-month span").on("click", function() {
        $(this).css({
            "color" : "#4584ff"
        });
        $(this).siblings().css({
            "color" : "#a8a8a8"
        });
        nowMonth = parseInt($(this).text());
        $(".mdate-btn em").text(nowMonth);
        $(".mdate-btn").attr("dateCon", "close");
        $(".time-date").css({
            "display" : "none"
        });
        $(".t-year strong").text(nowYear + "年");
        $(this).removeClass("bchose");
        director();
    })
        function director() {
          $.ajax({
            type:'get', url:"/admin/targets/" + position +"?year=" + nowYear + "&month=" + nowMonth,
            success: function(data) {
              if (data.error) {
              } 
            }
          });
        }
}
$(".mdate-btn").on("click", function() {
	if ($(this).attr("dateCon") == "close") {
		$(this).attr("dateCon", "open");
		$(this).next("div").css({
			"display" : "block"
		});
		$(this).addClass("bchose");
	} else {
		$(this).attr("dateCon", "close");
		$(this).next("div").css({
			"display" : "none"
		});
		$(this).removeClass("bchose");
	}
})
// 弹窗
//$(".mar-operation span").on("click", function() {
//	$(".mask").css({
//		"display" : "block"
//	});
//	$(".mask-box").css({
//		"margin-top" : -($(".mask-box").height() + parseInt($(".mask-box").css("padding-bottom")))/2
//	})
//})
//$(".mask-head span").on("click", function() {
//	$(this).parent().parent().find("input").val("");
//	$(".mask").css({
//		"display" : "none"
//	})
//})
//$(".mask-confirm").on("click", function() {
//	$(this).parent().parent().find("input").val("");
//	$(".mask").css({
//		"display" : "none"
//	})
//})
//// 弹窗
//$(".as-operation span").on("click", function() {
//	$(".mask").css({
//		"display" : "block"
//	});
//	$(".mask-box").css({
//		"margin-top" : -($(".mask-box").height() + parseInt($(".mask-box").css("padding-bottom")))/2
//	})
//})
// 手机验证
function verification(type, num) {
	var r = {
		user: /^1[34578]\d{9}$/,
		password: /^.{6,20}$/
	};
	if (type == "mobile") {
		if (!r.user.test(num)) {
			return false;
		} else {
			return true;
		}
	};
	if (type == "pwd") {
		if (!r.password.test(num)) {
			return false;
		} else {
			return true;
		}
	};
}
// 必填的输入框  未输入样式
$(".pay-btn").on("click", function() {

	if (confirmInput()) {
		console.log("可以跳转到下一步");
		var postJson = {
		   "user" : [{
		    "mobile" : "" + $('.reg-ac .in-l').val() + "",
		    "password" : "" + $('.reg-ac .in-r').val() + "",
		    "password_confirmation" : "" + $('.reg-ac .in-r').val() + ""
		   }],
		   "employee" : [{
		    "name" : "" + $('.bs-box').find('input').eq(0).val() + "",
		    "alias_name" : "" + $('.bs-box').find('input').eq(1).val() + "",
		    "sex" : "" + $('.b-sex').find('span').text() + "",
		    "birthday" : "" + $('.bs-box .date-input').val() + "",
		    "weixin" : "" + $('.bs-box').find('input').eq(2).val() + "",
		    "mobile" : "" + $('.bs-box').find('input').eq(3).val() + "",
		    "emergency_contact" : "" + $('.bs-box').find('input').eq(5).val() + "",
		    "emergency_mobile" : "" + $('.bs-box').find('input').eq(6).val() + "",
		    "address" : "" + $('.bs-box').find('input').eq(7).val() + "",
		    "remark" : "" + $('.bs-box').find('input').eq(8).val() + "",
		    "avatar" : "" + $('#employee_avatar').val() + ""
		  }],
		  "employee_of_studio" : [

		  ],
		  "salary" : [

		  ],
		'utf-8': $("input[name=utf-8]").val(),
		'authenticity_token': $("input[name=authenticity_token]").val()
		};
		if ($(".pay-info").find(".welfare").hasClass("we-show")) {
			var wel = {};
			var welArr = [];
			wel.type = "福利补助";
			postJson.salary.push(wel);
			for (var i = 0; i < $(".welfare .sub-n").length; i++) {
				if ($($(".welfare .sub-n")[i]).find(".ck").attr("checked")) {
					welArr.push($($(".welfare .sub-n")[i]).attr("data_id"));
				};
			};
			wel.employee_welfare_ids = welArr;
		};
		var sal = ["department_id", "position_id", "age_group", "teacher_level"];
		var emptit = [];
		// 改成了 传id
		for (var i = 0; i < $(".pos-dwrite").length; i++) {
			var emp = {};
			for (var j = 0; j < $(".pos-dwrite").eq(i).find("em").length; j++) {
				// $($(".pos-dwrite").find("em")).eq(j).text();
				if (sal[j] == "position_id" || sal[j] == "department_id" || sal[j] == "teacher_level") {
					emp[sal[j]] = $($(".pos-dwrite").eq(i).find("em")).eq(j).attr("data_id");
				} else {
					emp[sal[j]] = $($(".pos-dwrite").eq(i).find("em")).eq(j).text();
				}
			};
			var emArr = [];
			for (var t = 0; t < $(".pos-dwrite").eq(i).find("strong").length; t++) {
				emArr.push($($(".pos-dwrite").eq(i).find("strong")).eq(t).attr("data_id"));
			};
			if (emArr.length > 0) {
				for (var k = 0; k < emArr.length; k++) {
					var newEm = Object.assign({}, emp, {studio_id: emArr[k]});
					postJson.employee_of_studio.push(newEm);
				};
			}
		};
		for (var i = 0; i < $(".pos-show").length; i++) {
			var pos = {};
			if ($($(".pos-show")[i]).children("h4").text() == "导师") {
				pos.type = "导师";
				pos.课时费 = $($(".pos-show")[i]).find(".m-sl input").val();
				pos.私教提成 = $($(".pos-show")[i]).find("#personal").val();
				if ($($(".pos-show")[i]).find(".rad-box .common-show").prev().text() == "固定提成") {
					pos.固定提成 = $($(".pos-show")[i]).find(".rad-box .common-show input").val();
				}
				if ($($(".pos-show")[i]).find(".rad-box .common-show").prev().text() == "阶段提成" && $($(".pos-show")[i]).find(".rad-box .comm-write").length > 0) {
					pos.阶段提成 = [];
					for (var j = 0; j < $($(".pos-show")[i]).find(".rad-box .comm-write").length; j++) {
						var stage = {};
						stage.amount = $($(".pos-show")[i]).find(".rad-box .comm-write").eq(j).find("b").eq(2).text();
						stage.range_min = $($(".pos-show")[i]).find(".rad-box .comm-write").eq(j).find("b").eq(0).text();
						stage.range_max = $($(".pos-show")[i]).find(".rad-box .comm-write").eq(j).find("b").eq(1).text();
						pos.阶段提成.push(stage);
					};
				}
				var excText = $($(".pos-show")[i]).find(".p-exc i").text();
				pos[excText.substring(0, excText.length - 1)] = [];
				for (var j = 0; j < $($(".pos-show")[i]).find(".p-exc .comm-write").length; j++) {
					var stage = {};
					stage.amount = $($(".pos-show")[i]).find(".p-exc .comm-write").eq(j).find("b").eq(2).text();
					stage.range_min = $($(".pos-show")[i]).find(".p-exc .comm-write").eq(j).find("b").eq(0).text();
					stage.range_max = $($(".pos-show")[i]).find(".p-exc .comm-write").eq(j).find("b").eq(1).text();
					pos[excText.substring(0, excText.length - 1)].push(stage);
				};
			} else {
				pos.type = $($(".pos-show")[i]).children("h4").text();
				var excText = $($(".pos-show")[i]).find(".p-exc i").text();
				pos[excText.substring(0, excText.length - 1)] = [];
				for (var j = 0; j < $($(".pos-show")[i]).find(".p-exc .comm-write").length; j++) {
					var stage = {};
					stage.amount = $($(".pos-show")[i]).find(".p-exc .comm-write").eq(j).find("b").eq(2).text();
					stage.range_min = $($(".pos-show")[i]).find(".p-exc .comm-write").eq(j).find("b").eq(0).text();
					stage.range_max = $($(".pos-show")[i]).find(".p-exc .comm-write").eq(j).find("b").eq(1).text();
					pos[excText.substring(0, excText.length - 1)].push(stage);
				};
			}
			postJson.salary.push(pos);
		};
		console.log(postJson);


    var dst = [];
    postJson.salary.forEach(x => {
      if (x.employee_welfare_ids) {
        x.employee_welfare_ids.forEach(y => {
          dst.push({
            salary_type: x.type,
            employee_welfare_id: y,
          });
        });
      } else if (Object.keys(x).length === 2) {
        var keys = Object.keys(x)
        keys = keys.filter(x => x != 'type');
        x[keys[0]] && x[keys[0]].forEach(y => {
          dst.push(Object.assign({
            salary_type: x.type + '_' + keys[0],
          }, y));
        });
      } else {
        var keys = Object.keys(x)
        keys = keys.filter(x => x != 'type');
        keys.forEach(y => {
          if (typeof x[y] === 'object') {
            x[y].forEach(z => {
              dst.push(Object.assign({
                salary_type: x.type + '_' + y,
              }, z));
            });
          } else {
            dst.push({
              salary_type: x.type + '_' + y,
              amount: x[y],
            });
          }
        });
      }
    });
    postJson.salary = dst;

	if ($("#basePay").val() != "") {
		var bp = {};
		bp.salary_type = "基本工资";
		bp.amount = $("#basePay").val();
		postJson.salary.push(bp);
	}	
    var postCon = JSON.stringify(postJson);
    // console.log(postCon);
    // // 解析的时候
    // console.log(JSON.parse(postCon));

    $.ajax({
      data: postJson,
      type:'post', url:"/admin/employees",
      success: function(data) {
        if (data.error) {
        }
      }
    });
	}
})

function _get_current_date_int(){
    var date = new Date();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    return parseInt(date.getFullYear()+""+month+""+strDate);
}

function confirmInput() {
	var writeIn = true;
	for (var i = 0; i < $(".required").length; i++) {
		if ($($(".required")[i]).val() == "") {
			$($(".required")[i]).css({
				"border" : "1px solid #ff5a60"
			});
			writeIn = false;
		}else{
            if($($(".required")[i]).attr("id")=="birthday"){

            	var birthday = $($(".required")[i]).val();
            	if(parseInt(birthday.replace(/-0|-/g,"")) >= _get_current_date_int()){
					$($(".required")[i]).css({"border" : "1px solid #ff5a60"});
					writeIn = false;
            	}
            }

		}
	}
	console.log("检测1："+writeIn);
	for (var i = 0; i < $(".mob").length; i++) {
		if (!verification("mobile", $($(".mob")[i]).val())) {
			writeIn = false;
			$($(".mob")[i]).css({
				"border" : "1px solid #ff5a60"
			});
		} else {
			$($(".mob")[i]).css({
				"border" : "1px solid #abadc1"
			});
		}
	}
	console.log("检测2："+writeIn);
	if ($(".b-sex").attr("choice") == "mandatory") {
		writeIn = false;
		$(".b-sex").next("b").css({
			"display" : "inline"
		});
	};
	console.log("检测3："+writeIn);
	if ($(".pas").hasClass("new-pwd")) {
		if ($(".pas").val() == "" || !verification("pwd", $(".pas").val())) {
			writeIn = false;
			$(".pas").css({
				"border" : "1px solid #ff5a60"
			});
		} else {
			$(".pas").css({
				"border" : "1px solid #abadc1"
			});
		}
	};
	console.log("检测4："+writeIn);
	if ($(".st-pos .pos-dwrite").length == 0) {
		writeIn = false;
		$(".st-pos").find(".not-addpos").css({
			"display" : "block"
		});
	} else {
		$(".st-pos").find(".not-addpos").css({
			"display" : "none"
		})
	}
	console.log("检测5："+writeIn);
	return writeIn;
}
$(".required").blur(function() {
	if ($(this).val() != "") {
		$(this).css({
			"border" : "1px solid #abadc1"
		})
	};
})

// 账号信息验证
$(".reg-ac .in-l").blur(function() {
	if(verification("mobile", $(this).val())) {
		console.log("mobile格式正确");
		// 如果返回为 true 则格式正确  发送ajax 到后台
    $.ajax({
      type:'get', url:"/admin/users/user_present_or_not?mobile="+$(this).val(),
      success: function(data) {
        if (data["present"]) { //改为  ajax 返回到数据如果是 已有账号的条件
          $(".reg-ac .in-l").next().text("已有账号");
          if ($(".reg-ac .in-r").hasClass("new-pwd")) {
            $(".reg-ac .in-r").removeClass("new-pwd");
          };
        } else {
          $(".reg-ac .in-l").next().text("此账号为新账号");
          if (!$(".reg-ac .in-r").hasClass("new-pwd")) {
            $(".reg-ac .in-r").addClass("new-pwd");
          };
        }
      }
    })
	} else {
		$(this).css({
			"border" : "1px solid #ff5a60"
		});
	}
	$(".reg-ac .new-pwd").blur(function() {
		if ($(this).val() != "" && verification("pwd", $(this).val())) {
			$(this).css({
				"border" : "1px solid #abadc1"
			});
		} else {
			$(this).css({
				"border" : "1px solid #ff5a60"
			})
		}
	})
})
// 添加员工 选择导师 则出现 后面三个
$("#choicePos .dropdown-menu li").on("click", function() {

	var position_name = $(this).text();
	if (position_name == "导师") {
		$("#choicePos").nextAll(".p-t").css({
			"display" : "block"
		})
	} else {
		$("#choicePos").nextAll(".p-t").css({
			"display" : "none"
		})
	}

	// 财务或hr默认选中所有门店
    if(position_name=="财务总监" || position_name=="会计/出纳" || position_name=="人资行政总监" || position_name=="HR/行政"){
      $(".pos-branch .check").addClass("ad-checked").attr("hps-readonly","1").find("span").show();
      $(".pos-branch :checkbox").attr("checked",true);
    }

});
// 验证
function clearNoNum(obj) {
    //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d.]/g,"");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,"");
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g,".");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
}
function intNum(obj) {
	 //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d]/g,"");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,"");
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g,".");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
}
// 添加员工 职位信息添加
$("#careerInfo").on("click", function() {
	var arr = [];
	var writeIn = true;
	var liLen = 2;
	if ($("#choicePos span").text() == "导师") {
		liLen = 5;
	};
	for (var i = 0; i < liLen; i++) {
		if ($($(this).parent().find(".list-box")[i]).attr("choice") == "mandatory") {
			writeIn = false;
			$($(this).parent().find(".list-box")[i]).next("b").css({
				"display" : "inline"
			});
		};
	};
	var isCheck = false;
	for (var i = 0; i < $(this).parent().find(".ck").length; i++) {
		if ($($(this).parent().find(".ck")[i]).attr("checked")) {
			arr.push("<strong data_id=" + $($(this).parent().find(".ck")[i]).parent().parent().next("span").attr("data_id") + ">" + $($(this).parent().find(".ck")[i]).parent().parent().next("span").text() + "</strong>");
			isCheck = true;
			$(".p-und b").css({
				"display" : "none"
			})
		};
	};
	if (!isCheck) {
		$(".p-und b").css({
			"display" : "inline"
		})
	};
	if (writeIn && isCheck) {
		// console.log($(this).parent().find(".list-box").eq(1).find("span").text());
		salaryInfor("add", $(this).parent().find(".list-box").eq(1).find("span").text());
		for (var j = 0; j < $($(this).parent().find(".list-box")[1]).find("li").length; j++) {
			if ($($($(this).parent().find(".list-box")[1]).find("li")[j]).text() == $($(this).parent().find(".list-box")[1]).find("span").text()) {
				$($($(this).parent().find(".list-box")[1]).find("li")[j]).remove();
			};
		};
		if (liLen == 2) {
			$('.pos-info').last().before($("<div class='pos-dwrite'><div class='pos-dbox'><div class=pos-dcon><div class='dep'><span>部门名称：</span><em data_id=" + $(this).parent().find(".list-box").eq(0).find("span").attr("data_id") + ">" + $(this).parent().find(".list-box").eq(0).find("span").text() + "</em></div><div><span>职位名称：</span><em data_id=" + $(this).parent().find(".list-box").eq(1).find("span").attr("data_id") + ">" + $(this).parent().find(".list-box").eq(1).find("span").text() + "</em></div></div><div class='pos-dconf'><div class='dep'><span>所属分店：</span>" + arr + "</div></div></div><div class='comm-del'>删除</div></div>"));
			for (var i = 0; i < 2; i++) {
				$($(this).parent().find(".list-box")[i]).attr("choice", "mandatory");
				$($(this).parent().find(".list-box")[i]).find("span").text($($(this).parent().find(".list-box")[i]).find("li").eq(0).text());
			};
		} else {
			// $('.pos-info').last().before($("<div class='pos-dwrite'><div class='pos-dbox'><div class=pos-dcon><div class='dep'><span>部门名称：</span><em data_id=" + $(this).parent().find(".list-box").eq(0).find("span").attr("data_id") + ">" + $(this).parent().find(".list-box").eq(0).find("span").text() + "</em></div><div><span>职位名称：</span><em data_id=" + $(this).parent().find(".list-box").eq(1).find("span").attr("data_id") + ">" + $(this).parent().find(".list-box").eq(1).find("span").text() + "</em></div></div><div class='pos-dcon'><div class='dep'><span>所属分店：</span>" + arr +"</div><div><span>年龄分组：</span><em>" + $(this).parent().find(".list-box").eq(2).find("span").text() + "</em></div></div><div class='pos-dconf'><div class='dep'><span>舞种：</span><em>" + $(this).parent().find(".list-box").eq(3).find("span").text() + "</em></div><div><span>导师等级：</span><em data_id=" + $(this).parent().find(".list-box").eq(4).find("span").attr("data_id") + ">" + $(this).parent().find(".list-box").eq(4).find("span").text() + "</em></div></div></div><div class='comm-del'>删除</div></div>"));
		    $('.pos-info').last().before($("<div class='pos-dwrite'><div class='pos-dbox'><div class=pos-dcon><div class='dep'><span>部门名称：</span><em data_id=" + $(this).parent().find(".list-box").eq(0).find("span").attr("data_id") + ">" + $(this).parent().find(".list-box").eq(0).find("span").text() + "</em></div><div><span>职位名称：</span><em data_id=" + $(this).parent().find(".list-box").eq(1).find("span").attr("data_id") + ">" + $(this).parent().find(".list-box").eq(1).find("span").text() + "</em></div></div><div class='pos-dcon'><div class='dep'><span>所属分店：</span>" + arr +"</div><div><span>年龄分组：</span><em>" + $(this).parent().find(".list-box").eq(2).find("span").text() + "</em></div></div><div class='pos-dconf'><div><span>导师等级：</span><em data_id=" + $(this).parent().find(".list-box").eq(3).find("span").attr("data_id") + ">" + $(this).parent().find(".list-box").eq(3).find("span").text() + "</em></div></div></div><div class='comm-del'>删除</div></div>"));

			for (var i = 0; i < 5; i++) {
				$($(this).parent().find(".list-box")[i]).attr("choice", "mandatory");
				$($(this).parent().find(".list-box")[i]).find("span").text($($(this).parent().find(".list-box")[i]).find("li").eq(0).text());
			};
		}

		$(".st-pos").find(".not-addpos").css({
			"display" : "none"
		})
		$(this).parent().find(".ck").attr("checked", false);
		$(this).parent().find(".check").removeClass("ad-checked");
		$(this).parent().find(".check").children("span").css({
			"display" : "none"
		});

		$(".pos-branch .check").attr("hps-readonly","0");
	};
	$(".pos-dwrite .comm-del").on("click", function() {
		salaryInfor("remove", $(this).prev().find(".dep").eq(0).next().children("em").text());
		$($(this).parent().parent().parent().find(".list-box").eq(1)).children("ul").append("<li>" + $(this).prev().find("em").eq(1).text() + "</li>");
		//添加员工 选择导师 则出现 后面三个
		$("#choicePos .dropdown-menu li").unbind();
		$("#choicePos .dropdown-menu li").on("click", function() {
			if ($(this).text() == "导师") {
				$("#choicePos").nextAll(".p-t").css({
					"display" : "block"
				});
			} else {
				$("#choicePos").nextAll(".p-t").css({
					"display" : "none"
				});
			}
		});
		$(this).parent().remove();
	})


});
// 薪资信息模块的出现
function salaryInfor(operation, obj) {
	if (operation == "add") {
		switch(true) {
			case obj == "导师": $(".occ-t").addClass("pos-show");break;
			case obj == "服务顾问": $(".occ-a").addClass("pos-show");break;
			case obj == "产品总监": $(".occ-c").addClass("pos-show");break;
			case obj == "运营总监": $(".occ-y").addClass("pos-show");break;
			case obj == "市场总监": $(".occ-s").addClass("pos-show");break;
			case obj == "市场专员": $(".occ-m").addClass("pos-show");break;
		}
	} else {
		switch(true) {
			case obj == "导师": $(".occ-t").removeClass("pos-show");break;
			case obj == "服务顾问": $(".occ-a").removeClass("pos-show");break;
			case obj == "产品总监": $(".occ-c").removeClass("pos-show");break;
			case obj == "运营总监": $(".occ-y").removeClass("pos-show");break;
			case obj == "市场总监": $(".occ-s").removeClass("pos-show");break;
			case obj == "市场专员": $(".occ-m").removeClass("pos-show");break;
		}
	}
}
$(".rad-sl .comm-add, .p-exc .comm-add").on("click", function() {
	if ($(this).parent().parent().find("input").eq(0).val() != "" && $(this).parent().parent().find("input").eq(1).val() != "") {
		if (Number($(this).parent().parent().children(".stage-box").children().text()) > Number($(this).parent().parent().find("input").eq(0).val())) {
			$(this).parent().parent().find("input").eq(0).css({
				"border" : "1px solid #ff5a60"
			});
			return;
		} else {
			$(this).parent().parent().find("input").eq(0).css({
				"border" : "1px solid #9ea0b7"
			})
		};
		$(this).parent().parent().find("input").eq(1).css({
			"border" : "1px solid #9ea0b7"
		});
		$(this).parent().parent().before("<div class='comm-write'><b>" + $(this).parent().parent().children(".stage-box").children().text() + "</b><em>-</em><b>" + $(this).parent().parent().find("input").eq(0).val() + "</b><em>" + $(this).parent().parent().find("strong").eq(0).text() + "</em><div class='cx'><em>" + $(this).parent().find("em").text() + "</em><b>" + $(this).parent().find("input").val() + "</b><em>" + $(this).parent().find("strong").text() + "</em><div class='comm-del'>删除</div></div></div>");
		$(this).parent().parent().parent().find(".comm-write").last().prevAll().find(".comm-del").css({
			"display" : "none"
		})
		$(this).parent().parent().find("span").text(Number($(this).parent().parent().find("input").eq(0).val()) + 1);
		$(this).parent().parent().find("input").eq(0).val("");
		$(this).parent().parent().find("input").eq(1).val("");
		$(".p-box .comm-del").on("click", function() {
			if ($(this).parent().parent().parent().find(".comm-write").length > 0) {
			    console.log("调试文本:"+$(this).parent().parent().children("b").eq(0).text());
			    console.log("当前文本为:"+$(this).parent().parent().next().children(".stage-box").children().text());
				$(this).parent().parent().next().children(".stage-box").children().text($(this).parent().parent().children("b").eq(0).text());
				$(this).parent().parent().prev().find(".comm-del").css({
					"display" : "block"
				})
			}
			$(this).parent().parent().next().children("span").text($(this).parent().parent().children("b").eq(0).text());
			$(this).parent().parent().remove();
		});
	} else {
		if ($(this).parent().parent().find("input").eq(0).val() == "") {
			$(this).parent().parent().find("input").eq(0).css({
				"border" : "1px solid #ff5a60"
			});
		} else {
			$(this).parent().parent().find("input").eq(0).css({
				"border" : "1px solid #9ea0b7"
			});
		}
		if ($(this).parent().parent().find("input").eq(1).val() == "") {
			$(this).parent().parent().find("input").eq(1).css({
				"border" : "1px solid #ff5a60"
			});
		} else {
			$(this).parent().parent().find("input").eq(1).css({
				"border" : "1px solid #9ea0b7"
			});
		}
	}
	// if ($(this).parent().parent().find("input").) {};
	// $(this).parent().parent()
})
