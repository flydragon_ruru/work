// 右上角 下拉菜单
$(".peo-message").on("click", function() {
 if ($(this).hasClass("op-peomessage")) {
  $(this).removeClass("op-peomessage");
 } else {
  $(this).addClass("op-peomessage");
 }
});
// 左边下拉
$(".down-list").parent().on("click", function() {
	if (!$(this).parent().hasClass("active")) {
		$(".active").removeClass("active");
		$(this).parent().addClass("active");
	} else {
		$(".active").removeClass("active");
	}
})
$(".menu-wrap a").on("click", function() {
	if (!$(this).hasClass("active-link")) {
		$(".active-link").removeClass("active-link");
		$(this).addClass("active-link");
	} else {
		$(".active-link").removeClass("active-link");
	}
})

$(".test").on("click", function() {
	$(".content-container").load("test.html");
})
/* 下拉列表 */
$(".downbtn").on("click", function() {
	if ($(this).next("ul").hasClass("menuopen")) {
		$(this).next("ul").removeClass("menuopen");
	} else {
		$(this).next("ul").addClass("menuopen");
		$(this).parent().hover(function() {
			
		}, function() {
			$(this).children("ul").removeClass("menuopen");
		});
		$(".menuopen li").on("click", function() {
			$(this).parent().removeClass("menuopen");
			$(this).parent().parent().children("div").children("span").text($(this).text());
			if ($(this).parent().parent().attr("Choice") == "mandatory" || $(this).parent().parent().attr("Choice") == "selected") {
				if ($(this).index() != 0) {
					// if ($(this).parent().parent().hasClass("b-sex")) {
					// 	console.log("aaaaaa");
					// };
					if ($(this).parent().parent().hasClass("spe")) {
						
						$(this).parent().parent().children("div").children("span").attr("data_id", $(this).attr("data_id"));
					};
					$(this).parent().parent().attr("Choice", "selected");
					$(this).parent().parent().next("b").css({
						"display" : "none"
					})
				} else {
					$(this).parent().parent().attr("Choice", "mandatory");
				}
			};
		})
	}
});
/* 复选框 */
$(".check").on("click", function() {
	if ($(this).prev("input").prop("checked")) {
		$(this).prev("input").prop("checked", false);
		$(this).removeClass("ad-checked");
		$(this).children("span").css({
			"display" : "none"
		})
	} else {
		$(this).addClass("ad-checked");
		$(this).prev("input").prop("checked", true);
		$(this).children("span").css({
			"display" : "block"
		})
	}
})
/* 日历  启示日期  结束日期 */
function updateT1() {
	var d = $('.date-list1').val().split("-");
	for (var i = 0; i < d.length; i++) {
		d[i] = Number(d[i]);
	};
	var trs = $('.date-list2').next().children("table").children("tbody").children();
	for (var i = 0; i < trs.length; i++) {
		for (var j = 0, tds = $(trs[i]).children(); j < tds.length; j++) {
			var tDate = $(tds[j]).attr("date").split("-");
			if ($(tds[j]).hasClass("unselected_mon")) {
				$(tds[j]).removeClass("unselected_mon");
			};
			for (var t = 0; t < tDate.length; t++) {
				tDate[t] = Number(tDate[t]);
			};
			if ($('.date-list1').val() != "") {
				if (tDate[0] < d[0] || (tDate[0] == d[0] && tDate[1] < d[1]) || (tDate[0] == d[0] && tDate[1] == d[1] && tDate[2] < d[2])) {
					$(tds[j]).addClass("unselected_mon");
				}
			}
		};
	};
}

function updateT2() {
	var tDate = $('.date-list2').val().split("-");
	for (var i = 0; i < tDate.length; i++) {
		tDate[i] = Number(tDate[i]);
	};
	var trs = $('.date-list1').next().children("table").children("tbody").children();
	for (var i = 0; i < trs.length; i++) {
		for (var j = 0, tds = $(trs[i]).children(); j < tds.length; j++) {
			var d = $(tds[j]).attr("date").split("-");
			if ($(tds[j]).hasClass("unselected_mon")) {
				$(tds[j]).removeClass("unselected_mon");
			};
			for (var t = 0; t < d.length; t++) {
				d[t] = Number(d[t]);
			};
			if ($('.date-list2').val() != "") {
				if (tDate[0] < d[0] || (tDate[0] == d[0] && tDate[1] < d[1]) || (tDate[0] == d[0] && tDate[1] == d[1] && tDate[2] < d[2])) {
				$(tds[j]).addClass("unselected_mon");
				}
			};
		};
	};
}

// 添加课程模块 单词售卖课程 
$(".cb-box .dropdown-menu").eq(0).children("li").on("click", function() {
	if ($(this).text() == "单次售卖课程") {
		$(".period").removeClass("per-show");
	} else {
		$(".period").addClass("per-show");
	}
})
$(".cb-box .dropdown-menu").eq(1).children("li").on("click", function() {
	if ($(this).text() == "私教") {
		$(".duration").removeClass("dur-show");
		$(".card").removeClass("card-show");
		$(".order").css({
			"display" : "none"
		});
		$(".private").css({
			"display" : "block"
		});
	} else {
		$(".duration").addClass("dur-show");
		$(".card").addClass("card-show");
		$(".order").css({
			"display" : "block"
		});
		$(".private").css({
			"display" : "none"
		});
	}
})
$(".ct-ch").on("click", function() {
	if($(this).prev().attr("checked")) {
		$(this).parent().parent().next().next("input").addClass("required");
	} else {
		$(this).parent().parent().next().next("input").removeClass("required").css({
			"border" : "1px solid #a4a6bc" 
		})
	}
})
// 添加课程时间
$(".ct-box .comm-add").on("click", function() {
	if (!$(this).parent().hasClass("ctl")) {
		var canAdd = true;
		if ($(this).parent().children(".list-box").attr("choice") == "selected") {
			$(this).parent().children(".list-box").next("b").css({
				"display" : "none"
			});
		} else {
			canAdd = false;
			$(this).parent().children(".list-box").next("b").css({
				"display" : "inline"
			});
		}
		if ($(this).parent().children("input").eq(0).val() != "" && Number($(this).parent().children("input").eq(0).val()) < 24) {
			if ($(this).parent().children("input").eq(0).val().length > 2) {
				$(this).parent().children("input").eq(0).val($(this).parent().children("input").eq(0).val().replace(/\b(0+)/gi,""));
			};
			if ($(this).parent().children("input").eq(0).val().length < 2) {
				$(this).parent().children("input").eq(0).val("0" + $(this).parent().children("input").eq(0).val());
			};
			$(this).parent().children("input").eq(0).css({
				"border" : "1px solid #9ea0b7"
			});
		} else {
			canAdd = false;
			$(this).parent().children("input").eq(0).css({
				"border" : "1px solid #ff5a60"
			});
		}
		if ($(this).parent().children("input").eq(1).val() != "" && Number($(this).parent().children("input").eq(1).val()) < 60) {
			if ($(this).parent().children("input").eq(1).val().length > 2) {
				$(this).parent().children("input").eq(1).val($(this).parent().children("input").eq(1).val().replace(/\b(0+)/gi,""));
			};
			if ($(this).parent().children("input").eq(1).val().length < 2) {
				$(this).parent().children("input").eq(1).val("0" + $(this).parent().children("input").eq(1).val());
			};
			$(this).parent().children("input").eq(1).css({
				"border" : "1px solid #9ea0b7"
			});
		} else {
			canAdd = false;
			$(this).parent().children("input").eq(1).css({
				"border" : "1px solid #ff5a60"
			});
		}
		if (canAdd) {
			$(this).parent().children(".list-box").attr("choice", "mandatory");
			$(this).parent().before($("<div class='ct-write'><strong data_id='" + $(this).parent().children('.list-box').find('span').attr("data_id") + "'>" + $(this).parent().children('.list-box').find('span').text() + "</strong><span>" + $(this).parent().children("input").eq(0).val() + "</span><em>时</em><span>" + $(this).parent().children("input").eq(1).val() + "</span><em>分</em><div class='comm-del'>删除</div></div>"));
			$(this).parent().parent().find(".ct-write").last().prevAll().find(".comm-del").css({
				"display" : "none"
			})
			$(this).parent().children(".list-box").find("span").text($(this).parent().children(".list-box").find("li").eq(0).text());
			$(this).parent().children("input").eq(0).val("");
			$(this).parent().children("input").eq(1).val("");
		};
	} else {
		var canAdd = true;
		if ($(this).parent().find("input").eq(0).val() != "") {
			$(this).parent().find("input").eq(0).css({
				"border" : "1px solid #9ea0b7"
			});
		} else {
			canAdd = false;
			$(this).parent().find("input").eq(0).css({
				"border" : "1px solid #ff5a60"
			});;
		}
		if ($(this).parent().find("input").eq(1).val() != "" && Number($(this).parent().find("input").eq(1).val()) < 24 && Number($(this).parent().find("input").eq(1).val()) >= 0) {
			if ($(this).parent().find("input").eq(1).val().length > 2) {
				$(this).parent().find("input").eq(1).val($(this).parent().find("input").eq(1).val().replace(/\b(0+)/gi,""));
			};
			if ($(this).parent().find("input").eq(1).val().length < 2) {
				$(this).parent().find("input").eq(1).val("0" + $(this).parent().find("input").eq(1).val());
			};
			$(this).parent().find("input").eq(1).css({
				"border" : "1px solid #9ea0b7"
			});
		} else {
			canAdd = false;
			$(this).parent().find("input").eq(1).css({
				"border" : "1px solid #ff5a60"
			});
		}
		if ($(this).parent().find("input").eq(2).val() != "" && Number($(this).parent().find("input").eq(2).val()) < 60 && Number($(this).parent().find("input").eq(2).val()) >= 0) {
			if ($(this).parent().find("input").eq(2).val().length > 2) {
				$(this).parent().find("input").eq(2).val($(this).parent().find("input").eq(2).val().replace(/\b(0+)/gi,""));
			};
			if ($(this).parent().find("input").eq(2).val().length < 2) {
				$(this).parent().find("input").eq(2).val("0" + $(this).parent().find("input").eq(2).val());
			};
			$(this).parent().find("input").eq(2).css({
				"border" : "1px solid #9ea0b7"
			});
		} else {
			canAdd = false;
			$(this).parent().find("input").eq(2).css({
				"border" : "1px solid #ff5a60"
			});
		}
		if (canAdd) {
			$(this).parent().before($("<div class='ct-write'><strong>" + $(this).parent().find("input").eq(0).val() + "</strong><span>" + $(this).parent().find("input").eq(1).val() + "</span><em>时</em><span>" + $(this).parent().find("input").eq(2).val() + "</span><em>分</em><div class='comm-del'>删除</div></div>"));
			$(this).parent().parent().find(".ct-write").last().prevAll().find(".comm-del").css({
				"display" : "none"
			})
			$(this).parent().find("input").eq(0).val("");
			$(this).parent().find("input").eq(1).val("");
			$(this).parent().find("input").eq(2).val("");
		};
	}
	$(".comm-del").on("click", function() {
		$(this).parent().parent().find(".ct-write").last().prev(".ct-write").find(".comm-del").css({
			"display" : "block"
		})
		$(this).parent().remove();
	})
})
// 添加课程模块 保存按钮
$(".preserve").on("click", function() { 
	if (confirmSet()) {
		console.log("可以跳转到下一步");
		var isOrde;
		if($(".cb-box .list-box").eq(1).find("span").text() == "私教") {
			isOrder = true;
		} else {
			if ($(".order .check").prev("input").attr("checked")) {
				isOrder = true;
			} else {
				isOrder = false;
			}
		};
	 	
		var postJson = {
		   	"courseinfo" : [{
		      	"sale_type" : $(".cb-box .list-box").eq(0).find("span").text(),
		      	"course_type" : $(".cb-box .list-box").eq(1).find("span").text(),
		      	"teacher_id" : $(".cb-box .list-box").eq(2).find("span").attr("data_id"),
		      	"room_id" : $(".cb-box .list-box").eq(3).find("span").attr("data_id"),
		     	"dance_type" : $(".cb-box .list-box").eq(4).find("span").text(),
		     	"age_group" : $(".cb-box .list-box").eq(5).find("span").text(),
		      	"name" : $(".cb-box input").eq(0).val(),
		      	"duration" : $(".cb-box input").eq(1).val(),
		     	"appointmented_or_not" : isOrder,
		     	"pay_ways" : [{
	                         		// "次卡" : "",
	                         		// "充值" : "",
	                         		// "直接售卖" : "",
	                         		// "年卡" : "",
	                         		// "月卡" : ""
	                      		}],//消费方式
	      		"course_set" : []
			}],
                        'utf-8': $("input[name=utf-8]").val(),
                        'authenticity_token': $("input[name=authenticity_token]").val()
		};
		if($(".cb-box .list-box").eq(0).find("span").text() == "打包售卖课程") {
			postJson.courseinfo[0].period = $("#cho").val();
		} 
		if ($(".consumption .check").eq(0).prev("input").attr("checked")) {
			postJson.courseinfo[0].pay_ways[0].次卡 = $(".consumption .ct-i").eq(0).val();
		};
		if ($(".consumption .check").eq(1).prev("input").attr("checked")) {
			postJson.courseinfo[0].pay_ways[0].充值 = $(".consumption .ct-i").eq(1).val();
		};
		if ($(".consumption .check").eq(2).prev("input").attr("checked")) {
			postJson.courseinfo[0].pay_ways[0].直接售卖 = true;
		} else {
			postJson.courseinfo[0].pay_ways[0].直接售卖 = false;
		}
		if ($(".consumption .check").eq(3).prev("input").attr("checked")) {
			postJson.courseinfo[0].pay_ways[0].年卡 = true;
		} else {
			postJson.courseinfo[0].pay_ways[0].年卡 = false;
		}
		if ($(".consumption .check").eq(4).prev("input").attr("checked")) {
			postJson.courseinfo[0].pay_ways[0].月卡 = true;
		} else {
			postJson.courseinfo[0].pay_ways[0].月卡 = false;
		}
		if ($("#weekly .ck").attr("checked") && $("#weekly .date-list1").val()!= "" && $("#weekly .date-list2").val()!= "") {
			if ($("#weekly .ct-write").length > 0) {
				var wkTime = [];
				var week = {};
				week.type = "每周循环";
				week.course_begin_at = $(".date-list1").val();
				week.course_over_at = $(".date-list2").val();
				for (var i = 0; i < $("#weekly .ct-write").length; i++) {
					var obj = {};
					obj.星期 = $($("#weekly .ct-write")[i]).find("strong").attr('data_id');
					obj.小时 = $($("#weekly .ct-write")[i]).find("span").eq(0).text();
					obj.分钟 = $($("#weekly .ct-write")[i]).find("span").eq(1).text();
					wkTime.push(obj);
				};	
				week.时间 = wkTime;
				postJson.courseinfo[0].course_set.push(week);
			};
		};
		if ($("#oneday .ck").attr("checked")) {
			if ($("#oneday .ct-write").length > 0) {
				var wkTime = [];
				var week = {};
				week.type = "单天设置";
				for (var i = 0; i < $("#oneday .ct-write").length; i++) {
					var obj = {};
					obj.星期 = $($("#oneday .ct-write")[i]).find("strong").text();
					obj.小时 = $($("#oneday .ct-write")[i]).find("span").eq(0).text();
					obj.分钟 = $($("#oneday .ct-write")[i]).find("span").eq(1).text();
					wkTime.push(obj);
				};
				week.时间 = wkTime;
				postJson.courseinfo[0].course_set.push(week);
			};
		}
		console.log(postJson);
		
                $.ajax({
                  data: postJson,
                  type:'post', url:"/admin/course_infos",
                  success: function(data) {
                    if (data.error) {
                      // console.log("wrong");
                    } 
                  }
                });
	}
})

// 验证是否必填
function confirmSet() {
	var writeIn = true;
	for (var i = 0; i < $(".required").length; i++) {
		if ($($(".required")[i]).val() == "") {
			$($(".required")[i]).css({
				"border" : "1px solid #ff5a60" 
			});
			writeIn = false;
		} else {
			$($(".required")[i]).css({
				"border" : "1px solid #9ea0b7" 
			});
		}
	}
	for (var i = 0; i < $(".cb-box .list-box").length; i++) {
		if ($($(".cb-box .list-box")[i]).attr("choice") == "mandatory") {
			$($(".cb-box .list-box")[i]).next("b").css({
				"display" : "inline"
			})
			writeIn = false;
		};
	}
	if($(".cb-box .list-box").eq(0).find("span").text() == "打包售卖课程") {
		if ($("#cho").val() == "") {
			$("#cho").css({
				"border" : "1px solid #ff5a60" 
			});
			writeIn = false;
		} else {
			$("#cho").css({
				"border" : "1px solid #9ea0b7" 
			});
		}
	}
	if ($("#weekly .ck").attr("checked")) {
		if ($("#weekly .ct-write").length > 0) {
			$("#weekly .not-ad").css({
				"display" : "none"
			});
			if ($("#weekly .date-list1").val() == "") {
				$("#weekly .date-list1").css({
					"border" : "1px solid #ff5a60" 
				});
				writeIn = false;
			} else {
				$("#weekly .date-list1").css({
					"border" : "1px solid #9ea0b7" 
				});
			}
			if ($("#weekly .date-list2").val() == "") {
				$("#weekly .date-list2").css({
					"border" : "1px solid #ff5a60" 
				});
				writeIn = false;
			} else {
				$("#weekly .date-list2").css({
					"border" : "1px solid #9ea0b7" 
				});
			}
		} else {
			$("#weekly .not-ad").css({
				"display" : "block"
			});
			writeIn = false;
		}
	}
	if ($("#oneday .ck").attr("checked")) {
		if ($("#oneday .ct-write").length == 0) {
			$("#oneday .not-ad").css({
				"display" : "block"
			});
			writeIn = false;
		} else  {
			$("#oneday .not-ad").css({
				"display" : "none"
			});
		}
	}
	return writeIn;
}
// 验证
function clearNoNum(obj) {
    //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d.]/g,"");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,"");
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g,".");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
}
function intNum(obj) {
	 //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d]/g,"");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,"");
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g,".");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
}



/********** 放入common.js *／
/* 多选下拉列表 */
$(".mul-downbtn").on("click", function() {
  if (!$(this).next().hasClass("menuopen")) {
    $(this).next().addClass("menuopen");
  }
});
$(".multiselect-down li").on("click", function() {
  if ($(this).hasClass("mul-select")) {
    $(this).removeClass("mul-select");
  } else {
    $(this).addClass("mul-select");
  };
});
$(".mul-confirm").on("click", function() {
  var arr = [];
  for (var i = 0; i < $(this).parent().parent().find(".mul-select").length; i++) {
    arr.push($($(this).parent().parent().find(".mul-select")[i]).children("strong").text());
  };
  console.log(arr);
  // 在此处加入ajax 提交数据
  $(this).parent().parent().removeClass("menuopen");
})
$(".mul-cancel").on("click", function() {
  $(this).parent().parent().removeClass("menuopen");
})
$(".mul-downbtn input").on('input',function(e){
  if ($(this).val() != "") {
    // console.log($(this).val());
    $(".multiselect-down li").addClass("mul-hide");
    $($(".multiselect-down li:contains(" + $(this).val() + ")")).removeClass("mul-hide");
  } else {
    $(".multiselect-down li").removeClass("mul-hide");
  }
});  
