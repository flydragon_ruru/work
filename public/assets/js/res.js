// 调用函数
var peoPhone = "";
function formBuild(parent, user, pwd, verf) {
	var r = {
		user: /^1[34578]\d{9}$/,
		password: /^.{6,20}$/
	}
	var err = {
		conform: "符合要求",
		errorPhone: "请输入正确的手机号",
		errorPwd: "请输入正确的密码"
	}
	var ipt = $(parent);
	var userNum = 0;
	if (verf == 0) {
		pwdNum = 1;
	} else if(verf == 1) {
		pwdNum = 3;
	} else if(verf == 2) {
		pwdNum = 2;
	}
	if (user) {
		var userError = $(user);
		var userT = false;
		if (ipt.eq(userNum).val() != "") {
			ipt.eq(userNum).siblings("label").parent().removeClass("show-placeholder");
			console.log("userNum:"+userNum+","+ipt.eq(userNum).val()+",结果为:"+r.user.test(ipt.eq(userNum).val()));
			if (r.user.test(ipt.eq(userNum).val())) {
				userT = true;
			}
		} else {
			console.log(2);
			ipt.eq(userNum).siblings("label").parent().addClass("show-placeholder");		
		}
	};
	if (pwd) {
		var pwdError = $(pwd);
		var pwdT = false;
		if (ipt.eq(pwdNum).val() != "") {
			ipt.eq(pwdNum).siblings("label").parent().removeClass("show-placeholder");
			if (r.password.test(ipt.eq(pwdNum).val())) {
				pwdT = true;
			}
		} else {
			ipt.eq(pwdNum).siblings("label").parent().addClass("show-placeholder");		
		}
	};

	$(parent).on("keydown", function(e){
		var e = e || window.event;

		// 按下tab键，水印文字会消失，需要进行判断处理
		if (e.keyCode != "9") {
			$(this).siblings("label").parent().removeClass("show-placeholder");
		}

	}).on("keyup", function(){
		if ($(this).val() == "") {
			$(this).siblings("label").parent().addClass("show-placeholder");
		} else {
			$(this).siblings("label").parent().removeClass("show-placeholder");
			if($(this).parent().hasClass("ipt-user")) {
				if (r.user.test(ipt.eq(userNum).val())) {
					$(userError).css({
						"display" : "none"
					});
					userT = true;
				} 
			} else if ($(this).parent().hasClass("ipt-pwd")) {
				if (r.password.test(ipt.eq(pwdNum).val())) {
					$(pwdError).css({
						"display" : "none"
					});
					pwdT = true;
				}
			}
		}
	});

	
	$("#username").blur(function(){
		if (ipt.eq(userNum).val() != "") {
			if (!r.user.test(ipt.eq(userNum).val())) {
				$(userError).css({
					"display" : "block"
				});
				userError.text(err.errorPhone);
				userT = false;
			} else {
				$(userError).css({
					"display" : "none"
				});
				userT = true;
			}
		} else {
			$(userError).css({
				"display" : "block"
			});
			userError.text("手机号不能为空");
			userT = false;
		}
	})
	$("#password").blur(function() {
		if (ipt.eq(pwdNum).val() != "") {
			if (!r.password.test(ipt.eq(pwdNum).val())) {
				$(pwdError).css({
					"display" : "block"
				});
				pwdError.text(err.errorPwd);
				pwdT = false;
			} else {
				$(pwdError).css({
					"display" : "none"
				});
				pwdT = true;
			}
		} else {
			$(pwdError).css({
				"display" : "block"
			});
			pwdError.text("密码不能为空");
			pwdT = false;
		}
	});

	
	$("#name").blur(function(){
		if ($(this).val() == "") {
			$(".name-err").css({
				"display" : "block"
			});
			$(".name-err").text("用户名不能为空");
		} else {
			$(".name-err").css({
				"display" : "none"
			});
		}
	});
	$("#password-again").blur(function() {
		if ($(this).val() != $("#password").val()) {
			$(".pwd-again-err").css({
				"display" : "block"
			});
			$(".pwd-again-err").text("确认密码不正确");
		} else if($(this).val() == ""){
			$(".pwd-again-err").css({
				"display" : "block"
			});
			$(".pwd-again-err").text("确认密码不能为空");
		} else {
			$(".pwd-again-err").css({
				"display" : "none"
			});
		}
	});
	
	$(".change-btn").on("click", function() {
		if (ipt.eq(userNum).val() != "") {
			if (!r.user.test(ipt.eq(userNum).val())) {
				$(userError).css({
					"display" : "block"
				});
				userError.text(err.errorPhone);
				userT = false;
			} else {
				$(userError).css({
					"display" : "none"
				});
				userT = true;
			}
		} else {
			$(userError).css({
				"display" : "block"
			});
			userError.text("手机号不能为空");
			userT = false;
		}

		if (ipt.eq(pwdNum).val() != "") {
			if (!r.password.test(ipt.eq(pwdNum).val())) {
				$(pwdError).css({
					"display" : "block"
				});
				pwdError.text(err.errorPwd);
				pwdT = false;
			} else {
				$(pwdError).css({
					"display" : "none"
				});
				pwdT = true;
			}
		} else {
			$(pwdError).css({
				"display" : "block"
			});
			pwdError.text("密码不能为空");
			pwdT = false;
		}
		if (r.user.test(ipt.eq(userNum).val()) && r.password.test(ipt.eq(pwdNum).val())) {
			// 此处 进行验证    判断验证码输入的是否正确  如果正确则进入数据库进行密码的修改 如果不正确则弹窗提示 
			var  user_data = {
				user: {
					mobile: $("#username").val(),					
					password: $("#password").val(),
				},
				'utf-8': $("input[name=utf-8]").val(),
				'authenticity_token': $("input[name=authenticity_token]").val(),
				verf: $("#verf").val()
			};
			$.ajax({
				data: user_data,
				type:'post', url:"/admin/users/change_pwd",
				success: function(data) {
					if (data.error) {
						// console.log("wrong");
					} 
				}
			});
		}
	})
	$(".login-btn a").on("click", function() {
       _do_login();
	});

    var _do_login = function(){

    	//主要处理浏览器,自动填充用户名密码的情况
        var username = $("#username").val();
        var password = $("#password").val();
        userT = r.user.test(username);
        pwdT = r.password.test(password);
  //       console.log("user_name:"+username+",password:"+password);
  //       console.log("pwdT:"+pwdT+",userT:"+userT);
        
		// console.log("登录按钮点击!");
		if (pwdT && userT) {

			if ($("#remember").prop("checked")) {
				localStorageUser(true);
			} else {
				localStorageUser(false);
			}
			// console.log("此处放的是  登陆页面成功跳转链接");
			var user = {
				username: $("#username").val(),
				password: $("#password").val()
			};
			// console.log("通过登录验证,信息为:"+JSON.stringify(user));
			$.ajax({
				data: user,
				type:'post', url:"/admin/sessions/sign_in",
				success: function(data) {
					console.log(data.message);
					userError.show().text(data.message);
					pwdError.show().text(data.message);
					userT = false;
					pwdT = false;
					// if (data.error) {
					//    // console.log("wrong");	
					// }
				}
			});
		} else {
            console.log("userNum:"+ipt.eq(userNum).val() );
			if (ipt.eq(userNum).val() != "") {
				if (!r.user.test(ipt.eq(userNum).val())) {
					$(userError).css({
						"display" : "block"
					});
					userError.text(err.errorPhone);
					userT = false;
				} else {
					$(userError).css({
						"display" : "none"
					});
					userT = true;
				}
			} else {
				$(userError).css({
					"display" : "block"
				});
				userError.text("手机号不能为空");
				userT = false;
			}

			if (ipt.eq(pwdNum).val() != "") {
				if (!r.password.test(ipt.eq(pwdNum).val())) {
					$(pwdError).css({
						"display" : "block"
					});
					pwdError.text(err.errorPwd);
					pwdT = false;
				} else {
					$(pwdError).css({
						"display" : "none"
					});
					pwdT = true;
				}
			} else {
				$(pwdError).css({
					"display" : "block"
				});
				pwdError.text("密码不能为空");
				pwdT = false;
			}
            
			console.log("未通过登录验证,userError:"+userError.text()+",pwdError:"+pwdError.text());
		}
    }

    this._do_login = _do_login;

	$(".res-btn1").on("click", function() {
		// alert(1);
		// console.log("返回信息为:"+JSON.stringify(data));
		if (userT) {
			// 备注  添加 获取验证是否正确  是否跳转到下一步
			// console.log("此处放的是 注册页面  下一步按钮成功跳转的链接");
			peoPhone = $("#username").val();
			$.ajax({
				type:'get', url:"/admin/users/search_user_by_mobile?mobile="+peoPhone ,
				success: function(data) {
					console.log("返回信息为:"+JSON.stringify(data));
					console.log("返回信息为:"+data.message);
					if (data.message!=null) {
						// console.log("wrong");
						$(".user-err").show().text("手机号已注册,请直接登录!");
						userT = false;
					}
				}
			})
			// $(".re-con").load("res-t.html");
		} else {
			if (ipt.eq(userNum).val() != "") {
				if (!r.user.test(ipt.eq(userNum).val())) {
					$(userError).css({
						"display" : "block"
					});
					userError.text(err.errorPhone);
					userT = false;
				} else {
					$(userError).css({
						"display" : "none"
					});
					userT = true;
				}
			} else {
				$(userError).css({
					"display" : "block"
				});
				userError.text("手机号不能为空");
				userT = false;
			}
		}
	});
	$(".res-btn2").on("click", function() {
		if (pwdT && userT && $("#verf").val() != "" && $("#name").val() != "" && $("#password-again").val() == $("#password").val()){
			// console.log("此处放的是注册按钮成功跳转的链接");
			var  user_data = {
				user: {
					mobile: $("#username").val(),					
					name: $('#name').val(),
					password: $("#password").val(),
					password_confirmation: $("#password-again").val()
				},
				'utf-8': $("input[name=utf-8]").val(),
				'authenticity_token': $("input[name=authenticity_token]").val(),
				verf: $("#verf").val()
			};
			$.ajax({
				data: user_data,
				type:'post', url:"/admin/users",
				success: function(data) {
					if (data.error) {
						// console.log("wrong");
					} 
				}
			});

		} else {
			if (ipt.eq(userNum).val() != "") {
				if (!r.user.test(ipt.eq(userNum).val())) {
					$(userError).css({
						"display" : "block"
					});
					userError.text(err.errorPhone);
					userT = false;
				} else {
					$(userError).css({
						"display" : "none"
					});
					userT = true;
				}
			} else {
				$(userError).css({
					"display" : "block"
				});
				userError.text("手机号不能为空");
				userT = false;
			}

			if (ipt.eq(pwdNum).val() != "") {
				if (!r.password.test(ipt.eq(pwdNum).val())) {
					$(pwdError).css({
						"display" : "block"
					});
					pwdError.text(err.errorPwd);
					pwdT = false;
				} else {
					$(pwdError).css({
						"display" : "none"
					});
					pwdT = true;
				}
			} else {
				$(pwdError).css({
					"display" : "block"
				});
				pwdError.text("密码不能为空");
				pwdT = false;
			}

			if ($("#name").val() == "") {
				$(".name-err").css({
					"display" : "block"
				});
				$(".name-err").text("用户名不能为空");
			};

			if ($("#password-again").val() != $("#password").val()) {
				$(".pwd-again-err").css({
					"display" : "block"
				});
				$(".pwd-again-err").text("确认密码不正确");
			} else if($("#password-again").val() == ""){
				$(".pwd-again-err").css({
					"display" : "block"
				});
				$(".pwd-again-err").text("确认密码不能为空");
			} else {
				$(".pwd-again-err").css({
					"display" : "none"
				});
			}
		}
	})
	var verfT = false;
	$(".verf-btn").on("click", function() {
		if (verfT) {
			return;
		} else {
			verfT = true;
			$(this).addClass("un-verf");
			peoPhone = $("#username").val();
			//获取验证码
			$.ajax({
				type:'get', url:"/sms/captcha?mobile="+peoPhone+'&target=signup',
				success: function(data) {
					if (data.error) {
						// console.log("wrong");
					} 
				}
			});
		}
		var nowT = 60;
		var time = setInterval(function() {
			nowT--;
			if (nowT == -1) {
				clearInterval(time);
				nowT = 60;
				$(".verf-btn em").text(nowT);
				$(".verf-btn").removeClass("un-verf");
				verfT = false;
			};
			$(".verf-btn em").text(nowT);
		}, 1000)
	}	);
	var forT = false;
	$("#forget").on("click", function() {
		if (forT) {
			return;
		} else {
			forT = true;
			$(this).addClass("un-verf");
			peoPhone = $("#username").val();
			//获取验证码
			$.ajax({
				type:'get', url:"/sms/captcha?mobile="+peoPhone+'&target=modify_password',
				success: function(data) {
					if (data) {
						if (data.error) {
							alert("您的手机号没有注册");
						} 
					}
				}
			});
		}
		var nowT = 60;
		var time = setInterval(function() {
			nowT--;
			if (nowT == -1) {
				clearInterval(time);
				nowT = 60;
				$("forget em").text(nowT);
				$(".forget").removeClass("un-verf");
				forT = false;
			};
			$(".forget em").text(nowT);
		}, 1000)
	});
} 

// localStorage方法
  function localStorageUser(save) {
  	if(!save) {
  		var isSave = false;
  		var ls = window.localStorage;
  		if (ls) {
  			ls.setItem("isSave", isSave);
  			var s =  JSON.parse(ls.getItem("isSave"));
  		};
  	} else {
  		var user = {
  			username: $("#username").val(),
  			password: $("#password").val()
  		}
  		var isSave = true;
  		var ls = window.localStorage;
  		if (ls) {
  			ls.setItem("user", JSON.stringify(user));
  			ls.setItem("isSave", isSave);
  			var obj =  JSON.parse(ls.getItem("user"));
  			var s =  JSON.parse(ls.getItem("isSave"));
  		};
  	}
  }
/* 复选框 */
$(".check").on("click", function() {
	if ($(this).prev("input").prop("checked")) {
		$(this).prev("input").prop("checked", false);
		$(this).removeClass("ad-checked");
		$(this).children("span").css({
			"display" : "none"
		})
	} else {
		$(this).addClass("ad-checked");
		$(this).prev("input").prop("checked", true);
		$(this).children("span").css({
			"display" : "block"
		})
	}
})
