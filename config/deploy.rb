# config valid only for current version of Capistrano
lock '3.7.2'

set :application, 'yundiudiuHeipis'
# set :deploy_user, 'deploy'
set :ssh_options, { user: 'deploy' }
set :repo_url, 'git@git.coding.net:heipis/yundiudiuHeipis.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name


# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
append :linked_files, 'config/database.yml', 'config/secrets.yml', 'config/redis.yml'

# Default value for linked_dirs is []
# set :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system', 'public/upload','public/compressor'

set :linked_dirs, ['log', 'tmp/pids', 'tmp/cache', 'tmp/sockets','public/compressor']

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# set :passenger_restart_with_touch, true


#There is a known bug that prevents sidekiq from starting when pty is true on Capistrano 3.
set :pty,  false

# set :rbenv_type, :user # or :system, depends on your rbenv setup
# set :rbenv_ruby, '2.3.1'

set :rvm_type, :user                     # Defaults to: :auto
set :rvm_ruby_version, '2.3.3'      # Defaults to: 'default'

set :use_sudo, true



# require "whenever/capistrano"
# set :whenever_variables, defer { "'environment=#{rails_env}&current_path=#{current_path}'" }

#把静态文件复制到assets_tmp 文件中
before "deploy:symlink:linked_dirs", "custom:mkdir_assets_tmp"
#复制临时文件里面的静态文件
after "deploy:symlink:linked_dirs", "custom:cp_assets"

# before :starting, :ensure_user
# deploy:symlink:linked_dirs

# p "发布路径为:#{shared_path}"

namespace :custom do

  desc "创建临时静态文件夹"
  task :mkdir_assets_tmp do 
  	on roles(:web) do 
  	  execute :mkdir, " -p #{release_path}/public/assets_tmp"	
  	  execute :cp, " -rf #{release_path}/public/assets/* #{release_path}/public/assets_tmp"
  	end
  end

  desc "复制临时静态文件夹的文件到public/assets "
  task :cp_assets do
    on roles(:web) do
      execute :cp, " -rf #{release_path}/public/assets_tmp/* #{release_path}/public/assets/"
      execute :rm, " -rf #{release_path}/public/assets_tmp"
    end
  end

end