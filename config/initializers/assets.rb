require  'find'
# Be sure to restart your server when you modify this file.
# Version of your assets, change this if you want to expire all your assets.
# begin

Rails.application.config.assets.version='1.0'
# rescue  Exception => e
#   p "异常信息为：#{e.message}"
# end

# p "assets version:#{Rails.application.assets.version}"
# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.

#需要整合的样式扩展名
NEED_TO_COMPILE_EXT = %w(.scss .css .coffee .js)

#js文件
Find.find("#{Dir::pwd}/app/assets/javascripts") do |filename|
  ext = File.extname(filename)
  if File.file?(filename) && NEED_TO_COMPILE_EXT.include?(ext)
    Rails.application.config.assets.precompile << "#{File.basename(filename,'.*')}.js"
  end
  # puts "文件名:#{File.basename(filename)} , 扩展名:#{File.extname(filename)} " if File.file?(filename)
end

#js文件
Find.find("#{Dir::pwd}/app/assets/stylesheets") do |filename|
  ext = File.extname(filename)
  if File.file?(filename) && NEED_TO_COMPILE_EXT.include?(ext)
    Rails.application.config.assets.precompile << "#{File.basename(filename,'.*')}.css"
  end
  # puts "文件名:#{File.basename(filename)} , 扩展名:#{File.extname(filename)} " if File.file?(filename)
end