require 'qiniu'

qiniu_conf = QiniuConfService.call
#七牛云初始化  flydragon
Qiniu.establish_connection! access_key: qiniu_conf.access_key,
                            secret_key: qiniu_conf.secret_key