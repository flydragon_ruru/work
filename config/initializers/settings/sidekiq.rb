redis_conn = proc {
  redis_config = YAML.load_file('config/redis.yml')
  redis = redis_config[Rails.env || ENV['RACK_ENV'] || 'development']
  Redis.new(redis)
}

Redis.current = redis_conn.call

# default timeout 5
Sidekiq.configure_client do |config|
  config.redis = ConnectionPool.new(size: 5, &redis_conn)
end

Sidekiq.configure_server do |config|
  config.redis = ConnectionPool.new(size: 50, &redis_conn)
  # poll_interval = processes * time
  config.average_scheduled_poll_interval = 50
end

Sidekiq.default_worker_options = {retry: false}

# Sidekiq自定义日志
# Sidekiq.logger.formatter = Sidekiq::Logging::CustomLogger.new
