Time::DATE_FORMATS.merge!(  
  :friendly => lambda { |time|  
    if time.year == Time.now.year  
      time.strftime "%m月 "  
    else  
      time.strftime "%m月, %Y"  
    end  
  }  
) 