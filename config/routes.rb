Rails.application.routes.draw do

  get 'qiniu/uptoken'
  post 'qiniu/private_url'

  root 'admin/sessions#new'
  get 'sms/captcha', to: 'sms#captcha'

  namespace :admin do
    root 'main#index'

    #商城支付
    get 'shop_detail/index'
    get 'shop_detail/go_to_alipay_pc_direct_pay'
    
    get 'shop_buy/index'
    get 'shop_buy/pingpp_alipay_pc_dirct_payment' 
    

    #分店的首页
    resources :branchs

    namespace :targets do
      #意向学员分配
      resources :potential_students do
        member do
        get 'assign'
        end
        collection do
        end
      end
      #学员分配
      resources :students do
        member do
          get 'assign'
        end
      end
    end


    #用户注册
    resources :users do
      collection do
        get 'search_user_by_mobile'
        get 'user_present_or_not'
        get 'sign_up', to: "users#new"
        get 'phone', to: "users#new"
        get 'email', to: "users#email"
        get 'change_pwd', to: 'users#change_pwd'
        post 'change_pwd', to: "users#update_pwd"
      end
    end


    #用户登录与退出
    resources :sessions do
      collection do
        get "sign_in", to: "sessions#new"
        post 'sign_in', to: "sessions#create"
        get "authoriz_failed", to: "sessions#authoriz_failed"
        get :logout
      end
    end

    #固定成本设置
    resources :fixed_costs do
    end

    #分店内的数据统计
    resources :statistics do
      collection do
        get "comprehensive"
        get "commodity"
        get "curriculum"
        get "recruit"
        get "service"
        get "student"
        get "teacher"
      end
    end

    #公司那边的数据统计
    resources :statistical_datum do
      collection do
        #综合统计
        get "comprehensive"
        get "change_comprehensive"
        get "commodity"
        get "change_commodity"
        get "curriculum"
        get "change_curriculum"
        get "recruit"
        get "service"
        get "student"
        get "change_student"
        get "teacher"
        get "change_teacher"
        get "finance"
        get "resource"
      end

    end

    #服务顾问服务系统
    resources :service_consultants do

    end

    #指标的设置
    resources :targets do
      collection do
        #---- 总监指标 ----
        get 'director'
        get 'director_target'
        get 'new_director_target'
        post 'save_director_target'
        get 'edit_director_target'
        post 'update_director_target'
        #---- end -----
        #----- 市场专员 ------
        get 'market'
        get 'market_target'
        get 'new_market_target'
        post 'save_market_target'
        get 'edit_market_target'
        post 'update_market_target'
        get 'detail_market'
        get "get_market_detail_list"
        get 'assign_potential_students'
        #---------- end ----------
        #---------服务顾问--------
        get 'operator'
        get 'operator_target'
        get 'new_operator_target'
        post 'save_operator_target'
        get 'edit_operator_target'
        post 'update_operator_target'
        get 'detail_operator'
        get "get_operator_detail_list"
        #----------- end ----------
        #---------导师指标---------
        get 'teacher'
        get 'teacher_target'
        get 'new_teacher_target'
        post 'save_teacher_target'
        get 'edit_teacher_target'
        post 'update_teacher_target'
        get 'detail_teacher'
        #----------- end ----------

      end
    end

    #学员系统
    resources :students  do
      #评价或回访
      resources :student_remarks do
        collection do
          get "get_data_list"
        end
      end
      member do
        get "orders"
      end
      collection do
        post "save_student"
        get "get_student_list"
      end
    end

    #课程周表
    resources :course_weeks do
      member do
        get "appointment"
        get "student_check"
        get "teacher_check"
      end
      collection do
        get "get_course_week"
        post "check_in"
        get "new_private_check_in"
        post "private_check_in"
        post "teacher_check_in"
        post "appointmented"
        get "get_pay_way"
        delete "destroy_appointment_record"
      end
    end

    #课程设置
    resources :course_infos do
      member do
        delete "close_course"
      end
      collection do
        get "get_course_list"
        post "update_course"
        delete "destroy_appointment_record"
      end
    end

    #意向学员
    resources :potential_students do
      collection do
        get "new_remark"
        post "save_remark"
        get "check_student"
        post "save_student"
        get "get_student_list"
      end
    end

    #财务
    resources :finances do
      member do
        get 'edit_income'
        get 'edit_expense'
      end
      collection do
        get "laborage"
        get 'initial_state'
        post 'set_initial'
        get 'income'
        get 'expense'
        get "get_finance_list"
        get 'month_expenditure'
      end
    end

    # 商品
    resources :goods do
      member do
        get :record
        get :change_status
        get :price
        post :update_good
      end
      collection do
        get :goods_shop
        get :get_good_list
        get :new_order
        post :create_order
      end
    end

    # 导师系统
    resources :teachers do
      member do
        get :get_course_week
        get :get_course_week_detail
      end
      collection do
        get :get_teacher_search_out
        get :get_teacher_by_name_phone
        get :get_my_students_by_teacher_id
        get :get_my_record_teaching_salary
        get :get_my_students_by_teacher_id
        get :show_my_student_detail
        get :remark_my_student
        get :new_comment_in_teacher
      end
    end

    # 订单
    resources :orders do
    end

    # 公司
    resources :companies do
      collection do
        get :recharge
        get :new_studio
        get :finish_paying
        get :switch_company
      end
    end

    # 员工
    resources :employees do
      collection do
        get 'get_employee'
      end
    end

    # 分店
    resources :studios do
      collection do
        get :finish_paying
        get :switch_studio
        get :update_studio_name
      end
    end

    # 公司设置
    resources :company_configurations do
      collection do
        #删除公司设置
        delete :destroy_configuration
        delete :destroy_goods_category

        #学员设置
        get :student
        post :create_student_group
        post :create_potential_student_group

        #课程设置
        get :course
        post :create_course_room
        post :update_private_education

        #商品设置
        get :goods
        post :create_goods_category

        #人资设置
        get :employee
        post :create_department
        post :create_position
        post :create_teacher_level
        post :create_employee_welfare

        #奖金设置
        get :bonus
        post :update_company_name
        post :update_bonus
      end
    end

  end
end
