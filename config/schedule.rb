require File.expand_path(File.dirname(__FILE__) + "/environment")
# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, {:error => "#{Rails.root}/log/cron_error.log", :standard => "#{Rails.root}/log/cron.log"}

#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

#每天晚上1:20分,刷新当月员工工资的统计数据
every :day, :at => '1:20am' do
  rake "salary:last_and_current_month[2]"
end

#每天晚上2:20分,刷新单位前天的收支信息
every :day, :at => '2:20am' do
  rake "finance:yesterday"
end



# every 1.minute do # 1.minute 1.day 1.week 1.month 1.year is also supported
#   rake "salary:last_and_current_month[30]"
# end