require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)


module YundiudiuHeipisNew
  class Application < Rails::Application
    config.time_zone = 'Beijing'
    config.active_record.default_timezone = :local
    config.encoding = 'utf-8'
    config.eager_load_paths << Rails.root.join('lib')

    config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = 'zh-CN'
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
